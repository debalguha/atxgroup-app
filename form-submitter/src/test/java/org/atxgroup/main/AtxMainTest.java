package org.atxgroup.main;

import static org.junit.Assert.assertFalse;

import java.io.File;
import java.net.URL;
import java.util.LinkedList;
import java.util.Queue;

import org.atxgroup.worker.ResultObject;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AtxMainTest {
	@SuppressWarnings({ "resource" })
	@Test
	public void testStartScrapingFileQueueOfResultObjectString() throws Exception{
		//SystemUtils.setDefaults();
		ApplicationContext childCtx = null;
		try {
			ApplicationContext rootCtx = new ClassPathXmlApplicationContext("applicationContext.xml");
			childCtx = new ClassPathXmlApplicationContext(new String[]{"child-application-context.xml"}, rootCtx);
			//childCtx = new FileSystemXmlApplicationContext(new String[]{System.getProperty("user.dir")+"form-submitter/src/main/webapp/WEB-INF/spring-mvc-context.xml"}, rootCtx);
//			NameService service = childCtx.getBean(NameService.class);
//			NamesCache cache = childCtx.getBean(NamesCache.class);
			AtxMain atxMain = childCtx.getBean(AtxMain.class);
//			atxMain.setPoolSize(5);
//			atxMain.setFormNameClassMap((Map<String, String>)childCtx.getBean("form-class-map"));
//			atxMain.afterPropertiesSet();
			URL url = Thread.currentThread().getContextClassLoader().getResource("tooltester2.csv");
			Queue<ResultObject> queue = new LinkedList<ResultObject>();
			atxMain.startScraping(new File(url.getFile()), queue, "HEYERVERB");
			assertFalse(queue.isEmpty());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally{
			childCtx = null;
		}
	}	
}
