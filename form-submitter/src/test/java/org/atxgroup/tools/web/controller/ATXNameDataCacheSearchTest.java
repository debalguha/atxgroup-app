package org.atxgroup.tools.web.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.atxgroup.persistence.model.NameRequest;
import org.atxgroup.tools.cache.ATXNamesDataCache;
import org.junit.Test;

public class ATXNameDataCacheSearchTest {
	private static final Log logger = LogFactory.getLog(ATXNameDataCacheSearchTest.class);
	@Test
	public void testSearchInNamesCache() {
		NameRequest name = null;
		Set<NameRequest> names = new HashSet<NameRequest>();
		String oneNameChosen = null;
		for(int i=0;i<10000;i++){
			name = new NameRequest();
			name.setAction("rsvp");
			String randomStr = UUID.randomUUID().toString();
			name.setName(randomStr);
			name.setEmail(randomStr);
			if(i==5477)
				oneNameChosen = randomStr;
			names.add(name);
		}
		logger.info("begining search");
		ATXNamesDataCache cache = new ATXNamesDataCache();
		name = cache.searchInNamesCacheForNameTest(oneNameChosen, names).iterator().next();
		System.out.println(name.getName()+"--"+name.getEmail());
		assertEquals(oneNameChosen, name.getName());
		assertNotNull(cache.searchInNamesCacheForNameTest("Debal", names));
		assertTrue(cache.searchInNamesCacheForNameTest("Debal", names).isEmpty());
		assertTrue(cache.searchInNamesCacheForNameTest("Debal", new HashSet<NameRequest>()).isEmpty());
		logger.info("ending search");
	}
	
}
