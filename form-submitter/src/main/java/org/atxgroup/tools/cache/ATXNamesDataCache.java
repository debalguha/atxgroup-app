package org.atxgroup.tools.cache;

import static ch.lambdaj.Lambda.filter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.atxgroup.names.cache.NamesCache;
import org.atxgroup.names.cache.TooMuchReordsFoundException;
import org.atxgroup.persistence.model.NameRequest;
import org.atxgroup.persistence.service.NameService;
import org.hamcrest.Matcher;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import ch.lambdaj.function.matcher.Predicate;

public class ATXNamesDataCache implements NamesCache, InitializingBean {
	private Map<String, NameRequest> namesNameRequestCache;
	private Map<String, NameRequest> emailNameRequestCache;
	private Set<NameRequest> namesCache;
	private static final Log logger = LogFactory.getLog(ATXNamesDataCache.class);
	@Autowired
	private NameService nameService;
	@Value("${cache.search.limit}")
	private int searchLimit;
	
	private Lock cacheLock = new ReentrantLock();
	
	public void init(){
		namesNameRequestCache = new ConcurrentHashMap<String, NameRequest>();
		emailNameRequestCache = new ConcurrentHashMap<String, NameRequest>();
		namesCache = new CopyOnWriteArraySet<NameRequest>();		
		long minId = -1;
		try {
			minId = nameService.findMinId();
		} catch (Exception e) {
			logger.error("Unable to find minId. Seems database is unpopulated!!");
		}
		if(minId == -1)
			return;
		minId--;
		long maxId = nameService.findMaxId()+1;
		int step = 1000;
		Collection<NameRequest> names = new ArrayList<NameRequest>();
		try{
			while(minId<maxId){
				long limit = minId+step;
				Collection<NameRequest> namesWithinRange = nameService.findNamesWithinRange(minId, limit);
				if(namesWithinRange!=null && !namesWithinRange.isEmpty()){
					logger.info("Found "+namesWithinRange.size()+" names within "+minId+" to "+maxId+" range.");
					names.addAll(namesWithinRange);
				}else{
					logger.warn("Found no names within "+minId+" to "+maxId+" range.");
					break;
				}
				minId = limit+1;
			}
			if(!names.isEmpty()){
				logger.info("Going to build cache.");
				for(NameRequest nameRequest : names){
					namesNameRequestCache.put(nameRequest.getName(), nameRequest);
					emailNameRequestCache.put(nameRequest.getEmail(), nameRequest);
					namesCache.add(nameRequest);
				}
				logger.info("End building cache with ["+namesCache.size()+"] entries.");
			}else
				logger.warn("No names exist in database. Cache will remain empty.");
		}catch(Exception e){
			throw new RuntimeException("Error while buiding cache!! Can not continue!!", e);
		}
	}

	/* (non-Javadoc)
	 * @see org.atxgroup.tools.cache.NamesCache#setNameService(org.atxgroup.persistence.service.NameService)
	 */
	public void setNameService(NameService nameService) {
		this.nameService = nameService;
	}
	
	public List<NameRequest> searchInNamesCacheForNameTest(final String name, Set<NameRequest> names){
		this.namesCache = names;
		return lambdaSearchInCacheForName(name);
	}
	
	public List<NameRequest> searchInNamesCacheForEmailTest(final String email, Set<NameRequest> names){
		this.namesCache = names;
		return searchInEmailCache(email);
	}	
	
	/* (non-Javadoc)
	 * @see org.atxgroup.tools.cache.NamesCache#searchInNamesCache(java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	public List<NameRequest> searchInNamesCache(final String name){
		cacheLock.lock();
		
		try {
			List<NameRequest> names = null;
			if(name.equals("*")){
				names = new ArrayList<NameRequest>();
				names.addAll(namesCache);
				return names;
			}		
			if(namesNameRequestCache.containsKey(name)){
				List<NameRequest> retList = new ArrayList<NameRequest>(1);
				retList.add(namesNameRequestCache.get(name));
				return retList;
			}
			names = lambdaSearchInCacheForName(name);
			if(names.size()>searchLimit)
				throw new TooMuchReordsFoundException(new Long(names.size()), "Your search returned too much records. Please narrow down your search.");
			return names;
		} catch (Exception e) {
			logger.error(e);
			return Collections.EMPTY_LIST;
		} finally{
			cacheLock.unlock();
		}
	}
	
	/* (non-Javadoc)
	 * @see org.atxgroup.tools.cache.NamesCache#searchInEmailCache(java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	public List<NameRequest> searchInEmailCache(final String email){
		cacheLock.lock();
		try {
			if(email.equals("*")){
				List<NameRequest> names = new ArrayList<NameRequest>();
				names.addAll(namesCache);
				return names;
			}
			if(emailNameRequestCache.containsKey(email)){
				List<NameRequest> retList = new ArrayList<NameRequest>(1);
				retList.add(emailNameRequestCache.get(email));
				return retList;
			}
			return lambdaSearchInCacheForEMail(email);
		} catch (RuntimeException e) {
			logger.error(e);
			return Collections.EMPTY_LIST;
		} finally{
			cacheLock.unlock();
		}
	}	
	
	private List<NameRequest> lambdaSearchInCacheForName(final String elementToSearch){
		Matcher<NameRequest> nameMatcher = new Predicate<NameRequest>() {
			@Override
			public boolean apply(NameRequest item) {
				return item.getName().toLowerCase().contains(elementToSearch.toLowerCase());
			}
		};
		return filter(nameMatcher, namesCache);
	}
	
	private List<NameRequest> lambdaSearchInCacheForEMail(final String elementToSearch){
		Matcher<NameRequest> nameMatcher = new Predicate<NameRequest>() {
			@Override
			public boolean apply(NameRequest item) {
				return item.getEmail().toLowerCase().contains(elementToSearch.toLowerCase());
			}
		};
		return filter(nameMatcher, namesCache);
	}	

	/* (non-Javadoc)
	 * @see org.atxgroup.tools.cache.NamesCache#getNamesCache()
	 */
	public Set<NameRequest> getNamesCache() {
		return namesCache;
	}
	
	/* (non-Javadoc)
	 * @see org.atxgroup.tools.cache.NamesCache#updateCache(org.atxgroup.persistence.model.NameRequest)
	 */
	public void updateCache(NameRequest name){
		/*namesCache.add(name);
		namesNameRequestCache.put(name.getName(), name);
		emailNameRequestCache.put(name.getEmail(), name);*/
		//throw new MethodNotSupportedException("This method is no longer supported");
	}

	public void afterPropertiesSet() throws Exception {
		this.init();
	}

	public void refresh() {
		cacheLock.lock();
		this.init();
		cacheLock.unlock();
	}
	
	public NameRequest searchInEmailCacheForExactMatch(String email) {
		return emailNameRequestCache.get(email);
	}
}
