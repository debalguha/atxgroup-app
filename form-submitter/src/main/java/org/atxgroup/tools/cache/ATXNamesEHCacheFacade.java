package org.atxgroup.tools.cache;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.atxgroup.names.cache.NamesCache;
import org.atxgroup.persistence.model.NameRequest;
import org.atxgroup.persistence.service.NameService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import net.sf.ehcache.Cache;
import net.sf.ehcache.Element;
import net.sf.ehcache.search.Query;
import net.sf.ehcache.search.Result;

@Component
public class ATXNamesEHCacheFacade implements NamesCache, InitializingBean{
	private static final Logger logger = LoggerFactory.getLogger(ATXNamesEHCacheFacade.class);
	@Autowired
	private Cache cache;
	
	@Autowired
	private NameService nameService;
	@Value("${cache.search.limit}")
	private int searchLimit;
	
	@Override
	public void afterPropertiesSet() throws Exception {
		loadCache();
	}

	private void loadCache() {
		long minId = -1;
		try {
			minId = nameService.findMinId();
		} catch (Exception e) {
			logger.error("Unable to find minId. Seems database is unpopulated!!");
		}
		if(minId == -1)
			return;
		minId--;
		long maxId = nameService.findMaxId()+1;
		int step = 1000;
		try{
			while(minId<maxId){
				long limit = minId+step;
				Collection<NameRequest> namesWithinRange = nameService.findNamesWithinRange(minId, limit);
				if(!CollectionUtils.isEmpty(namesWithinRange)){
					logger.info("Found "+namesWithinRange.size()+" names within "+minId+" to "+maxId+" range.");
					for(NameRequest nameRequest : namesWithinRange)
						cache.put(new Element(nameRequest.getEmail(), nameRequest));
				}else{
					logger.warn("Found no names within "+minId+" to "+maxId+" range.");
					break;
				}
				minId = limit+1;
			}
			logger.info("End building cache with ["+cache.getStatistics().getSize()+"] entries.");
		}catch(Exception e){
			throw new RuntimeException("Error while buiding cache!! Can not continue!!", e);
		}
	}

	@Override
	public void setNameService(NameService nameService) {
		this.nameService = nameService;
	}

	@Override
	public List<NameRequest> searchInNamesCache(String name) {
		Query query = cache.createQuery().addCriteria(cache.getSearchAttribute("name").ilike("*".concat(name).concat("*")));
		query.includeValues();
		return Lists.transform(query.execute().all(), new Function<Result, NameRequest>() {
			public NameRequest apply(Result input) {
				return (NameRequest)input.getValue();
			};
		});
	}

	@Override
	public List<NameRequest> searchInEmailCache(String email) {
		Query query = cache.createQuery().addCriteria(cache.getSearchAttribute("email").ilike("*".concat(email).concat("*")));
		query.includeValues();
		return Lists.transform(query.execute().all(), new Function<Result, NameRequest>() {
			public NameRequest apply(Result input) {
				return (NameRequest)input.getValue();
			};
		});
	}

	@Override
	public NameRequest searchInEmailCacheForExactMatch(String email) {
		return cache.isKeyInCache(email)?(NameRequest)cache.get(email).getObjectValue():null	;
	}

	@Override
	public Set<NameRequest> getNamesCache() {
		Set<NameRequest> ret = Sets.newHashSet();
		for(Object key : cache.getKeys())
			ret.add((NameRequest)cache.get(key).getObjectValue());
		return ret;
	}

	@Override
	public synchronized void updateCache(NameRequest name) {
		cache.put(new Element(name.getEmail(), name));
	}

	@Override
	public void refresh() {
		cache.removeAll();
		loadCache();
	}

}
