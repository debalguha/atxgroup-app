package org.atxgroup.tools.web.vo;

import java.util.Collection;

public class PageableStatusVO<T> {
	private final Long totalRecords;
	private final Collection<T> items;
	
	public PageableStatusVO(Long totalRecords, Collection<T> items) {
		super();
		this.totalRecords = totalRecords;
		this.items = items;
	}

	public Long getTotalRecords() {
		return totalRecords;
	}

	public Collection<T> getItems() {
		return items;
	}

}
