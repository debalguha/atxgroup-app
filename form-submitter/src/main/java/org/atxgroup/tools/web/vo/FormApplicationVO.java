package org.atxgroup.tools.web.vo;

public class FormApplicationVO {
	private final String formName;
	private final String group;
	
	public FormApplicationVO(String formName, String group) {
		super();
		this.formName = formName;
		this.group = group;
	}
	public String getFormName() {
		return formName;
	}
	public String getGroup() {
		return group;
	}
}
