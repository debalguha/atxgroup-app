package org.atxgroup.tools.web;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

public class FormSubmitterSessionListener implements HttpSessionListener {


	@Override
	public void sessionCreated(HttpSessionEvent arg0) {
		Lock processingLock = new ReentrantLock(true);
		arg0.getSession().setAttribute("PROCESSING_LOCK", processingLock);
		
		Lock elementCountLock = new ReentrantLock(true);
		arg0.getSession().setAttribute("ELEMENT_COUNT_LOCK", elementCountLock);
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent arg0) {

	}

}
