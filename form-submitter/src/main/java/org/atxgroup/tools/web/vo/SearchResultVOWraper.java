package org.atxgroup.tools.web.vo;

import java.util.List;

public class SearchResultVOWraper {
	private String warningMessage;
	private String errorMessage;
	private boolean error;
	private boolean warning;
	private List<SearchResultVO> result;
	public String getWarningMessage() {
		return warningMessage;
	}
	public void setWarningMessage(String warningMessage) {
		this.warningMessage = warningMessage;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public boolean isError() {
		return error;
	}
	public void setError(boolean error) {
		this.error = error;
	}
	public boolean isWarning() {
		return warning;
	}
	public void setWarning(boolean warning) {
		this.warning = warning;
	}
	public List<SearchResultVO> getResult() {
		return result;
	}
	public void setResult(List<SearchResultVO> result) {
		this.result = result;
	}
}
