package org.atxgroup.tools.web.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Collection;
import java.util.Collections;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.atxgroup.persistence.model.NameRequest;
import org.atxgroup.persistence.service.NameService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mchange.io.FileUtils;

@RequestMapping("/report")
@Controller
public class ReportController {
	private static final Logger logger = LoggerFactory.getLogger(ReportController.class);
	@Autowired
	private NameService service;

	@RequestMapping(value = "/form/{formName}", method = RequestMethod.GET)
	public @ResponseBody Collection<NameRequest> search(@PathVariable("formName") String formName) {
		if (formName.equals("-"))
			return Collections.emptyList();
		return service.findAllNamesSubmittedForAForm(formName);
	}

	@RequestMapping(value = "/download", method = RequestMethod.GET)
	public void downloadReport(@RequestParam(value = "formSelectForReport") String formName,
			HttpServletResponse response) throws IOException {
		response.setHeader("Set-Cookie", "fileDownload=true; path=/");
		response.setHeader("Cache-Control", "must-revalidate");
		response.setHeader("Content-Type", "text/csv");
		if (formName.equals("-")) {
			File csvFile = File.createTempFile("null", ".csv");
			CSVPrinter printer = CSVFormat.EXCEL.withHeader("First Name", "Last Name", "Email")
					.print(new OutputStreamWriter(new FileOutputStream(csvFile)));
			printer.close();
			response.setHeader("Content-disposition", "attachment;filename=\"" + csvFile.getName() + "\"");
			logger.info("Going to copy file to output stream.");
			FileCopyUtils.copy(FileUtils.getBytes(csvFile), response.getOutputStream());
			logger.info("Copy finished!");
			response.getOutputStream().flush();
		} else {
			Collection<NameRequest> namesSubmittedForAForm = service.findAllNamesSubmittedForAForm(formName);
			File csvFile = createCSVFile(formName, namesSubmittedForAForm);

			response.setHeader("Content-disposition", "attachment;filename=\"" + csvFile.getName() + "\"");
			logger.info("Going to copy file to output stream.");
			FileCopyUtils.copy(FileUtils.getBytes(csvFile), response.getOutputStream());
			logger.info("Copy finished!");
			response.getOutputStream().flush();
		}
	}

	private File createCSVFile(String formName, Collection<NameRequest> namesSubmittedForAForm) throws IOException {
		File csvFile = File.createTempFile(formName, ".csv");
		CSVPrinter printer = CSVFormat.EXCEL.withHeader("First Name", "Last Name", "Email")
				.print(new OutputStreamWriter(new FileOutputStream(csvFile)));
		for (NameRequest name : namesSubmittedForAForm)
			printer.printRecord(name.getFirstName(), name.getLastName(), name.getEmail());
		printer.close();
		return csvFile;
	}
}
