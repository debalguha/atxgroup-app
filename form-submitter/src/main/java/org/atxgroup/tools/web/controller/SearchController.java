package org.atxgroup.tools.web.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.atxgroup.names.cache.NamesCache;
import org.atxgroup.names.cache.TooMuchReordsFoundException;
import org.atxgroup.persistence.model.NameRequest;
import org.atxgroup.persistence.model.NameRequestApplicationForm;
import org.atxgroup.tools.web.vo.FormApplicationVO;
import org.atxgroup.tools.web.vo.SearchResultVO;
import org.atxgroup.tools.web.vo.SearchResultVOWraper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@RequestMapping("/controller")
@Controller
public class SearchController {
	private static final Log logger = LogFactory.getLog(SearchController.class);
	@Autowired
	private NamesCache cache;
	
	/***************************************************
	 * URL: /rest/controller/search upload(): receives files
	 * 
	 * @param request
	 *            : MultipartHttpServletRequest auto passed
	 * @param response
	 *            : HttpServletResponse auto passed
	 * @return LinkedList<FileMeta> as json format
	 ****************************************************/

	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public @ResponseBody SearchResultVOWraper search(@RequestParam(value="attribSelect", required=true) String attribToSearchOn,
			@RequestParam(value="searchText", required=true) String searchTxt) {
		List<NameRequest> names = new ArrayList<NameRequest>();
		if(attribToSearchOn.equalsIgnoreCase("name"))
			names = cache.searchInNamesCache(searchTxt);
		else
			names = cache.searchInEmailCache(searchTxt);
		SearchResultVOWraper wrapper = new SearchResultVOWraper();
		try {
			if(names!=null && !names.isEmpty()){
				List<SearchResultVO> results = new ArrayList<SearchResultVO>();
				for(NameRequest name : names){
					List<FormApplicationVO> formApplications = new ArrayList<FormApplicationVO>();
					Set<NameRequestApplicationForm> applications = name.getApplications();
					if(applications!=null && !applications.isEmpty()){
						for(NameRequestApplicationForm applicationForm : applications){
							FormApplicationVO formApplicationVO = new FormApplicationVO(applicationForm.getApplicationForm().getFormName(), applicationForm.getGroup()) ;
							formApplications.add(formApplicationVO);
						}
					}
					results.add(new SearchResultVO(name.getName(), name.getEmail(), formApplications));
				}
				wrapper.setResult(results);
			}else{
				wrapper.setWarning(true);
				wrapper.setWarningMessage("Your search yield no results!!");
			}
		} catch(TooMuchReordsFoundException te){
			wrapper.setWarning(true);
			wrapper.setWarningMessage(te.getMessage());
		}catch (Exception e) {
			logger.error("Error while doing search.", e);
			wrapper.setError(true);
			wrapper.setErrorMessage("Some internal error occurred!! Please contact support.");
		}
		return wrapper;
	}
}
