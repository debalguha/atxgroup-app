package org.atxgroup.tools.web.controller;

import java.util.List;

import org.atxgroup.tools.web.forms.FileMeta;

public class FileReturnObject {
	private List<FileMeta> files;

	public FileReturnObject(List<FileMeta> files) {
		super();
		this.files = files;
	}

	public List<FileMeta> getFiles() {
		return files;
	}

	public void setFiles(List<FileMeta> files) {
		this.files = files;
	}
	
}
