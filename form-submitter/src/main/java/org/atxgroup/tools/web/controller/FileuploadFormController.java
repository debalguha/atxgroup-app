package org.atxgroup.tools.web.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.locks.Lock;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.atxgroup.main.AtxMain;
import org.atxgroup.tools.web.forms.FileMeta;
import org.atxgroup.tools.web.vo.ErrorRecordVO;
import org.atxgroup.worker.ResultObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

@RequestMapping("/controller")
@Controller
public class FileuploadFormController {
	private static final Log logger = LogFactory.getLog(FileuploadFormController.class);
	private List<FileReturnObject> filesUploadedTillNow = new CopyOnWriteArrayList<FileReturnObject>();
	@Autowired
	private AtxMain atxMain;

	/***************************************************
	 * URL: /rest/controller/upload upload(): receives files
	 * 
	 * @param request
	 *            : MultipartHttpServletRequest auto passed
	 * @param response
	 *            : HttpServletResponse auto passed
	 * @return LinkedList<FileMeta> as json format
	 ****************************************************/

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public @ResponseBody
	FileReturnObject upload(MultipartHttpServletRequest request, HttpServletResponse response) {
		final String formName = request.getParameter("formName");
		if(formName==null || formName.isEmpty()){
			logger.warn("formName parameter is not present in request. Ignoring the whole bunch of files.");
			return errorForAllFiles(request);
		}
		logger.info("Received upload request");
		Iterator<String> itr = request.getFileNames();
		MultipartFile mpf = null;
		List<FileMeta> files = new ArrayList<FileMeta>();
		while (itr.hasNext()) {
			mpf = request.getFile(itr.next());
			logger.info(mpf.getOriginalFilename() + " uploaded! for form "+formName);
			FileMeta fileMeta = new FileMeta();
			fileMeta.setName(mpf.getOriginalFilename());
			fileMeta.setSize(mpf.getSize() / 1024 + " Kb");
			fileMeta.setFileType(mpf.getContentType());
			String fileExtension = FilenameUtils.getExtension(fileMeta.getName());
			try {
				if(!"csv".equals(fileExtension))
					throw new IllegalArgumentException("Unsupported file type: "+fileExtension);
				fileMeta.setBytes(mpf.getBytes());
				String filePath = System.getProperty("java.io.tmpdir").concat("/").concat(mpf.getOriginalFilename());
				logger.info("File path::" + filePath);
				fileMeta.setFilePath(filePath);
				//FileUtils.forceDelete(new File(filePath));
				FileCopyUtils.copy(mpf.getBytes(), new FileOutputStream(filePath));
				logger.info("File copied.");
				Queue<ResultObject> sessionQueue = (Queue<ResultObject>) request.getSession().getAttribute("RESULT_QUEUE");
				final File csvFile = new File(fileMeta.getFilePath());
				logger.info("Begining file validation!");
				int linesFailedVaidation = validateCSVFile(csvFile, request.getSession());
				final List<String> lines = FileUtils.readLines(csvFile);
				if(linesFailedVaidation>0){
					fileMeta.setError("Some of the records are not conforming to the format, they will be ignored and reported on your screen.!!");
				}
				Lock processingLock = (Lock)request.getSession().getAttribute("ELEMENT_COUNT_LOCK");
				processingLock.lock();
				try{
					if (sessionQueue == null) {
						sessionQueue = new LinkedBlockingQueue<ResultObject>();
						request.getSession().setAttribute("RESULT_QUEUE", sessionQueue);
					}
					if(request.getSession().getAttribute("ELEMENT_COUNT")==null || (Boolean)request.getSession().getAttribute("NEW_REQUEST")){
						request.getSession().setAttribute("ELEMENT_COUNT", new Long(lines.size()-linesFailedVaidation-1));
						request.getSession().setAttribute("NEW_REQUEST", false);
					}else
						request.getSession().setAttribute("ELEMENT_COUNT", ((Long)request.getSession().getAttribute("ELEMENT_COUNT"))+new Long(lines.size()-linesFailedVaidation-1));
					
					
				}finally{
					processingLock.unlock();
				}

				final Queue<ResultObject> resultQueue = sessionQueue;
				logger.info("Initiate processing.");
				new Thread(new Runnable(){
					public void run() {
						try {
							atxMain.startScraping(lines, resultQueue, formName);
						} catch (Exception e) {
							logger.error("Unable to submit records", e);
						}
					}
				}).start();
				logger.info("Processing initiated.");
				files.add(fileMeta);				
			} catch (Exception e) {
				logger.error("Error occurred while processing!!", e);
				fileMeta.setError(e.getMessage());
				files.add(fileMeta);
				continue;
			}
		}
		FileReturnObject fileRet = new FileReturnObject(files);
		filesUploadedTillNow.add(fileRet);
		return fileRet;
	}

	private FileReturnObject errorForAllFiles(MultipartHttpServletRequest request) {
		Iterator<String> itr = request.getFileNames();
		MultipartFile mpf = null;
		List<FileMeta> files = new ArrayList<FileMeta>();
		while (itr.hasNext()) {
			mpf = request.getFile(itr.next());
			logger.info(mpf.getOriginalFilename() + " uploaded! ");
			FileMeta fileMeta = new FileMeta();
			fileMeta.setName(mpf.getOriginalFilename());
			fileMeta.setSize(mpf.getSize() / 1024 + " Kb");
			fileMeta.setFileType(mpf.getContentType());
			fileMeta.setError("Operation ignored. Somebody has tampered the html behind.");
			files.add(fileMeta);
		}
		FileReturnObject fileRet = new FileReturnObject(files);
		return fileRet;
	}

	@SuppressWarnings("unchecked")
	public int validateCSVFile(File csvFile, HttpSession httpSession) throws Exception{
		Set<LineRecordForUniqueEmailValidation> lineRecords = new HashSet<LineRecordForUniqueEmailValidation>();
		int linesFailedValidation = 0;
		int totalLines = 0;
		if(csvFile==null || !csvFile.isFile() || !csvFile.canRead())
			throw new IllegalStateException("File either does not exists or is read only.");
		List<ErrorRecordVO> failedLines = new ArrayList<ErrorRecordVO>();
		RandomAccessFile raf = new RandomAccessFile(csvFile, "r");
		String line = "";
		int counter=0;
		try {
			while((line=raf.readLine())!=null){
				if(line.trim().isEmpty() || counter==0){
					counter++;
					continue;
				}
				totalLines++;
				String []splits = line.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
				if(splits.length!=8){
					linesFailedValidation++;
					failedLines.add(new ErrorRecordVO(counter, line, "Line Format Error!!"));
				}else{
					String email = splits[3];
					if(!lineRecords.add(new LineRecordForUniqueEmailValidation(email, line))){
						linesFailedValidation++;
						failedLines.add(new ErrorRecordVO(counter, line, "Duplicate email!! This email has already repeated in the file and will be ignored!!"));
					}
				}
				counter++;
			}
		} catch (Exception e) {
			throw e;
		} finally{
			raf.close();
		}
		if(totalLines == linesFailedValidation)
			throw new IllegalArgumentException("Not a single record found in this file that can be processed!!");
		
		if(httpSession!=null){
			Map<String, List<ErrorRecordVO>> failedLinesPerFile = (Map<String, List<ErrorRecordVO>>) httpSession.getAttribute("LINES_FAILED_TO_PARSE");
			if(failedLinesPerFile == null){
				failedLinesPerFile = new HashMap<String, List<ErrorRecordVO>>();
				httpSession.setAttribute("LINES_FAILED_TO_PARSE", failedLinesPerFile);
			}
			failedLinesPerFile.put(csvFile.getName(), failedLines);
		}
		return linesFailedValidation;
	}

	/***************************************************
	 * URL: /rest/controller/get/{value} get(): get file as an attachment
	 * 
	 * @param response
	 *            : passed by the server
	 * @param value
	 *            : value from the URL
	 * @return void
	 ****************************************************/
	@RequestMapping(value = "/get/{value}", method = RequestMethod.GET)
	public void get(HttpServletResponse response, @PathVariable String value) {
		/*
		 * FileMeta getFile = files.get(Integer.parseInt(value)); try {
		 * response.setContentType(getFile.getFileType());
		 * response.setHeader("Content-disposition", "attachment; filename=\"" +
		 * getFile.getName() + "\""); FileCopyUtils.copy(getFile.getBytes(),
		 * response.getOutputStream()); } catch (IOException e) {
		 * e.printStackTrace(); }
		 */
		throw new UnsupportedOperationException("Download Not Supported!!");
	}

	public void setAtxMain(AtxMain atxMain) {
		this.atxMain = atxMain;
	}
	

}
