package org.atxgroup.tools.web.vo;

public class ErrorRecordVO {
	private final int lineNumber;
	private final String line;
	private final String message;
	public ErrorRecordVO(int lineNumber, String line, String message) {
		super();
		this.lineNumber = lineNumber;
		this.line = line;
		this.message = message;
	}
	public int getLineNumber() {
		return lineNumber;
	}
	public String getLine() {
		return line;
	}
	public String getMessage() {
		return message;
	}
}
