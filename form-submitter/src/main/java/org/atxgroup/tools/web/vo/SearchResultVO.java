package org.atxgroup.tools.web.vo;

import java.util.List;

public class SearchResultVO {
	final private String name;
	final private String email;
	final private List<FormApplicationVO> formApplications;
	public SearchResultVO(String name, String email, List<FormApplicationVO> formApplications) {
		super();
		this.name = name;
		this.email = email;
		this.formApplications = formApplications;
	}
	public String getName() {
		return name;
	}
	public String getEmail() {
		return email;
	}
	public List<FormApplicationVO> getFormApplications() {
		return formApplications;
	}
	
	
}
