package org.atxgroup.tools.web.controller;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Collection;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.atxgroup.names.cache.NamesCache;
import org.atxgroup.persistence.model.NameRequest;
import org.atxgroup.persistence.model.NameRequestApplicationForm;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mchange.io.FileUtils;

@Controller
@RequestMapping("/controller")
public class FileDownloadFormController {
	private static final Log logger = LogFactory
			.getLog(FileDownloadFormController.class);
	@Autowired
	private NamesCache cache;
	@RequestMapping(value = "/download", method = RequestMethod.GET)
	public void download(HttpSession session, HttpServletResponse response) {
		logger.info("Download request arrived.");
		File csvFile = (File) session.getAttribute("DOWNLOAD_FILE");
		if (csvFile == null || !csvFile.exists() || !csvFile.canRead()) {
			logger.error("No file found in session. Already downoaded.");
			return;
		}
		logger.info("File found in session: " + csvFile.getName());
		try {
			session.removeAttribute("DOWNLOAD_FILE");
			logger.info("Setting response headers and blah blah");
			response.setHeader("Set-Cookie", "fileDownload=true; path=/");
			response.setHeader("Cache-Control", "must-revalidate");
			response.setHeader("Content-Type", "text/csv");
			response.setHeader("Content-disposition", "attachment;filename=\"" + csvFile.getName() + "\"");
			logger.info("Going to copy file to output stream.");
			FileCopyUtils.copy(FileUtils.getBytes(csvFile), response.getOutputStream());
			logger.info("Copy finished!");
			response.getOutputStream().flush();
		} catch (IOException e) {
			logger.error(e);
		}
	}

	@RequestMapping(value = "/prepareDownload", method = RequestMethod.GET)
	public @ResponseBody
	String prepareDownload(HttpSession session) throws JsonGenerationException,
			JsonMappingException, IOException {
		File csvFile = File.createTempFile("NameStore", ".csv");
		Collection<NameRequest> names = null;//new ArrayList<NameRequest>();
		try {
			names = cache.getNamesCache();
			if (!names.isEmpty()) {
				logger.info("Going to write to file.");
				writeToFile(csvFile, names);
				session.setAttribute("DOWNLOAD_FILE", csvFile);
			} else
				throw new Exception("No name exists!!");
			return "SUCCESS";
		} catch (Exception e) {
			logger.error("Error while generating download file.", e);
			return "Error while generating download file [" + e.getMessage() + "]";
		}
	}

	private void writeToFile(File csvFile, Collection<NameRequest> names)
			throws Exception {
		BufferedWriter writer = null;
		try {
			writer = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(csvFile)));
			for (NameRequest name : names) {
				StringBuilder builder = new StringBuilder();
				builder.append(name.getName()).append(",")
						.append(name.getEmail()).append(",")
						.append(getAllFormNames(name)).append("\n");
				writer.write(builder.toString());
			}
		} catch (Exception e) {
			throw e;
		} finally {
			if (writer != null)
				writer.close();
		}

	}

	private Object getAllFormNames(NameRequest name) {
		Collection<NameRequestApplicationForm> requestApplicationForms = name
				.getApplications();
		StringBuilder builder = new StringBuilder();
		int counter = 0;
		if(requestApplicationForms==null || requestApplicationForms.isEmpty())
			return "";
		for (NameRequestApplicationForm requestApplicationForm : requestApplicationForms) {
			if (counter != 0)
				builder.append(",");
			builder.append(requestApplicationForm.getApplicationForm()
					.getFormName()).append(",").append(requestApplicationForm.getGroup());
			counter++;
		}
		return builder.toString();
	}

	public void setCache(NamesCache cache) {
		this.cache = cache;
	}
}
