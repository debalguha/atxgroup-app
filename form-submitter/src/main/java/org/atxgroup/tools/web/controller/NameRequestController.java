package org.atxgroup.tools.web.controller;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.atxgroup.main.AtxMain;
import org.atxgroup.names.cache.NamesCache;
import org.atxgroup.persistence.model.NameRequest;
import org.atxgroup.worker.ResultObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@RequestMapping("/controller")
@Controller
public class NameRequestController {
	private static final Log logger = LogFactory.getLog(NameRequestController.class);
	@Autowired
	private AtxMain atxMain;
	
	@Autowired
	private NamesCache cache; 

	@RequestMapping(value = "/name/submit/single", method = RequestMethod.POST)
	public @ResponseBody Object processSingleName(@RequestParam(value="name", required=true) String name, @RequestParam(value="firstName", required=true) String firstName, 
			@RequestParam(value="email", required=true) String email, @RequestParam(value="form", required=true) String form,
			@RequestParam(value="lastName", required=true) String lastName, @RequestParam(value="groupName", required=true) String groupName,
			@RequestParam(value="affiliation", required=true) String affiliation, @RequestParam(value="city", required=true) String city,
			@RequestParam(value="state", required=true) String state, @RequestParam(value="zipCode", required=true) String zipCode) {
		logger.info("Constructing Namerequest object.");
		NameRequest nameRequest = new NameRequest(null, email, null, null, name, false);
		nameRequest.setFirstName(firstName);
		nameRequest.setLastName(lastName);
		nameRequest.setAffiliation(affiliation);
		nameRequest.setGroupName(groupName);
		nameRequest.setName(name);
		nameRequest.setCity(city);
		nameRequest.setState(state);
		nameRequest.setZipCode(zipCode);
		logger.info("Namerequest object constructed. Going to call atx main");
		Queue<ResultObject> sessionQueue = new LinkedList<ResultObject>();
		try {
			atxMain.startScraping(Arrays.asList(new NameRequest[]{nameRequest}), sessionQueue, form);
			return sessionQueue;
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return "ERROR";
	}
	@RequestMapping(value = "/name/cache/refresh", method = RequestMethod.GET)
	public @ResponseBody String rebuildCache(){
		cache.refresh();
		return "done";
	}
	
	public void setAtxMain(AtxMain atxMain) {
		this.atxMain = atxMain;
	}
	public void setCache(NamesCache cache) {
		this.cache = cache;
	}
}
