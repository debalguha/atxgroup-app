package org.atxgroup.tools.web.controller;

public class LineRecordForUniqueEmailValidation {
	private final String email;
	private final String line;
	public LineRecordForUniqueEmailValidation(String email, String line) {
		super();
		this.email = email;
		this.line = line;
	}
	public String getEmail() {
		return email;
	}
	public String getLine() {
		return line;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LineRecordForUniqueEmailValidation other = (LineRecordForUniqueEmailValidation) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		return true;
	}
	
}
