package org.atxgroup.tools.web.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.locks.Lock;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.atxgroup.tools.web.vo.ErrorRecordVO;
import org.atxgroup.tools.web.vo.PageableStatusVO;
import org.atxgroup.worker.ResultObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import ch.lambdaj.Lambda;

@Controller
@RequestMapping("/controller")
public class UploadStatusController {
	private static final Log logger = LogFactory.getLog(UploadStatusController.class);

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/upload/status", method = RequestMethod.GET)
	public @ResponseBody
	PageableStatusVO<ResultObject> status(HttpServletRequest request, HttpSession session) {
		int page = Integer.parseInt(request.getParameter("page"));
		int per_page = Integer.parseInt(request.getParameter("per_page"));
		String sort = request.getParameter("sort");
		String order = request.getParameter("order");
		//Long elements = (Long) session.getAttribute("ELEMENT_COUNT");
		List<ResultObject> items = new ArrayList<ResultObject>();
		List<ResultObject> sessionQueueToList = new ArrayList<ResultObject>();
		Queue<ResultObject> sessionQueue = (Queue<ResultObject>) session.getAttribute("RESULT_QUEUE");
		if (sessionQueue != null && !sessionQueue.isEmpty()) {
			sessionQueueToList.addAll(sessionQueue);
			sortList(sessionQueueToList, sort, order);
			int index = (page - 1) * per_page;
			for (int counter = 0; counter < per_page; counter++) {
				if (index == sessionQueueToList.size())
					break;
				items.add(sessionQueueToList.get(index++));
			}
		}
		return new PageableStatusVO<ResultObject>(new Long(sessionQueue==null?0:sessionQueue.size()), items);
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/upload/status/state", method = RequestMethod.GET)
	public @ResponseBody
	Map<String, Integer> state(HttpSession session) {
		Map<String, Integer> retMap = new HashMap<String, Integer>(2);
		Queue<ResultObject> sessionQueue = (Queue<ResultObject>) session.getAttribute("RESULT_QUEUE");
		Lock processingLock = (Lock) session.getAttribute("ELEMENT_COUNT_LOCK");
		processingLock.lock();
		try {
			Long elements = (Long) session.getAttribute("ELEMENT_COUNT");
			//Integer elementsFailedValidation = (Integer) session.getAttribute("ELEMENTS_FAILED_VALIDATION");
			retMap.put("totalProcesed", sessionQueue.size());
			retMap.put("totalRecords", elements.intValue());
		} finally {
			processingLock.unlock();
		}
		return retMap;
	}

	private List<ResultObject> sortList(List<ResultObject> sessionQueueToList, String sort, final String order) {
		if (sort == null || order == null || sort.isEmpty() || order.isEmpty())
			return sessionQueueToList;
		if (sort.equalsIgnoreCase("name"))
			return Lambda.sort(sessionQueueToList, Lambda.on(ResultObject.class).getName());
		else if (sort.equals("email"))
			return Lambda.sort(sessionQueueToList, Lambda.on(ResultObject.class).getEmail());
		else if (sort.equals("success"))
			return Lambda.sort(sessionQueueToList, Lambda.on(ResultObject.class).isSuccess());
		else
			return Lambda.sort(sessionQueueToList, Lambda.on(ResultObject.class).isPreExists());
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/upload/status/isComplete", method = RequestMethod.GET)
	public @ResponseBody
	String statusIsComplete(HttpSession session) {
		Lock processingLock = (Lock) session.getAttribute("ELEMENT_COUNT_LOCK");
		processingLock.lock();
		Queue<ResultObject> sessionQueue = (Queue<ResultObject>) session.getAttribute("RESULT_QUEUE");
		Long elements = (Long) session.getAttribute("ELEMENT_COUNT");
		//Integer elementsFailedValidation = (Integer) session.getAttribute("ELEMENTS_FAILED_VALIDATION");

		try {
			//if ((elements.intValue() - elementsFailedValidation) <= sessionQueue.size()){
			if (elements.intValue()<= sessionQueue.size()){
				session.setAttribute("NEW_REQUEST", true);
				return "FINISH";
			}
		} finally {
			processingLock.unlock();
		}
		return "PENDING";
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/errors", method = RequestMethod.GET)
	public @ResponseBody
	Map<String, List<ErrorRecordVO>> findErrors(HttpSession session) {
		logger.debug("Error check request arrived.");
		if (session.getAttribute("LINES_FAILED_TO_PARSE") != null) {
			Map<String, List<ErrorRecordVO>> failedLinesPerFile = (Map<String, List<ErrorRecordVO>>) session.getAttribute("LINES_FAILED_TO_PARSE");
			if (failedLinesPerFile != null)
				logger.info("Found " + failedLinesPerFile.size() + " records");
			else
				logger.info("Found 0 records");
			session.removeAttribute("LINES_FAILED_TO_PARSE");
			return failedLinesPerFile;
		}
		logger.info("Nothing found");
		return Collections.EMPTY_MAP;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/session/clean", method = RequestMethod.GET)
	public @ResponseBody
	String cleanSession(HttpSession session) {
		Queue<ResultObject> sessionQueue = (Queue<ResultObject>) session.getAttribute("RESULT_QUEUE");
		Lock processingLock = (Lock) session.getAttribute("ELEMENT_COUNT_LOCK");
		processingLock.lock();
		try {
			session.setAttribute("NEW_REQUEST", true);
			session.setAttribute("ELEMENT_COUNT", new Long(0));
			if(!CollectionUtils.isEmpty(sessionQueue))
				sessionQueue.clear();
		} finally {
			processingLock.unlock();
		}
		return "OK";
	}
}
