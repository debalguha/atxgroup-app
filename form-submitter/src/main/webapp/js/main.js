/*
 * jQuery File Upload Plugin JS Example 8.9.0
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

/* global $, window */

$(function () {
    'use strict';

    // Initialize the jQuery File Upload widget:
    $('#fileupload').fileupload({
        disableImageResize: false
        // Uncomment the following to send cross-domain cookies:
        //xhrFields: {withCredentials: true},
    });

    // Enable iframe cross-domain access via redirect option:
    $('#fileupload').fileupload(
        'option',
        'redirect',
        window.location.href.replace(
            /\/[^\/]*$/,
            '/cors/result.html?%s'
        )
    );
    $('#fileupload').bind('fileuploadsubmit', function (e, data) {
    	data.formData = {formName: $('#fileupload').attr('name')};
    });

    if (window.location.hostname === 'blueimp.github.io') {
        // Demo settings:
        $('#fileupload').fileupload('option', {
            url: '//jquery-file-upload.appspot.com/',
            // Enable image resizing, except for Android and Opera,
            // which actually support image resizing, but fail to
            // send Blob objects via XHR requests:
            disableImageResize: /Android(?!.*Chrome)|Opera/
                .test(window.navigator.userAgent),
            maxFileSize: 5000000,
            acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i
        });
        // Upload server status check for browsers with CORS support:
        if ($.support.cors) {
            $.ajax({
                url: '//jquery-file-upload.appspot.com/',
                type: 'HEAD'
            }).fail(function () {
                $('<div class="alert alert-danger"/>')
                    .text('Upload server currently unavailable - ' +
                            new Date())
                    .appendTo('#fileupload');
            });
        }
    } else {
        // Load existing files:
        $('#fileupload').addClass('fileupload-processing');
        $('#fileupload').bind('fileuploadadd', function(e, data){
        	$('#hiddenErrorButton').css('display', 'none');
        	$.getJSON('rest/controller/session/clean', function(data){
    			$("#successStatusTableDiv").empty();
        	    $("#successStatusTableDiv").append(statusGrid.render().$el);
        	    $("#successStatusTablePaginatorDiv").append(statusPaginator.render().$el);
        	    tableItems.fetch({reset:true});
        	    $progressbar.attr('aria-valuenow', 'percentage');
        	    $progressbar.css("width : 0%;");
        	    $progressbar.find('span').html('0% Completed.');
        	});
        });
        $('#fileupload').bind('fileuploaddone', function (e, data) {
        	statusCheck();
        	errorCheck(data);
        });
        $('#fileupload').bind('fileuploadadd', function (e, data) {
        	$('#errorButton').css('display', 'none');
        });        
        $.ajax({
            // Uncomment the following to send cross-domain cookies:
            //xhrFields: {withCredentials: true},
            url: $('#fileupload').fileupload('option', 'url'),
            dataType: 'json',
            context: $('#fileupload')[0]
        }).always(function () {
            $(this).removeClass('fileupload-processing');
        }).done(function (result) {
            $(this).fileupload('option', 'done')
                .call(this, $.Event('done'), {result: result});
        });
    }

});