<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="row">
	<form action="/form-submitter/rest/report/download" method="get">
		<div class="col-sm-3">
			<select id="formSelectForReport" name="formSelectForReport" class="form-control">
				<option value='-'>---choose---</option>
				<option value="INSIDEMAZDA" data-title="Inside Mazda SXSW" data-msg="This tool automates form submission for Inside Mazda SXSW" selected="selected">Inside Mazda SXSW</option>
				<option value="BROWNIESANDLEMONADE" data-title="Brownies And Lemonade SXSW" data-msg="This tool automates form submission for Brownies And Lemonade SXSW">Brownies And Lemonade SXSW</option>
				<option value="BARRELEDPARTY" data-title="Barrled Party RSVPs" data-msg="This tool automates form submission for Barrled Party RSVPs">Barrled Party RSVPs</option>
				<option value="AUSTYNCITYLIMITS2016" data-title="Austyn City Limits - Bloody Mary Morning RSVP" data-msg="This tool automates form submission for Austyn City Limits - Bloody Mary Morning RSVP" selected="selected">Austyn City Limits</option>
				<option value="WHATSCENEWORKER" data-title="What Scene RSVP" data-msg="This tool automates form submission for What Scene RSVP" selected="selected">What Scene RSVP</option>
			</select>	
		</div>
		<div class="col-sm-1">
			<button type="submit" class="btn btn-warning">
				<i class="glyphicon glyphicon-download-alt"></i><span>CSV</span>
			</button>
		</div>
	</form>
	<div id="reportLoading" style="display: none;">
		<img alt="Working..." src="/form-submitter/img/ajax-loader.gif">
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-success">
			<div class="panel-heading">
				<h3>
					<i class="glyphicon glyphicon-list"></i> <span>Names Submitted</span>
				</h3>			
			</div>
			<div class="panel-body">
				<!-- <div id="filterDiv" style="float: left;">
					<button id="exportToCSV" type="submit" class="btn btn-primary start">Export CSV</button>
				</div> -->
				<div id="report-formWise-grid"></div>
				<div id="report-formWise-pagenator"></div>
			</div>		
		</div>
	</div>
</div>
<script type="text/javascript">
	var reportTable;
	var formReportBaseURL = "/form-submitter/rest/report/form/";
	var formChosen="-";
	var NameData = Backbone.Model.extend({});
	var nameColumns = [{
	    name: "firstName",
	    label: "First Name",
	    cell: "string",
	    editable: false
	  }, {
	    name: "lastName",
	    label: "Last Name",
	    cell: "string",
	    editable: false
	  }, {
	    name: "email",
	    label: "Email",
	    cell: "string",
	    editable: false
	  }
	];	
	var PageableReport = Backbone.PageableCollection.extend({		  
		  url: function(){return formReportBaseURL+formChosen},
		  state: {
		    pageSize: 15
		  },
		  mode: "client" // page entirely on the client side
	});

	var pageableReportRecords = new PageableReport();
	var pageableReportGrid = new Backgrid.Grid({
		  columns: nameColumns,
		  collection: pageableReportRecords
	});
	var reportPaginator = new Backgrid.Extension.Paginator({
        collection: pageableReportRecords
    });
	var clientSideFilter = new Backgrid.Extension.ClientSideFilter({
		  collection: pageableReportRecords,
		  placeholder: "Search in the browser",
		  // The model fields to search for matches
		  fields: ['firstName', 'lastName', 'email'],
		  // How long to wait after typing has stopped before searching can start
		  wait: 150
	});

	$("#report-formWise-grid").prepend(clientSideFilter.render().el);	
	$('#report-formWise-grid').append(pageableReportGrid.render().$el);
	$("#report-formWise-pagenator").append(reportPaginator.render().$el);
	$(document).ready(function(){
		$('#formSelectForReport').change(function(){
			formChosen = $(this).val();
			$('#reportLoading').css('display', 'block');
			pageableReportRecords.fetch({
				reset:true,
				success : function(){$('#reportLoading').css('display', 'none');}
			});
		});
	});
</script>