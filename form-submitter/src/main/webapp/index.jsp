<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->
<head>
<title>Form Submission Applications</title>

<meta charset="utf-8">

<meta name="description" content="">
<meta name="author" content="">

<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">

<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800">
<link rel="stylesheet" href="css/font-awesome.css">

<!-- <link rel="stylesheet" href="css/bootstrap.css"> -->
<link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
<!-- <link rel="stylesheet" href="css/bootstrap-responsive.css"> -->

<link rel="stylesheet" href="css/ui-lightness/jquery-ui-1.8.21.custom.css">

<link rel="stylesheet" href="css/application_mod.css">
<link rel="stylesheet" href="css/pages/dashboard.css">
<link rel="stylesheet" href="css/jquery.fileupload.css">
<link rel="stylesheet" href="css/jquery.fileupload-ui.css">

<link rel="stylesheet" href="css/backgrid.min.css">
<link rel="stylesheet" href="css/backgrid-paginator.css">
<link rel="stylesheet" href="css/backgrid.filter.min.css">

<link rel="stylesheet" href="./js/plugins/msgGrowl/css/msgGrowl.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<script src="js/libs/modernizr-2.5.3.min.js"></script>
<script src="js/tmpl.min.js"></script>
<script src="js/load-image.min.js"></script>
<script src="js/canvas-to-blob.min.js"></script>
<script src="js/jquery.blueimp-gallery.min.js"></script>
<script src="js/vendor/jquery.ui.widget.js"></script>
<script src="js/jquery.iframe-transport.js"></script>
<script src="js/jquery.fileupload.js"></script>
<script src="js/jquery.fileupload-process.js"></script>
<script src="js/jquery.fileupload-image.js"></script>
<script src="js/jquery.fileupload-audio.js"></script>
<script src="js/jquery.fileupload-video.js"></script>
<script src="js/jquery.fileupload-validate.js"></script>
<script src="js/jquery.fileupload-ui.js"></script>
<script src="js/libs/jquery.ui.touch-punch.min.js"></script>
<!-- <script src="js/libs/bootstrap/bootstrap.min.js"></script> -->
<script src="https://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
<script src="js/Theme.js"></script>
<script src="js/plugins/msgGrowl/js/msgGrowl.js"></script>
<script src="js/jquery.file.download.js"></script>

<script src="js/underscore-1.6.0.js"></script>
<script src="js/backbone1.1.2.js"></script>
<script src="js/backgrid.min.js"></script>
<script src="js/backbone-pageable.js"></script>
<script src="js/backgrid-paginator.js"></script>
<script src="js/backgrid.filter.min.js"></script>

<style type="text/css">
#table-inner td th{
	background-color: E5CCFF;
	border-style: 1px solid;
	font-size: 14px;
	font-family: verdana,​helvetica,​arial,​sans-serif;
	line-height: 22px;
}
.wordwrap { 
   white-space: pre-wrap;      /* CSS3 */   
   white-space: -moz-pre-wrap; /* Firefox */    
   white-space: -pre-wrap;     /* Opera <7 */   
   white-space: -o-pre-wrap;   /* Opera 7 */    
   word-wrap: break-word;      /* IE */
}
.backgrid td{
font: 12px/1.7em 'Open Sans', arial, sans-serif;
}
</style>
<script type="text/javascript">
/**
HtmlCell renders any html code

@class Backgrid.HtmlCell
@extends Backgrid.Cell
*/
var HtmlCell = Backgrid.HtmlCell = Backgrid.Cell.extend({

	/** @property */
	className : "html-cell",

	initialize : function() {
		Backgrid.Cell.prototype.initialize.apply(this, arguments);
	},

	render : function() {
		this.$el.empty();
		var rawValue = this.model.get(this.column.get("name"));
		var formattedValue = this.formatter.fromRaw(rawValue, this.model);
		this.$el.append('<div class="wordwrap">'+formattedValue+'</div>');
		//this.delegateEvents();
		return this;
	}
});
</script>
</head>

<body>

	<div id="wrapper">

		<div id="topbar">

			<div class="container">

				<a href="javascript:;" id="menu-trigger" class="dropdown-toggle" data-toggle="dropdown" data-target="#"> <i class="icon-cog"></i>
				</a>

				<div id="top-nav">

					<ul class="pull-right">
						<li><a href="javascript:;"><i class="icon-user"></i> Logged in as AtxGroup</a></li>
						<li><a href="javascript:;"><span class="badge badge-primary">1</span> New Message</a></li>
						<li><a href="login.html">Logout</a></li>
					</ul>

				</div>
				<!-- /#top-nav -->

			</div>
			<!-- /.container -->

		</div>
		<!-- /#topbar -->




		<div id="header">

			<div class="container">

				<a href="./index.html" class="brand">Tools Dashboard</a> <a href="javascript:;" class="btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <i class="icon-reorder"></i>
				</a>


			</div>
			<!-- /.container -->

		</div>
		<!-- /#header -->




		<div id="masthead">

			<div class="container">

				<div class="masthead-pad">

					<div class="masthead-text">
						<h2 id="site_title"></h2>
						<p id="site_msg"></p>
					</div>
					<!-- /.masthead-text -->

				</div>

			</div>
			<!-- /.container -->

		</div>
		<!-- /#masthead -->

		<div id="content">
			<div class="container">
				<div class="tabbable">
					<ul id="myTab" class="nav nav-pills nav-tabs">
						<li class="active"><a href="#uploadTab" data-toggle="tab">Upload CSV</a></li>
						<li class=""><a href="#searchTab" data-toggle="tab">Search</a></li>
						<li class=""><a href="#reportTab" data-toggle="tab">Report</a></li>
					</ul>
				</div>
				<div id="myTabContent" class="tab-content" style="margin-top: 30px;">
					<div class="tab-pane fade active in" id="uploadTab">
						<form id="fileupload" action="rest/controller/upload"
							method="post" enctype="multipart/form-data"
							name="AUSTYNPARTYWEEKEND">
							<div id="upload" class="row fileupload-buttonbar">
								<div class="col-sm-8">
									<div class="panel panel-success">
										<div class="panel-heading">
											<h3>
												<i class="glyphicon glyphicon-upload"></i> <span>Upload</span>
											</h3>
										</div>
										<div class="panel-body">
											<div class="form-group">
												<div class="col-sm-3">
													<label class="control-label" for="formSelect">Choose
														Form</label>
												</div>
												<div class="col-sm-5">
													<select id="formSelect" name="formSelect"
														class="form-control">
														<option value="INSIDEMAZDA" data-title="Inside Mazda SXSW" data-msg="This tool automates form submission for Inside Mazda SXSW" selected="selected">Inside Mazda SXSW</option>
														<option value="BROWNIESANDLEMONADE" data-title="Brownies And Lemonade SXSW" data-msg="This tool automates form submission for Brownies And Lemonade SXSW">Brownies And Lemonade SXSW</option>
														<option value="BARRELEDPARTY" data-title="Barrled Party RSVPs" data-msg="This tool automates form submission for Barrled Party RSVPs">Barrled Party RSVPs</option>
														<option value="AUSTYNCITYLIMITS2016" data-title="Austyn City Limits - Bloody Mary Morning RSVP" data-msg="This tool automates form submission for Austyn City Limits - Bloody Mary Morning RSVP" selected="selected">Austyn City Limits</option>
														<option value="WHATSCENEWORKER" data-title="What Scene RSVP" data-msg="This tool automates form submission for What Scene RSVP" selected="selected">What Scene RSVP</option>
														<!-- <option value="UNIVERSALEX" data-title="Universal Experience Music Group" data-msg="This tool automates form submission for pages.umusic-mail.com" selected="selected">Universal Experience Music Group</option>
														<option value="VIBESWORKER" data-title="Vibes" data-msg="This tool automates form submission for lndrnrs.com" selected="selected">Vibes
															Party</option>	
														<option value="AMERICANSONGWRITER" data-title="American Song Writer" data-msg="This tool automates form submission for americansongwriter.us5.list-manage.com">American Song
															Writer</option>														
														<option value="JUMPSTARTTEXAS" data-title="Jump Start Texas" data-msg="This tool automates form submission for jstx.co">Jump Start
															Texas</option>														
														<option value="AUSTYNPARTYWEEKEND" data-title="Austyn Party Weekend" data-msg="This tool automates form submission for austynpartyweekend.com">Austyn Party
															Week-end</option>
														<option value="COLORADOMUSICPARTY" data-title="Colorado Music Party" data-msg="This tool automates form submission for coloradomusicparty.com">Colorado Music
															Party</option>
														<option value="THEGREENROOM" data-title="The Green Room" data-msg="This tool automates form submission for itsthegreenroom.com">The Green 
															Room</option>
														<option value="THEFADER" data-title="The Green Room" data-msg="This tool automates form submission for thefader.com">Join The 
															Fader</option>
														<option value="HEYERVERB" data-title="RSVP for the Reverb Party" data-msg="This tool automates form submission for heyreverb.com">The Heyer 
															Reverb</option>
														<option value="WINDISHAGENCY" data-title="The Windish Agency" data-msg="This tool automates form submission for windishagency.com">The Windish 
															Agency</option>	 -->														
													</select>
												</div>
												<div class="col-sm-4"></div>												
											</div>
											<div class="form-group">
												<div class="col-sm-offset-3 col-sm-9" style="margin-top: 15px; margin-bottom: 10px;">
													<span class="btn btn-success fileinput-button" > <i
														class="glyphicon glyphicon-plus"></i> <span>Add
															files...</span> <input type="file" name="files[]" multiple>
													</span>
													<button type="submit" class="btn btn-primary start">
														<i class="glyphicon glyphicon-upload"></i> <span>Start
															upload</span>
													</button>
													<button type="reset" class="btn btn-danger cancel">
														<i class="glyphicon glyphicon-ban-circle"></i> <span>Cancel
															upload</span>
													</button>
													<div id="hiddenErrorButton" style="display: none;">
														<span class="btn btn-danger btn-lg" data-toggle="modal"
															data-target="#failedRecords">
															<i class="glyphicon glyphicon-exclamation-sign"></i> <span>There
																are errors</span>
														</span>
													</div>
													<span class="fileupload-process"></span>
												</div>											
											</div>
											<div class="form-group">
												<div id="workingDiv" class="col-sm-offset3 col-sm-9" style="display: none;">
													<img alt="Working..." src="/form-submitter/img/ajax-loader.gif">
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-4">
									<table role="presentation" class="table table-striped">
										<tbody class="files"></tbody>
									</table>
								</div>								
							</div>
						</form>
						<div class="row" style="margin-top: 15px;">
							<div class="col-sm-12">
								<div class="panel-group" id="accordion_1">
									<div class="panel panel-default">
										<div id="successPanelHeading" class="panel-heading">
											<h4 class="panel-title" style="font: 12px/1.7em 'Open Sans', arial, sans-serif; font-size: 13px; font-weight: 600;">
												<a data-toggle="collapse" data-parent="#accordion"
													href="#successPane">Succeeded Records</a>
											</h4>
										</div>
										<div id="successPane" class="panel-collapse collapse">
											<div class="panel-body">
												<div id="successStatusTableDiv"></div>
												<div id="successStatusTablePaginatorDiv"></div>
											</div>
										</div>
									</div>
								</div>							
							</div>
						</div>
					</div>
					<div class="tab-pane fade in" id="searchTab">
						<div id="search" class="row">
							<div class="col-sm-5">
								<div class="panel panel-primary">
									<div class="panel-heading">
										<h3>
											<i class="glyphicon glyphicon-search"></i> <span>Search</span>
										</h3>
									</div>
									<div class="panel-body">
										<form id="searchForm" class="form-horizontal" role="form" action="rest/controller/search">
											<div class="form-group">
												<div class="col-sm-3">
													<label class="control-label" for="attribSelect">Search
														on</label>
												</div>
												<div class="col-sm-9">
													<select id="attribSelect" name="attribSelect"
														class="form-control">
														<option value="name">Name</option>
														<option value="mail">Email</option>
													</select>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-3">
													<label class="control-label" for="searchText">Search
														Text</label>
												</div>
												<div class="col-sm-9">
													<input id="searchText" class="form-control" type="text"
														name="searchText" placeholder="seacrh text">
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-offset-3 col-sm-9">
													<button id="searchSubmit" class="btn btn-primary btn-large"
														type="button">
														<i class="glyphicon glyphicon-ok-circle"></i> <span>Submit</span>
													</button>
													<button class="btn btn-large" type="reset">
														<i class="glyphicon glyphicon-ban-circle"></i> <span>Cancle</span>
													</button>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-offset-3 col-sm-9">
													<p>
														You can download the name database <a id="download"
															href="#">here</a>
													</p>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
							<div class="col-sm-7">
								<div id="searchTableDIV">
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane fade in" id="reportTab">
						<jsp:include page="formWiseReport.jsp"/>
					</div>
				</div>
			</div>
			<!-- /.container -->

		</div>
		<!-- /#content -->

	</div>
	<!-- /#wrapper -->
	

	<div class="modal fade" id="failedRecords" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel"><span style="color: white;">Records failed to parse</span></h4>
				</div>
				<div id="failedRecordsModalBody" class="modal-body"></div>
				<div class="modal-footer">
					<button type="button" class="btn btn-inverse" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>

	<div id="footer">

		<div class="container">

			<div class="row">

				<div class="span6">
					� 2014 <a href="#">Rosepetals Infotech</a>, all rights reserved.
				</div>
				<!-- /span6 -->

				<div id="builtby" class="span6">Built by Rosepetals Infotech.</div>
				<!-- /.span6 -->

			</div>
			<!-- /row -->

		</div>
		<!-- /container -->

	</div>
	<div id="dialog-download"></div>
	<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
        <td>
            <span class="preview"></span>
        </td>
        <td>
            <p class="name">{%=file.name%}</p>
            <strong class="error text-danger"></strong>
        </td>
        <td>
            <p class="size">Processing...</p>
            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
        </td>
        <td>
            {% if (!i && !o.options.autoUpload) { %}
                <button class="btn btn-primary start" disabled>
                    <i class="glyphicon glyphicon-upload"></i>
                    <span>Start</span>
                </button>
            {% } %}
            {% if (!i) { %}
                <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancel</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}
</script>
	<!-- The template to display files available for download -->
	<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade">
        <td>
            <span class="preview">
                {% if (file.thumbnailUrl) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
                {% } %}
            </span>
        </td>
        <td>
            <p class="name">
                {% if (file.url) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
                {% } else { %}
                    <span>{%=file.name%}</span>
                {% } %}
            </p>
            {% if (file.error) { %}
                <div><span class="label label-danger">Error</span> {%=file.error%}</div>
            {% } %}
        </td>
        <td>
            <span class="size">{%=o.formatFileSize(file.size)%}</span>
        </td>
        <td>
            {% if (file.deleteUrl) { %}
                <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                    <i class="glyphicon glyphicon-trash"></i>
                    <span>Delete</span>
                </button>
                <input type="checkbox" name="delete" value="1" class="toggle">
            {% } else { %}
                <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancel</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}
</script>
<script>
	var searchTable;
	var sImageUrl = "img/";
	
	$(function() {
		Theme.init();
	});

	errorCheck = function(data) {
		$('#failedRecordsModalBody').empty();
		$.getJSON('rest/controller/errors',function(data) {
			$.each(data,function(key, val) {
				if (val.length <= 0)
					return;
				$('#hiddenErrorButton').css(
						'display', 'block');
				var appendHtml = '<div class="row"><div class="col-sm-12"><table class="table table-striped table-hover wordwrap" style="font: 12px/1.7em \'Open Sans\', arial, sans-serif;"><caption>'
						+ key
						+ '</caption><thead><tr><th class="col-sm-1">Line Number</th><th class="col-sm-6">Record</th><th class="col-sm-5">Error</th></tr></thead><tbody>';
				$.each(val, function(i, item) {
					appendHtml += '<tr><td>'
							+ item.lineNumber
							+ '</td><td>'
							+ item.line.replace(/,/g, ', ')
							+ '</td><td>'
							+ item.message
							+ '</td></tr>';
				});
				appendHtml += '</tbody></table></div></div>';
				$('#failedRecordsModalBody')
						.append(appendHtml);
			});
		});
	};
	formSelectChange = function() {
		$('#fileupload').attr('name', $('#formSelect').val());
		var seletedOption = $('#formSelect').find(":selected");
		$('#site_title').html(seletedOption.attr('data-title'));
		$('#site_msg').html(seletedOption.attr('data-msg'));
	};
	$(document).ready(function() {
		$('#formSelect').change(formSelectChange);
		formSelectChange();
		$('#download').click(function(event) {
			event.stopPropagation();
			$.get("rest/controller/prepareDownload",function(data) {
					if (data == 'SUCCESS') {
						$.msgGrowl({
							type : 'success',
							title : 'SUCCESS',
							text : 'Your download request has been processed and will start soon.'
						});
						$.fileDownload('rest/controller/download',
							{preparingMessageHtml : "Your download is on the way, please wait...",
							failMessageHtml : "There was a problem generating your report, please try again."
						});
					}
				}).fail(function(data) {
					$.msgGrowl({
						type : 'error',
						title : 'ERROR',
						text : data
					});
				});
		});
	});
	$(document).ready(function() {
		$('#searchSubmit').click(
			function() {
				$.post($('#searchForm').attr('action'),
					$('#searchForm').serialize(),
					function(data) {
						updateTable(data);
					}).error(function(data){}
				);
		});
	});
	$(document).ready(function() {
		$('#failedPane').on('shown.bs.collapse',function() {
			$('#failedPanelHeading').css('background-color','#8ab15a');
			$('#failedPanelHeading').css('text-shadow','0 -1px 0 rgba(0, 0, 0, 0.25)');
			$('#failedPanelHeading').find('h4').find('a').css('color', '#ffffff');
		});
		$('#failedPane').on('hidden.bs.collapse',function() {
			$('#failedPanelHeading').css('background-color','#F5F5F5');
			$('#failedPanelHeading').css('text-shadow','0 -1px 0 rgba(255, 255, 255, 0.25)');
			$('#failedPanelHeading').find('h4').find('a').css('color', '#444444');
		})
		$('#successPane').on('shown.bs.collapse',function() {
			$('#successPanelHeading').css('background-color','#8ab15a');
			$('#successPanelHeading').css('text-shadow','0 -1px 0 rgba(0, 0, 0, 0.25)');
			$('#successPanelHeading').find('h4').find('a').css('color', '#ffffff');
		});
		$('#successPane').on('hidden.bs.collapse',function() {
			$('#successPanelHeading').css('background-color','#F5F5F5');
			$('#successPanelHeading').css('text-shadow','0 -1px 0 rgba(255, 255, 255, 0.25)');
			$('#successPanelHeading').find('h4').find('a').css('color', '#444444');
		});
		$('#successPane').collapse('toggle');
	});
	
	
	var DataItem = Backbone.Model.extend({
		post : function(model, theUI){
			console.log('here..'+model);
			$.post('rest/controller/name/submit/single', this.toJSON(), function(data){
				  if(data == 'ERROR')
					  model.set('retryNum', (model.get('retryNum')+1));
				  else{
					  if(data[0].success || data[0].preExists){
						  model.set('error', '');
						  theUI.html('<span class="badge badge-primary"><i class="glyphicon glyphicon-thumbs-up"></i></span>');
				  	  }else{
				  		model.set('retryNum', (model.get('retryNum')+1));
				  		theUI.html('<span class="btn btn-warning"><i class="glyphicon glyphicon-ban-circle"></i> <span>Retry</span></span>');
				  	  }
			 	  }
			});
		}
	});
	var TableItems = Backbone.PageableCollection.extend({

		model : DataItem,
		url : "rest/controller/upload/status",
		state : {
			pageSize : 15,
			sortKey : "updated",
			order : 1
		},
		queryParams: {
	        totalPages: null,
	        totalRecords: null,
	        sortKey: "sort"
		},
        parseState: function (resp, queryParams, state, options) {
        	return {totalRecords: resp.totalRecords};
        },
        // get the actual records
        parseRecords: function (resp, options) {
          return resp.items;
        }
	});
	
	var tableItems = new TableItems();
	
	var RetryCell = Backgrid.Cell.extend({
		templateRetry : _.template('<span class="btn btn-warning"><i class="glyphicon glyphicon-ban-circle"></i> <span>Retry</span></span>'),
		templateNoOp : _.template('<span class="badge badge-primary"><i class="glyphicon glyphicon-thumbs-up"></i></span>'),
		events : {
			"click" : "retry"
		},
		retry : function(e) {
			e.preventDefault();
			if(this.model.get('success') || this.model.get('preExists'))
				return;
			this.$el.html("<img src='img/loading.gif'>");
			this.model.post(this.model, this.$el);
		},
		render : function() {
			if(this.model.get('success') || this.model.get('preExists'))
				this.$el.html(this.templateNoOp());
			else
				this.$el.html(this.templateRetry());
			this.delegateEvents();
			return this;
		}
	});
	createTableColumns = function() {
		var cols = columns = [ {
			name : "name", // The key of the model attribute
			label : "Name", // The name to display in the header
			editable : false, // By default every cell in a column is editable, but *ID* shouldn't be
			cell : "string"
		}, {
			name : "email",
			label : "Email",
			// The cell type can be a reference of a Backgrid.Cell subclass, any Backgrid.Cell subclass instances like *id* above, or a string
			cell : "string" // This is converted to "StringCell" and a corresponding class in the Backgrid package namespace is looked up
		}, {
			name : "success",
			label : "Form Processed",
			editable : false,
			cell : "boolean" // An integer cell is a number cell that displays humanized integers
		}, {
			name : "preExists",
			label : "Already Submitted",
			editable : false,
			cell : "boolean" // A cell type for floating point value, defaults to have a precision 2 decimal numbers
		}, {
			name : "processingError",
			label : "Error",
			editable : false,
			sortable : false,
			cell : "string"
		}, {
			name : "form",
			cell : "string",
			renderable : false
		}, {
			name : "firstName",
			cell : "string",
			renderable : false
		}, {
			name : "lastName",
			cell : "string",
			renderable : false
		}, {
			name : "group",
			cell : "string",
			renderable : false
		}, {
			name : "affiliation",
			cell : "string",
			renderable : false
		}, {
			name : "retryNum",
			cell : "integer",
			renderable : false
		}, {
			name : "retry",
			label : "",
			editable : false,
			cell : RetryCell
		} ];
		return cols;
	}
	var columns = createTableColumns();
	var statusGrid = new Backgrid.Grid({
		columns : columns,
		collection : tableItems,
		emptyText : "No Data"
	});
	var statusPaginator = new Backgrid.Extension.Paginator({
        collection: tableItems
    });
    $("#successStatusTableDiv").append(statusGrid.render().$el);
    $("#successStatusTablePaginatorDiv").append(statusPaginator.render().$el);
    var interval;
    statusCheck = function(){
    	$('#workingDiv').css('display', 'block');
    	interval = setInterval(function(){
    		$("#successStatusTableDiv").empty();
    	    $("#successStatusTableDiv").append(statusGrid.render().$el);
    	    $("#successStatusTablePaginatorDiv").append(statusPaginator.render().$el);
			adjustpaginatorControl();
    	    tableItems.fetch({reset:true});
    		$.get("rest/controller/upload/status/isComplete",function(data) {
    			if(data == 'FINISH'){
    				clearInterval(interval);
    				$('#workingDiv').css('display', 'none');
    			}
    		});
    	}, 5000);
    }
    adjustpaginatorControl = function(){
    	$("a[title='First']").html("<<");
    	$("a[title='Previous']").html("<");
    	
    	$("a[title='Last']").html(">>");
    	$("a[title='Next']").html(">");
    }
    adjustpaginatorControl();
    tableItems.fetch({reset:true});
    var searchTableColumns = [{
        name: "name",
        label: "Name",
        // The cell type can be a reference of a Backgrid.Cell subclass, any Backgrid.Cell subclass instances like *id* above, or a string
        cell: "string" // This is converted to "StringCell" and a corresponding class in the Backgrid package namespace is looked up
      }, {
        name: "email",
        label: "Email",
        cell: "string" // An integer cell is a number cell that displays humanized integers
      },{
    	    name: "formApplications",
    	    label: "Form and Affiliation",
    	    formatter: _.extend({}, Backgrid.CellFormatter.prototype, {
    	        fromRaw: function (rawValue) {
    	        	var htmlStr = '';
    	        	$.each(rawValue, function(index, item){
    	        		htmlStr+='<li>'+item.formName+' - '+item.group+'</li>'
    	        	});
    	            return '<ul>'+htmlStr+'</ul>';
    	        }
    	    }),
    	    cell: "html"
      }
    ];
    var SearchElement = Backbone.Model.extend({});

    var SearchResult = Backbone.Collection.extend({
      model: SearchElement,
    });
    var searchResults = new SearchResult();
	var searchGrid = new Backgrid.Grid({
	  columns: searchTableColumns,
	  collection: searchResults
	});    
	function updateTable(data) {
		searchResults.reset();
		$('#searchTableDIV').append(searchGrid.render().$el)
		if (data.warning) {
			$.msgGrowl({
				type : 'warning',
				title : 'Warning',
				text : data.warningMessage
			});
			return;
		}
		if (data.error) {
			$.msgGrowl({
				type : 'error',
				title : 'Error',
				text : data.errorMessage
			});
			return;
		}
		searchResults.reset(data.result);
		$('#searchTableDIV').append(searchGrid.render().$el)
		$.msgGrowl({
			type : 'success',
			title : 'Success',
			text : data.successMessage
		});
	}	
	$('#searchTableDIV').append(searchGrid.render().$el);
</script>
<script src="js/main.js"></script>
</body>
</html>