import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.Socket;

import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.NTCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.ProxyClient;
import org.apache.http.protocol.HTTP;

/**
 * Example code for using {@link ProxyClient} in order to establish a tunnel through an HTTP proxy.
 */
public class ProxyClientDemo {

    public final static void main(String[] args) throws Exception {

        ProxyClient proxyClient = new ProxyClient();
        HttpHost target = new HttpHost("www.yahoo.com", 80);
        HttpHost proxy = new HttpHost("inet-proxy-a.appl.swissbank.com", 8085);        
		CredentialsProvider credentials = new BasicCredentialsProvider();
		credentials.setCredentials(new AuthScope("inet-proxy-a-appl.swissbank.com", 8085), new NTCredentials("guhade", "R00t9016", "W01B65QX", "UBSW"));        
        Socket socket = proxyClient.tunnel(proxy, null, credentials.getCredentials(AuthScope.ANY));
        try {
            Writer out = new OutputStreamWriter(socket.getOutputStream(), HTTP.DEF_CONTENT_CHARSET);
            out.write("GET / HTTP/1.1\r\n");
            out.write("Host: " + target.toHostString() + "\r\n");
            out.write("Agent: whatever\r\n");
            out.write("Connection: close\r\n");
            out.write("\r\n");
            out.flush();
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(socket.getInputStream(), HTTP.DEF_CONTENT_CHARSET));
            String line = null;
            while ((line = in.readLine()) != null) {
                System.out.println(line);
            }
        } finally {
            socket.close();
        }
    }

}