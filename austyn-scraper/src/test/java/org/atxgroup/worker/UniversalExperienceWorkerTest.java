package org.atxgroup.worker;

import static junit.framework.Assert.assertTrue;

import org.atxgroup.main.SystemUtils;
import org.atxgroup.persistence.model.NameRequest;
import org.codehaus.jackson.JsonNode;
import org.junit.Test;

public class UniversalExperienceWorkerTest extends BaseTest {
	@Test
	public void testDoWork() throws Exception {
		NameRequest req = new NameRequest();
		req.setName("Desmond Heynes");
		req.setEmail("dheynes@gmail.com");
		req.setAffiliation("Apple");
		UniversalExperienceWorker worker = new UniversalExperienceWorker(req, SystemUtils.isProxyEnabled(), "UNIVERSALEX");
		Object []returnResult = worker.call();
		JsonNode root = objMapper.readTree(returnResult[1].toString());
		assertTrue(root.has("response"));
		assertTrue(root.get("success").asBoolean());
	}

}
