package org.atxgroup.worker;

import static org.junit.Assert.assertTrue;

import org.atxgroup.main.SystemUtils;
import org.atxgroup.persistence.model.NameRequest;
import org.codehaus.jackson.JsonNode;
import org.junit.Test;

public class InsideMazdaWorkerTest extends BaseTest{
	@Test
	public void doWorkTest() throws Exception {
		NameRequest req = new NameRequest();
		req.setName("Dmitriy Ushakov");
		req.setEmail("usha.k.dimitr@gmail.com");
		req.setZipCode("78703");
		
		InsideMazdaWorker worker = new InsideMazdaWorker(req, SystemUtils.isProxyEnabled(), "MONTREALPARTYFEAT");
		Object []returnResult = worker.call();
		JsonNode root = objMapper.readTree(returnResult[1].toString());
		assertTrue(root.has("response"));
		assertTrue(root.get("success").asBoolean());
	}
}
