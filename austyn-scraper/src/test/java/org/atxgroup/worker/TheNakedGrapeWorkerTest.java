package org.atxgroup.worker;

import static junit.framework.Assert.assertTrue;

import org.atxgroup.main.SystemUtils;
import org.atxgroup.persistence.model.NameRequest;
import org.codehaus.jackson.JsonNode;
import org.junit.Test;

public class TheNakedGrapeWorkerTest extends BaseTest {

	@Test
	public void testDoWork() throws Exception {
		NameRequest req = new NameRequest();
		req.setName("Sidney Lebeouff");
		req.setEmail("lebusidney@peperonity.com");
		req.setAffiliation("Peperonity");
		TheNakedGrapeMusicWorker worker = new TheNakedGrapeMusicWorker(req, SystemUtils.isProxyEnabled(), "THENAKEDGRAPEMUSIC");
		Object []returnResult = worker.call();
		JsonNode root = objMapper.readTree(returnResult[1].toString());
		assertTrue(root.has("response"));
		assertTrue(root.get("success").asBoolean());
	}
}
