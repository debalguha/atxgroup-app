package org.atxgroup.worker;

import static org.junit.Assert.assertTrue;

import org.atxgroup.main.SystemUtils;
import org.atxgroup.persistence.model.NameRequest;
import org.codehaus.jackson.JsonNode;
import org.junit.Test;

public class AustynCityLimitsWorkerTest extends BaseTest{
	@Test
	public void testDoWork() throws Exception {
		NameRequest req = new NameRequest();
		req.setName("Paul Dirak");
		req.setEmail("pdirac@gmail.com");
		req.setCity("Austyn");
		req.setState("Texas");
		req.setZipCode("78702");
		req.setAffiliation("Freelancer");
		AustynCityLimitsWorker worker = new AustynCityLimitsWorker(req, SystemUtils.isProxyEnabled(), "MONTREALPARTYFEAT");
		Object []returnResult = worker.call();
		JsonNode root = objMapper.readTree(returnResult[1].toString());
		assertTrue(root.has("response"));
		assertTrue(root.get("success").asBoolean());
	}
}
