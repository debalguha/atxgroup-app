package org.atxgroup.worker;

import static junit.framework.Assert.assertTrue;

import org.atxgroup.main.SystemUtils;
import org.atxgroup.persistence.model.NameRequest;
import org.codehaus.jackson.JsonNode;
import org.junit.Test;

public class ReliveAustynWorkerTest extends BaseTest{

	@Test
	public void testDoWork() throws Exception {
		NameRequest req = new NameRequest();
		req.setName("Desmond Heynes");
		req.setEmail("dheynes@gmail.com");
		req.setAffiliation("Apple");
		ReliveAustynWorker worker = new ReliveAustynWorker(req, SystemUtils.isProxyEnabled(), "RELIVEAUSTYN");
		Object []returnResult = worker.call();
		JsonNode root = objMapper.readTree(returnResult[1].toString());
		assertTrue(root.has("response"));
		assertTrue(root.get("success").asBoolean());
	}

}
