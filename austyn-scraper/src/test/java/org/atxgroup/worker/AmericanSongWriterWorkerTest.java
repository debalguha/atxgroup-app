package org.atxgroup.worker;

import static org.junit.Assert.assertTrue;

import org.atxgroup.persistence.model.NameRequest;
import org.codehaus.jackson.JsonNode;
import org.junit.Test;

public class AmericanSongWriterWorkerTest extends BaseTest {
	@Test
	public void testDoWork() throws Exception {
		NameRequest req = new NameRequest();
		req.setName("Megankumar Deary");
		req.setEmail("deary_011@mailinator.com");
		AmericanSongWriterWorker worker = new AmericanSongWriterWorker(req, false, "AMERICANSONGWRITER");
		Object []returnResult = worker.call();
		System.out.println(((NameRequest)returnResult[0]).getProcessingError());
		JsonNode root = objMapper.readTree(returnResult[1].toString());
		assertTrue(root.has("response"));
		assertTrue(root.get("success").asBoolean());
	}

}
