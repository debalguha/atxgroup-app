package org.atxgroup.worker;

import static org.junit.Assert.assertTrue;

import org.atxgroup.persistence.model.NameRequest;
import org.codehaus.jackson.JsonNode;
import org.junit.Test;

public class VibesWorkerTest extends BaseTest {
	@Test
	public void testDoWork() throws Exception {
		NameRequest req = new NameRequest();
		req.setName("Desmond Heynes");
		req.setEmail("dheynes@gmail.com");
		VibesWorker worker = new VibesWorker(req, false, "VIBESWORKER");
		Object []returnResult = worker.call();
		System.out.println(((NameRequest)returnResult[0]).getProcessingError());
		JsonNode root = objMapper.readTree(returnResult[1].toString());
		assertTrue(root.has("response"));
		System.out.println(root.get("response").toString());
		assertTrue(root.get("success").asBoolean());
	}

}
