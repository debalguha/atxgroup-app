package org.atxgroup.worker;

import static junit.framework.Assert.assertTrue;

import org.atxgroup.main.SystemUtils;
import org.atxgroup.persistence.model.NameRequest;
import org.codehaus.jackson.JsonNode;
import org.junit.Test;

public class HeyerVerbWorkerTest extends BaseTest {
	@Test
	public void testDoWork() throws Exception{
		NameRequest req = new NameRequest();
		req.setName("Jesse Elliot");
		req.setEmail("jescarpellio@gmail.com");
		HeyerVerbWorker worker = new HeyerVerbWorker(req, SystemUtils.isProxyEnabled(), "HEYERVERB");
		Object []returnResult = worker.call();
		JsonNode root = objMapper.readTree(returnResult[1].toString());
		assertTrue(root.has("response"));
		assertTrue(root.get("success").asBoolean());
	}

}
