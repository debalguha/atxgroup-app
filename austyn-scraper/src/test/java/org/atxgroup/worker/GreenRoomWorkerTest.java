package org.atxgroup.worker;

import static org.junit.Assert.assertTrue;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.atxgroup.main.SystemUtils;
import org.atxgroup.persistence.model.NameRequest;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Test;

public class GreenRoomWorkerTest extends BaseTest {
	@Test
	public void testDoWork() throws Exception {
		NameRequest req = new NameRequest();
		req.setName("Desmond Heynes");
		req.setEmail("dheynes_1@gmail.com");
		ItsTheGreenRoomWorker worker = new ItsTheGreenRoomWorker(req, SystemUtils.isProxyEnabled(), "THEGREENROOM");
		Object []returnResult = worker.call();
		JsonNode root = objMapper.readTree(returnResult[1].toString());
		assertTrue(root.has("response"));
		assertTrue(root.get("success").asBoolean());
	}
	
	@Test
	public void testDecoder() throws Exception{
		String str_1 = "formId=52acb595e4b0e46ab6258b1d&collectionId=52acb575e4b0324b1f6347a2&form=%7B%22name-yui_3_10_1_1_1387050317539_12258%22%3A%5B%22Desmond%22%2C%22Heynes%22%5D%2C%22email-yui_3_10_1_1_1387050317539_12573%22%3A%22dheynes%40gmail.com%22%7D";
		String str_2 = "formID=52acb595e4b0e46ab6258b1d&collectionId=52acb575e4b0324b1f6347a2&form=%7B%22email-yui_3_10_1_1_1387050317539_12573%22%3A%22dheynes%40gmail.com%22%2C%22name-yui_3_10_1_1_1387050317539_12258%22%3A%5B%22Desmond%22%2C%22Heynes%22%5D%7D";
		System.out.println(URLDecoder.decode(str_1, "UTF-8"));
		System.out.println(URLDecoder.decode(str_2, "UTF-8"));
		Map<String, Object> postReqMap = new TreeMap<String, Object>(new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {
				return o2.compareTo(o1);
			}
		});
		List<String> nameList = new ArrayList<String>();
		nameList.add("Desmond");
		nameList.add("Heynes");
		postReqMap.put("name-yui_3_10_1_1_1387050317539_12258", nameList);
		postReqMap.put("email-yui_3_10_1_1_1387050317539_12573", "dheynes@gmail.com");
		System.out.println(new ObjectMapper().writeValueAsString(postReqMap));
	}

}
