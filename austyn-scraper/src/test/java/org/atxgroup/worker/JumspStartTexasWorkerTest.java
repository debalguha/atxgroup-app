package org.atxgroup.worker;

import static org.junit.Assert.assertTrue;

import org.atxgroup.main.SystemUtils;
import org.atxgroup.persistence.model.NameRequest;
import org.codehaus.jackson.JsonNode;
import org.junit.Test;

public class JumspStartTexasWorkerTest extends BaseTest{
	@Test
	public void testDoWork() throws Exception {
		NameRequest req = new NameRequest();
		req.setName("Desmond Heynes");
		req.setEmail("dheynes@gmail.com");
		JumpStartTexasWorker worker = new JumpStartTexasWorker(req, SystemUtils.isProxyEnabled(), "JUMPSTARTTEXAS");
		Object []returnResult = worker.call();
		JsonNode root = objMapper.readTree(returnResult[1].toString());
		assertTrue(root.has("response"));
		assertTrue(root.get("success").asBoolean());
	}

}
