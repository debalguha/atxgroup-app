package org.atxgroup.worker;

import org.atxgroup.main.SystemUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Before;

public class BaseTest {
	protected static ObjectMapper objMapper;
	@Before
	public void setUp() throws Exception {
		SystemUtils.setDefaults();
		objMapper = new ObjectMapper();
	}
}
