package org.atxgroup.worker;

import static org.junit.Assert.assertTrue;

import org.atxgroup.persistence.model.NameRequest;
import org.codehaus.jackson.JsonNode;
import org.junit.Test;

public class WindishAgencyWorkerTest extends BaseTest{
	@Test
	public void testDoWork() throws Exception {
		NameRequest req = new NameRequest();
		req.setName("Dhiraj Mehta");
		req.setEmail("dmehta@gmail.com");
		WindishAgencyWorker worker = new WindishAgencyWorker(req, false, "WINDISHAGENCY");
		Object []returnResult = worker.call();
		JsonNode root = objMapper.readTree(returnResult[1].toString());
		assertTrue(root.has("response"));
		assertTrue(root.get("success").asBoolean());
	}

}
