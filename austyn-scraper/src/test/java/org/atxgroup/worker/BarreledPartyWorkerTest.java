package org.atxgroup.worker;

import static org.junit.Assert.assertTrue;

import org.atxgroup.main.SystemUtils;
import org.atxgroup.persistence.model.NameRequest;
import org.codehaus.jackson.JsonNode;
import org.junit.Test;

public class BarreledPartyWorkerTest extends BaseTest{
	@Test
	public void testDoWork() throws Exception {
		NameRequest req = new NameRequest();
		req.setName("Megan Deary");
		req.setEmail("deary_011@mailinator.com");
		req.setCity("Austin");
		req.setState("Texas");
		req.setZipCode("78702");
		req.setAffiliation("Developer");
		BarreledPartyWorker worker = new BarreledPartyWorker(req, SystemUtils.isProxyEnabled(), "BARRELEDPARTY");
		Object []returnResult = worker.call();
		JsonNode root = objMapper.readTree(returnResult[1].toString());
		assertTrue(root.has("response"));
		assertTrue(root.get("success").asBoolean());
	}
}
