package org.atxgroup.worker;

import static org.junit.Assert.assertTrue;

import org.atxgroup.main.SystemUtils;
import org.atxgroup.persistence.model.NameRequest;
import org.codehaus.jackson.JsonNode;
import org.junit.Test;

public class WhatSceneWorkerTest extends BaseTest{
	@Test
	public void testDoWork() throws Exception {
		NameRequest req = new NameRequest();
		req.setName("Tapan Mukherjee");
		req.setEmail("mukherjeetapank@gmail.com");
		WhatSceneWorker worker = new WhatSceneWorker(req, SystemUtils.isProxyEnabled(), "MONTREALPARTYFEAT");
		Object []returnResult = worker.call();
		JsonNode root = objMapper.readTree(returnResult[1].toString());
		assertTrue(root.has("response"));
		assertTrue(root.get("success").asBoolean());
	}
}
