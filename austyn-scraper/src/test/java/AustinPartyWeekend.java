/*
 * $HeadURL$
 * $Revision$
 * $Date$
 * ====================================================================
 *
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 * [Additional notices, if required by prior licensing conditions]
 *
 */

import java.io.IOException;
import java.net.URLEncoder;
import java.util.List;

import org.apache.http.Consts;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.NTCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CookieStore;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.cookie.Cookie;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * This is a sample application that demonstrates
 * how to use the Jakarta HttpClient API.
 *
 * This application sets an HTTP cookie and
 * updates the cookie's value across multiple
 * HTTP GET requests.
 *
 * @author Sean C. Sullivan
 * @author Oleg Kalnichevski
 *
 */
public class AustinPartyWeekend {

    /**
     *
     * Usage:
     *          java CookieDemoApp http://mywebserver:80/
     *
     *  @param args command line arguments
     *                 Argument 0 is a URL to a web server
     *
     *
     */
    public final static void main(String[] args) throws Exception {
        //CloseableHttpClient httpclient = HttpClients.createDefault();
//    	HttpClient httpClient = getHttpClient();
    	HttpClientContext context = HttpClientContext.create();
    	CloseableHttpClient httpClient = geClosabletHttpClient(context);
        try {
        	System.out.println(URLEncoder.encode("http://gimme.io/austinpartyweekend?embed=1&embed_url=http%3A%2F%2Faustinpartyweekend.com%2F&mode=popup&ts=Fri%20Jan%2010%202014%2002:01:34%20GMT+0530%20(India%20Standard%20Time)", "UTF-8"));
            //HttpGet httpget = new HttpGet("http://www.google.com/");
        	HttpGet httpget = new HttpGet("http://gimme.io/austinpartyweekend?embed=1&embed_url=http%3A%2F%2Faustinpartyweekend.com%2F&mode=popup&ts=Fri%20Jan%2010%202014%2002:01:34%20GMT+0530%20(India%20Standard%20Time)");
        	/*HttpPut httpput = new HttpPut("http://gimme.io/api/rsvp");
        	httpput.*/
            // Create a custom response handler
            ResponseHandler<String> responseHandler = new ResponseHandler<String>() {

                public String handleResponse(
                        final HttpResponse response) throws ClientProtocolException, IOException {
                    int status = response.getStatusLine().getStatusCode();
                    Header []allHeaders = response.getAllHeaders();
                    for(Header header : allHeaders)
                    	System.out.println(header.getName()+"--"+header.getValue());
                    if (status >= 200 && status < 303) {
                        HttpEntity entity = response.getEntity();
                        return entity != null ? EntityUtils.toString(entity) : null;
                    } else {
                        throw new ClientProtocolException("Unexpected response status: " + status);
                    }
                }

            };
            httpClient.execute(httpget, responseHandler, context);
            System.out.println("----------------------------------------");
            //System.out.println(responseBody);
            System.out.println("----------------------------------------");

            String imp_id="";
            String imp_key="";
            
            List<Cookie> cookies = context.getCookieStore().getCookies();
            for(Cookie cookie : cookies){
            	System.out.println(cookie.getName()+"--"+cookie.getValue());
            	if(cookie.getName().equalsIgnoreCase("gimme_imp_id"))
            		imp_id = cookie.getValue();
            	if(cookie.getName().equalsIgnoreCase("gimme_imp_key"))
            		imp_key = cookie.getValue();
            }
            RequestObject obj = new RequestObject("rsvp", "aryanmitra9113@yahoo.com", imp_id, imp_key, "Aryan Kumar Mitra", false);
            String jsonObject = new ObjectMapper().writeValueAsString(obj);
            System.out.println("JsonObject:: "+jsonObject);
            StringEntity entity = new StringEntity(jsonObject,
                    ContentType.create("application/json", Consts.UTF_8));
            //HttpPost httppost = new HttpPost("http://gimme.io/api/rsvp");
            HttpPut httpput = new HttpPut("http://gimme.io/api/rsvp");
            httpput.setEntity(entity);
            String postResponse = httpClient.execute(httpput, responseHandler, context);
            System.out.println(postResponse);
        } finally {
            httpClient.close();
        }
    }

	private static CloseableHttpClient geClosabletHttpClient(HttpClientContext context) {
	    RequestConfig globalConfig = RequestConfig.custom().setCookieSpec(CookieSpecs.BEST_MATCH).build();
	    CookieStore cookieStore = new BasicCookieStore();
	    context.setCookieStore(cookieStore);
	    HttpHost proxy = new HttpHost("", 8085);
	    CloseableHttpClient httpClient = HttpClients.custom().setDefaultRequestConfig(globalConfig).setDefaultCookieStore(cookieStore).setProxy(proxy).build();	
	    
	    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	    credsProvider.setCredentials(new AuthScope("", 8085), new NTCredentials("guhade", "R00t9016", "", ""));
	    context.setCredentialsProvider(credsProvider);
	    //httpClient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);
		return httpClient;
		
	}	
}
class RequestObject{
	private final String action;
	private final String email;
	private final String impression_id;
	private final String security_hash;
	private final String name;
	private final boolean subscribe;
	public RequestObject(String action, String email, String impression_id, String security_hash, String name, boolean subscribe) {
		super();
		this.action = action;
		this.email = email;
		this.impression_id = impression_id;
		this.security_hash = security_hash;
		this.name = name;
		this.subscribe = subscribe;
	}
	public String getAction() {
		return action;
	}
	public String getEmail() {
		return email;
	}
	public String getImpression_id() {
		return impression_id;
	}
	public String getSecurity_hash() {
		return security_hash;
	}
	public String getName() {
		return name;
	}
	public boolean isSubscribe() {
		return subscribe;
	}
	
}
