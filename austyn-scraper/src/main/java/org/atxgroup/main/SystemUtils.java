package org.atxgroup.main;

public class SystemUtils {
	public static String getNTLMProxyHost(){
		return System.getProperty("http.proxy.host");
	}
	public static String getNTLMProxyport(){
		return System.getProperty("http.proxy.port");
	}
	public static String getNTLMProxyUser(){
		return System.getProperty("http.proxy.username");
	}	
	public static String getNTLMProxyPassword(){
		return System.getProperty("http.proxy.password");
	}
	public static String getNTLMProxyWorkstation(){
		return System.getProperty("http.proxy.workstation");
	}
	public static String getNTLMProxyDomain(){
		return System.getProperty("http.proxy.domain");
	}
	public static boolean isProxyEnabled(){
		return Boolean.getBoolean("http.proxy.state");
	}
	public static void setDefaults(){
		System.setProperty("http.proxy.host", "inet-proxy-b.appl.swissbank.com");
		System.setProperty("http.proxy.port", "8085");
		System.setProperty("http.proxy.username", "guhade");
		System.setProperty("http.proxy.password", "R00t9016");
		System.setProperty("http.proxy.workstation", "W01B65QX");
		System.setProperty("http.proxy.domain", "UBSW");
		System.setProperty("http.proxy.state", "false");
	}
}
