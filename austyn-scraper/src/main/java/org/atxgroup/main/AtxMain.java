package org.atxgroup.main;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.atxgroup.names.cache.NamesCache;
import org.atxgroup.persistence.model.NameRequest;
import org.atxgroup.persistence.model.NameRequestApplicationForm;
import org.atxgroup.persistence.model.SubmitStatus;
import org.atxgroup.persistence.service.NameService;
import org.atxgroup.worker.GenericWorker;
import org.atxgroup.worker.ResultObject;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class AtxMain implements InitializingBean {
	@Autowired
	private NameService nameService;
	@Autowired
	private NamesCache cache;
	@Resource(name = "form-class-map")
	private Map<String, String> formNameClassMap;
	@Resource(name = "form-proxy-map")
	private Map<String, Boolean> formNameProxyMap;
	@Value("${worker.pool.size}")
	private int poolSize;

	private static final Log logger = LogFactory.getLog(AtxMain.class);
	private Executor e;
	private CompletionService<Object[]> csReq;

	public AtxMain(NameService nameService) {
		this.nameService = nameService;
	}

	public AtxMain(NameService nameService, NamesCache namesCache) {
		this.nameService = nameService;
		this.cache = namesCache;
	}

	public AtxMain() {

	}

	public void startScraping(File csvFile, Queue<ResultObject> sessionQueue, String formName) throws Exception {
		List<String> lines = FileUtils.readLines(csvFile);
		startScraping(lines, sessionQueue, formName);
	}

	@SuppressWarnings("unchecked")
	public void startScraping(List<String> lines, Queue<ResultObject> sessionQueue, String formName)
			throws InterruptedException, ExecutionException, ClassNotFoundException, InstantiationException,
			IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException,
			SecurityException {
		logger.info("Total " + lines.size() + " records to create.");
		List<NameRequest> skippedNames = new ArrayList<NameRequest>();
		List<NameRequestApplicationForm> batchUpdateGroupNames = new ArrayList<NameRequestApplicationForm>();
		Iterator<String> lineItr = lines.iterator();
		lineItr.next();
		lineItr.remove();
		int futureCount = 0;
		List<Future<Object[]>> futures = new ArrayList<Future<Object[]>>();
		Class<GenericWorker> classToLoad = (Class<GenericWorker>) Class.forName(formNameClassMap.get(formName));
		boolean isProxyEnabled = formNameProxyMap.get(formName) == null ? false : formNameProxyMap.get(formName);
		for (String line : lines) {
			if (line == null || line.trim().isEmpty())
				continue;
			futureCount++;
			String[] splits = line.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
			NameRequest reqObj = findNameRequestObjectIfExists(splits[3]);
			if (reqObj == null) {
				reqObj = new NameRequest();
				reqObj.setEmail(splits[3]);
			}
			reqObj.setFirstName(splits[1]);
			reqObj.setLastName(splits[2]);
			reqObj.setName(splits[1].concat(" ").concat(splits[2]));
			logger.info("Setting group to while creating object:: " + splits[0]);
			reqObj.setGroupName(splits[0]);
			reqObj.setCity(splits[4]);
			reqObj.setState(splits[5]);
			reqObj.setZipCode(splits[6]);
			reqObj.setAffiliation(splits[7]);
			if (isAlreadySubmitted(reqObj, formName)) {
				logger.info("Record already submitted.");
				NameRequestApplicationForm application = findApplicationForm(reqObj, formName);
				if (application.getGroup() == null || application.getGroup().isEmpty()) {
					logger.info("Application form found, but no group info. Setting group to: " + splits[0]);
					application.setGroup(splits[0]);
					application.setAffiliation(splits[4]);
					batchUpdateGroupNames.add(application);
				} else
					skippedNames.add(reqObj);
				continue;
			}
			futures.add(csReq.submit(classToLoad.getConstructor(NameRequest.class, Boolean.class, String.class)
					.newInstance(reqObj, isProxyEnabled, formName)));
		}
		if (batchUpdateGroupNames != null && !batchUpdateGroupNames.isEmpty()) {
			logger.info("Going to batch update groups for " + batchUpdateGroupNames.size());
			try {
				nameService.batchUpdateGroupName(batchUpdateGroupNames);
				for (NameRequestApplicationForm applicationForm : batchUpdateGroupNames) {
					ResultObject obj = new ResultObject();
					obj.setSuccess(false);
					obj.setProcessed(true);
					obj.setPreExists(true);
					obj.setProcessingError("Already submitted, but group updated.");
					obj.setName(applicationForm.getNameRequest().getName());
					obj.setEmail(applicationForm.getNameRequest().getEmail());
					obj.setFirstName(applicationForm.getNameRequest().getFirstName());
					obj.setLastName(applicationForm.getNameRequest().getLastName());
					obj.setAffiliation(applicationForm.getNameRequest().getAffiliation());
					obj.setGroupName(applicationForm.getNameRequest().getGroupName());
					obj.setCity(applicationForm.getNameRequest().getCity());
					obj.setState(applicationForm.getNameRequest().getState());
					obj.setZipCode(applicationForm.getNameRequest().getZipCode());
					obj.setForm(formName);
					sessionQueue.offer(obj);
					cache.updateCache(applicationForm.getNameRequest());
				}
				logger.info("Batch update executed.");
			} catch (Exception e) {
				logger.error("Error while updating group names.", e);
				String processingError = "Error while updating group names. Please contact support";
				for (NameRequestApplicationForm applicationForm : batchUpdateGroupNames) {
					ResultObject obj = new ResultObject();
					obj.setSuccess(false);
					obj.setProcessed(true);
					obj.setPreExists(true);
					obj.setProcessingError(processingError);
					obj.setName(applicationForm.getNameRequest().getName());
					obj.setEmail(applicationForm.getNameRequest().getEmail());
					obj.setFirstName(applicationForm.getNameRequest().getFirstName());
					obj.setLastName(applicationForm.getNameRequest().getLastName());
					obj.setAffiliation(applicationForm.getNameRequest().getAffiliation());
					obj.setGroupName(applicationForm.getNameRequest().getGroupName());
					obj.setCity(applicationForm.getNameRequest().getCity());
					obj.setState(applicationForm.getNameRequest().getState());
					obj.setZipCode(applicationForm.getNameRequest().getZipCode());
					obj.setForm(formName);
					sessionQueue.offer(obj);
				}
				logger.info("Processing error set");
			}
		}
		addSkippedNamesToSession(skippedNames, sessionQueue, formName);
		logger.info("Total futures " + futureCount + ", SIZE=" + futures.size());
		ObjectMapper mapper = new ObjectMapper();
		for (Future<Object[]> future : futures) {
			Object[] ret = future.get();
			NameRequest returnObj = (NameRequest) ret[0];
			ResultObject obj = new ResultObject();
			obj.setProcessed(true);
			obj.setName(returnObj.getName());
			obj.setEmail(returnObj.getEmail());
			obj.setFirstName(returnObj.getFirstName());
			obj.setLastName(returnObj.getLastName());
			obj.setAffiliation(returnObj.getAffiliation());
			obj.setGroupName(returnObj.getGroupName());
			obj.setCity(returnObj.getCity());
			obj.setState(returnObj.getState());
			obj.setZipCode(returnObj.getZipCode());
			obj.setForm(formName);
			String resultJSON = (String) ret[1];
			logger.debug("Retrun:: " + resultJSON);
			if (resultJSON != null && !resultJSON.isEmpty()) {
				Map<String, Object> mapObj = null;
				try {
					mapObj = mapper.readValue(resultJSON, Map.class);
				} catch (Exception e) {
					logger.error("Unable to unmarshall json into Map", e);
				}
				if (mapObj != null && mapObj.get("response") != null)
					obj.setSuccess((Boolean) mapObj.get("success"));
				else {
					obj.setSuccess(false);
					logger.info("Converted json map is null or no response.");
				}
			} else
				logger.info("Result JSON is null");
			obj.setProcessingError(returnObj.getProcessingError());
			try {
				if (obj.isSuccess()) {
					logger.info("Creating entry for " + formName + " with group: " + returnObj.getGroupName()
							+ ", affiliation: " + returnObj.getAffiliation());
					nameService.createNameAndSubmitApplicationForm(returnObj, formName, resultJSON, SubmitStatus.SUCCESS);
				} else {
					if (returnObj.getID() <= 0)
						nameService.createName(returnObj);
				}
				cache.updateCache(returnObj);
			} catch (Exception e) {
				logger.error("Error while creating entity.", e);
				String processingError = obj.getProcessingError() == null ? "" : obj.getProcessingError();
				obj.setProcessingError(
						processingError.concat(" There has been an error while submitting application."));
				logger.info("Processing error set");
			}
			if (sessionQueue.contains(obj))
				sessionQueue.remove(obj);
			sessionQueue.offer(obj);
			futureCount--;
		}
		/*
		 * ResultObject obj = new ResultObject(); obj.setFinished(true);
		 * obj.setName("ZZZZZZZZZ"); obj.setEmail("zzzzzzz@zzzz.zzz");
		 * sessionQueue.offer(obj);
		 */
		logger.info("Processed return results for all futures.");
	}

	@SuppressWarnings("unchecked")
	public void startScraping(Collection<NameRequest> reqs, Queue<ResultObject> sessionQueue, String formName)
			throws InterruptedException, ExecutionException, ClassNotFoundException, InstantiationException,
			IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException,
			SecurityException {
		boolean isCacheDirty = false;
		logger.info("Total " + reqs.size() + " records to create.");
		List<NameRequest> skippedNames = new ArrayList<NameRequest>();
		List<NameRequestApplicationForm> batchUpdateGroupNames = new ArrayList<NameRequestApplicationForm>();
		int futureCount = 0;
		List<Future<Object[]>> futures = new ArrayList<Future<Object[]>>();
		Class<GenericWorker> classToLoad = (Class<GenericWorker>) Class.forName(formNameClassMap.get(formName));
		boolean isProxyEnabled = formNameProxyMap.get(formName) == null ? false : formNameProxyMap.get(formName);
		for (NameRequest reqObjVar : reqs) {
			NameRequest reqObj = findNameRequestObjectIfExists(reqObjVar.getEmail());
			if (reqObj == null)
				reqObj = reqObjVar;
			else {
				if (isAlreadySubmitted(reqObj, formName)) {
					logger.info("Record already submitted.");
					NameRequestApplicationForm application = findApplicationForm(reqObj, formName);
					if (application.getGroup() == null || application.getGroup().isEmpty()) {
						logger.info("Application form found, but no group info. Setting group to: "
								+ reqObjVar.getGroupName());
						application.setGroup(reqObjVar.getGroupName());
						application.setAffiliation(reqObjVar.getAffiliation());
						batchUpdateGroupNames.add(application);
					} else
						skippedNames.add(reqObj);
					continue;
				} else {
					reqObj.setGroupName(reqObjVar.getGroupName());
					reqObj.setAffiliation(reqObjVar.getAffiliation());
				}
			}
			futures.add(csReq.submit(classToLoad.getConstructor(NameRequest.class, Boolean.class, String.class)
					.newInstance(reqObj, isProxyEnabled, formName)));
		}
		if (batchUpdateGroupNames != null && !batchUpdateGroupNames.isEmpty()) {
			logger.info("Going to batch update groups for " + batchUpdateGroupNames.size());
			try {
				nameService.batchUpdateGroupName(batchUpdateGroupNames);
				isCacheDirty = true;
				for (NameRequestApplicationForm applicationForm : batchUpdateGroupNames) {
					ResultObject obj = new ResultObject();
					obj.setSuccess(false);
					obj.setProcessed(true);
					obj.setPreExists(true);
					obj.setProcessingError("Already submitted, but group updated.");
					obj.setName(applicationForm.getNameRequest().getName());
					obj.setEmail(applicationForm.getNameRequest().getEmail());
					obj.setFirstName(applicationForm.getNameRequest().getFirstName());
					obj.setLastName(applicationForm.getNameRequest().getLastName());
					obj.setAffiliation(applicationForm.getNameRequest().getAffiliation());
					obj.setGroupName(applicationForm.getNameRequest().getGroupName());
					sessionQueue.offer(obj);
				}
				logger.info("Batch update executed.");
			} catch (Exception e) {
				logger.error("Error while updating group names.", e);
				String processingError = "Error while updating group names. Please contact support";
				for (NameRequestApplicationForm applicationForm : batchUpdateGroupNames) {
					ResultObject obj = new ResultObject();
					obj.setSuccess(false);
					obj.setProcessed(true);
					obj.setPreExists(true);
					obj.setProcessingError(processingError);
					obj.setName(applicationForm.getNameRequest().getName());
					obj.setEmail(applicationForm.getNameRequest().getEmail());
					obj.setFirstName(applicationForm.getNameRequest().getFirstName());
					obj.setLastName(applicationForm.getNameRequest().getLastName());
					obj.setAffiliation(applicationForm.getNameRequest().getAffiliation());
					obj.setGroupName(applicationForm.getNameRequest().getGroupName());
					sessionQueue.offer(obj);
				}
				logger.info("Processing error set");
			}
		}
		addSkippedNamesToSession(skippedNames, sessionQueue, formName);
		logger.info("Total futures " + futureCount);
		ObjectMapper mapper = new ObjectMapper();
		for (Future<Object[]> future : futures) {
			Object[] ret = future.get();
			NameRequest returnObj = (NameRequest) ret[0];
			ResultObject obj = new ResultObject();
			obj.setProcessed(true);
			obj.setName(returnObj.getName());
			obj.setEmail(returnObj.getEmail());
			obj.setFirstName(returnObj.getFirstName());
			obj.setLastName(returnObj.getLastName());
			obj.setAffiliation(returnObj.getAffiliation());
			obj.setGroupName(returnObj.getGroupName());
			String resultJSON = (String) ret[1];
			logger.info("Retrun:: " + resultJSON);
			if (resultJSON != null && !resultJSON.isEmpty()) {
				Map<String, Object> mapObj = null;
				try {
					mapObj = mapper.readValue(resultJSON, Map.class);
				} catch (Exception e) {
					logger.error("Unable to unmarshall json into Map", e);
				}
				if (mapObj != null && mapObj.get("response") != null)
					obj.setSuccess((Boolean) mapObj.get("success"));
				else {
					obj.setSuccess(false);
					logger.info("Converted json map is null or no response.");
				}
				if (!obj.isSuccess())
					obj.setProcessed(false);
			} else
				logger.info("Result JSON is null");
			obj.setProcessingError(returnObj.getProcessingError());
			try {
				if (obj.isSuccess()) {
					logger.info("Creating entry for " + formName + " with group: " + returnObj.getGroupName()
							+ ", affiliation: " + returnObj.getAffiliation());
					nameService.createNameAndSubmitApplicationForm(returnObj, formName, resultJSON, SubmitStatus.FAIL);
				} else {
					if (returnObj.getID() <= 0)
						nameService.createName(returnObj);
				}
				isCacheDirty = true;
			} catch (Exception e) {
				logger.error("Error while creating entity.", e);
				String processingError = obj.getProcessingError() == null ? "" : obj.getProcessingError();
				obj.setProcessingError(
						processingError.concat(" There has been an error while submitting application."));
				logger.info("Processing error set");
			}
			if (sessionQueue.contains(obj))
				sessionQueue.remove(obj);
			sessionQueue.offer(obj);
			futureCount--;
		}
		if (isCacheDirty)
			cache.refresh();
		logger.info("Processed return results for all futures.");
	}

	private NameRequestApplicationForm findApplicationForm(NameRequest name, String formName) {
		Set<NameRequestApplicationForm> formsSubmitted = name.getApplications();
		for (NameRequestApplicationForm requestApplicationForm : formsSubmitted) {
			if (requestApplicationForm.getApplicationForm().getFormName().equals(formName))
				return requestApplicationForm;
		}
		return null;
	}

	private NameRequest findNameRequestObjectIfExists(String email) {
		if (cache != null) {
			logger.info("Cache is not null!! Looking into cache");
			NameRequest searchedName = cache.searchInEmailCacheForExactMatch(email);
			if (searchedName != null) {
				logger.info("Present in cache.");
				return searchedName;
			}
			logger.warn("Not found in cache.");
			return null;
		} else
			return nameService.findNameRequestByEmail(email);
	}

	private void addSkippedNamesToSession(List<NameRequest> skippedNames, Queue<ResultObject> sessionQueue,
			String formName) {
		ResultObject obj = null;
		for (NameRequest name : skippedNames) {
			obj = new ResultObject();
			obj.setEmail(name.getEmail());
			obj.setSuccess(false);
			obj.setName(name.getName());
			obj.setPreExists(true);
			obj.setProcessed(false);
			populateFirstAndLastName(name);
			Set<NameRequestApplicationForm> applications = name.getApplications();
			for (NameRequestApplicationForm form : applications) {
				if (form.getApplicationForm().getFormName().equals(formName)) {
					obj.setGroupName(form.getGroup());
					obj.setAffiliation(form.getAffiliation());
				}
			}
			obj.setProcessingError("Already submitted!");
			if (sessionQueue.contains(obj))
				sessionQueue.remove(obj);
			sessionQueue.offer(obj);
		}
	}

	private boolean isAlreadySubmitted(NameRequest name, String formName) {
		Set<NameRequestApplicationForm> formsSubmitted = name.getApplications();
		if(formsSubmitted == null)
			return false;
		for (NameRequestApplicationForm requestApplicationForm : formsSubmitted) {
			if (requestApplicationForm.getApplicationForm().getFormName().equals(formName))
				return true;
		}
		return false;
	}

	public void setNameService(NameService nameService) {
		this.nameService = nameService;
	}

	public void setCache(NamesCache cache) {
		this.cache = cache;
	}

	public void setFormNameClassMap(Map<String, String> formNameClassMap) {
		this.formNameClassMap = formNameClassMap;
	}

	public void setPoolSize(int poolSize) {
		this.poolSize = poolSize;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		e = new ThreadPoolExecutor(poolSize, poolSize, Long.MAX_VALUE, TimeUnit.NANOSECONDS,
				new LinkedBlockingQueue<Runnable>());
		csReq = new ExecutorCompletionService<Object[]>(e);
	}

	public void setFormNameProxyMap(Map<String, Boolean> formNameProxyMap) {
		this.formNameProxyMap = formNameProxyMap;
	}

	protected void populateFirstAndLastName(NameRequest requestObject) {
		String firstName = "";
		String lastName = "";
		if (requestObject.getFirstName() == null || requestObject.getLastName() == null) {
			String[] splits = requestObject.getName().split("\\s");
			firstName = splits[0];
			if (splits.length > 2) {
				StringBuilder lastNameBuilder = new StringBuilder();
				for (int i = 1; i < splits.length; i++) {
					lastNameBuilder.append(splits[i]);
					if (i < splits.length - 1)
						lastNameBuilder.append(" ");
				}
				lastName = lastNameBuilder.toString();
			} else if (splits.length == 2)
				lastName = splits[1];
			requestObject.setFirstName(firstName);
			requestObject.setLastName(lastName);
		}
	}
}
