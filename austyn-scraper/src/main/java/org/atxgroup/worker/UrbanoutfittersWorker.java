package org.atxgroup.worker;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.atxgroup.persistence.model.NameRequest;

public class UrbanoutfittersWorker extends GimmeWorker {
	private static final Log logger = LogFactory.getLog(UrbanoutfittersWorker.class);
	private static final String SITE_URL = "http://gimme.io/AustinAfterfest?embed=1&embed_url=http%3A%2F%2Fwww.urbanoutfitters.com%2Furban%2Fcatalog%2Fcategory.jsp%3Fid%3DAFTERFEST%2603052014&mode=popup&ts="+System.currentTimeMillis();
	public UrbanoutfittersWorker(NameRequest requestObject, Boolean proxyEnabled, String formName) {
		super(requestObject, proxyEnabled, formName);
	}

	@Override
	public Log getLogger() {
		return logger;
	}

	@Override
	public String getSiteURL() {
		return SITE_URL;
	}

}
