package org.atxgroup.worker;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.atxgroup.persistence.model.NameRequest;

public class BrooklynCountryCantinaWorker extends AbstractMultiPartGetHTMLAndPostWorker {

	private static final Log logger = LogFactory.getLog(HeyerVerbWorker.class);
	private static final String SITE_URL = "http://brooklyncountrycantina.com/";
	private static final String SUBMIT_URL = "http://brooklyncountrycantina.com/";
	
	private static final String FIRST_NAME_KEY = "input_1.3";
	private static final String LAST_NAME_KEY = "input_1.6";
	private static final String EMAIL_KEY = "input_2";
	
	private static final Map<String, String> paramXpathMap = new HashMap<String, String>();
	static{
		paramXpathMap.put("is_submit_1", "//input[@name='is_submit_1']/@value");
		paramXpathMap.put("gform_submit", "//input[@name='gform_submit']/@value");
		paramXpathMap.put("gform_unique_id", "//input[@name='gform_unique_id']/@value");
		paramXpathMap.put("state_1", "//input[@name='state_1']/@value");
		paramXpathMap.put("gform_target_page_number_1", "//input[@name='gform_target_page_number_1']/@value");//select[@name="degenerativeGuest[0].count"]/option[@selected]/@value
		paramXpathMap.put("gform_source_page_number_1", "//input[@name='gform_source_page_number_1']/@value");
		paramXpathMap.put("gform_field_values", "//input[@name='gform_field_values']/@value");
		paramXpathMap.put("_registrant.addRegistrantAsContact", "//input[@name='_registrant.addRegistrantAsContact']/@value");
		paramXpathMap.put("eventId", "//input[@name='eventId']/@value");
		paramXpathMap.put("eventKey", "//input[@name='eventKey']/@value");
		paramXpathMap.put("registrant.jmmlListId", "//input[@name='registrant.jmmlListId']/@value");
	}
	
	public BrooklynCountryCantinaWorker(NameRequest requestObject, Boolean proxyEnabled, String formName) {
		super(requestObject, proxyEnabled, formName);
	}


	@Override
	public List<NameValuePair> buildNameValuePair(String... html) throws Exception {
		List<NameValuePair> pairs = new ArrayList<NameValuePair>();
		pairs.add(new BasicNameValuePair(FIRST_NAME_KEY, requestObject.getFirstName()));
		pairs.add(new BasicNameValuePair(LAST_NAME_KEY, requestObject.getLastName()));
		pairs.add(new BasicNameValuePair(EMAIL_KEY, requestObject.getEmail()));
		for(Map.Entry<String, String> entry : paramXpathMap.entrySet())
			pairs.add(new BasicNameValuePair(entry.getKey(), getXPathOutputFromHtml(html[0], entry.getValue())));
		logger.debug(pairs);
		return pairs;
	}

	@Override
	public void addHeaders(HttpPost httpPost) {

	}

	@Override
	public long getSleepIntervalInMilliseconds() {
		return 500;
	}

	@Override
	public void buildResponse(Map<String, Object> retMap, String str) throws Exception {
		// TODO Auto-generated method stub You have successfully registered
		if(str.contains("Thanks for RSVP'ing")){
			retMap.put("success", new Boolean(true));
			retMap.put("response", "You have successfully registered");
		}else{
			logger.warn("Failed!!");
			logger.warn(str);
			retMap.put("success", new Boolean(false));
		}

	}

	@Override
	public Log getLogger() {
		return logger;
	}

	@Override
	public String getSiteURL() {
		// TODO Auto-generated method stub
		return SITE_URL;
	}


	@Override
	public String getSubmitURL() {
		return SUBMIT_URL;
	}

}
