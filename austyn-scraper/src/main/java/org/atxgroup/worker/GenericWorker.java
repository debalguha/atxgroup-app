package org.atxgroup.worker;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.logging.Log;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.NTCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CookieStore;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.conn.HttpClientConnectionManager;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.protocol.HttpContext;
import org.atxgroup.main.SystemUtils;
import org.atxgroup.persistence.model.NameRequest;
import org.w3c.dom.Document;
import org.w3c.tidy.Tidy;

public abstract class GenericWorker implements Callable<Object[]>{
	protected final NameRequest requestObject;
	protected final boolean proxyEnabled;
	protected final String formName;
	public abstract Log getLogger();
	public GenericWorker(NameRequest requestObject, Boolean proxyEnabled, String formName){
		this.requestObject = requestObject;
		this.proxyEnabled = proxyEnabled;
		this.formName = formName;
	}
	public Object[] call() throws Exception {
		Object []toRet = new Object[2];
		toRet[0] = requestObject;
		try {
			toRet[1] = doWork(); 
			getLogger().debug(toRet[1]);
		} catch (Exception e) {
			getLogger().error("Unable to process "+requestObject, e);
			requestObject.setProcessingError(e.getMessage());
			toRet[1] = "No Result Obtained";
		}
		return toRet;
	}
	protected CloseableHttpClient geClosabletHttpClient(HttpClientContext context, HttpClientConnectionManager connectionManager) {
		CloseableHttpClient httpClient = null;
		RequestConfig globalConfig = RequestConfig.custom().setCookieSpec(CookieSpecs.BEST_MATCH).build();
		CookieStore cookieStore = new BasicCookieStore();
		context.setCookieStore(cookieStore);
		if (proxyEnabled) {
			HttpHost proxy = new HttpHost(SystemUtils.getNTLMProxyHost(), Integer.parseInt(SystemUtils.getNTLMProxyport()));
			httpClient = HttpClients.custom().setDefaultRequestConfig(globalConfig).setDefaultCookieStore(cookieStore).setConnectionManager(connectionManager).setProxy(proxy).build();
			CredentialsProvider credsProvider = new BasicCredentialsProvider();
			credsProvider.setCredentials(new AuthScope(SystemUtils.getNTLMProxyHost(), Integer.parseInt(SystemUtils.getNTLMProxyport())), new NTCredentials(SystemUtils.getNTLMProxyUser(), SystemUtils.getNTLMProxyPassword(), SystemUtils.getNTLMProxyWorkstation(), SystemUtils.getNTLMProxyDomain()));
			context.setCredentialsProvider(credsProvider);
		} else
			httpClient = HttpClients.custom().setDefaultRequestConfig(globalConfig).setDefaultCookieStore(cookieStore).setConnectionManager(connectionManager).build();
		return httpClient;
	}
	protected void populateFirstAndLastName() {
		String firstName = "";
		String lastName = "";
		if(requestObject.getFirstName()==null || requestObject.getLastName()==null){
			String []splits =requestObject.getName().split("\\s");
			firstName = splits[0];
			if(splits.length>2){
				StringBuilder lastNameBuilder = new StringBuilder();
				for(int i=1;i<splits.length;i++){
					lastNameBuilder.append(splits[i]);
					if(i<splits.length-1)
						lastNameBuilder.append(" ");
				}
				lastName = lastNameBuilder.toString();
			}else if(splits.length==2)
				lastName = splits[1];
			requestObject.setFirstName(firstName);
			requestObject.setLastName(lastName);
		}
	}	
	public abstract String doWork() throws Exception;
	public abstract String getSiteURL();
	protected String fireGetRequestForCookies(CloseableHttpClient httpClient, String siteURL, HttpContext httpContext) throws ClientProtocolException, IOException {
		HttpGet httpget = new HttpGet(siteURL);
		return httpClient.execute(httpget, new StringResponseHandler(), httpContext);
	}
	protected String getXPathOutputFromHtml(String html, String xpath) throws XPathExpressionException {
		return getXPathOutputFromHtml(html, new String[]{xpath}).get(xpath);
	}
	protected Map<String, String> getXPathOutputFromHtml(String html, String ...xpaths) throws XPathExpressionException {
		Tidy tidy = new Tidy();
		tidy.setXHTML(true);
		tidy.setQuiet(true);
		tidy.setShowWarnings(false);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		Document xmlDocument = tidy.parseDOM(new ByteArrayInputStream(html.getBytes()), baos);
		XPath xPath =  XPathFactory.newInstance().newXPath();
		Map<String, String> evaluationResult = new HashMap<String, String>();
		for(String xpath : xpaths){
			String message = xPath.compile(xpath).evaluate(xmlDocument);
			evaluationResult.put(xpath, message);
		}
		
		return evaluationResult;
	}
}
