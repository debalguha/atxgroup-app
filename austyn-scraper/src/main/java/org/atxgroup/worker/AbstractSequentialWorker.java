package org.atxgroup.worker;

import java.util.Queue;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.atxgroup.persistence.model.NameRequest;
import org.atxgroup.worker.sequence.WorkSequence;

public abstract class AbstractSequentialWorker extends AbstractGetCookieAndPostWorker{
	public AbstractSequentialWorker(NameRequest requestObject, Boolean proxyEnabled, String formName) {
		super(requestObject, proxyEnabled, formName);
	}
	public abstract Queue<WorkSequence> getWorkSequence();
	@Override
	protected String firePostRequestForResponse(CloseableHttpClient httpClient, HttpPost httpPost) throws Exception {
		Queue<WorkSequence> sequenceQueue = getWorkSequence();
		String response = null;
		while(!sequenceQueue.isEmpty()){
			WorkSequence workSequence = sequenceQueue.poll();
			if(workSequence == null)
				break;
			response = workSequence.executeSequence(httpClient, httpContext, requestObject);
		}
		return response;
	}
}
