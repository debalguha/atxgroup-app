package org.atxgroup.worker;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.atxgroup.persistence.model.NameRequest;

public class AustynCityLimitsWorker extends AbstractGetHTMLAndPostWorker {
	private static final String SITE_URL = "http://www.austincitylimits.com/bloody-mary-morning-2015/";
	private static final String SUBMIT_URL = "http://www.austincitylimits.com/bloody-mary-morning-2015/";
	
	private static final String FIRST_NAME_KEY = "firstname";
	private static final String LAST_NAME_KEY = "lastname";
	private static final String EMAIL_KEY = "email";
	private static final String CITY_KEY = "city";
	private static final String STATE_KEY = "state";
	private static final String ZIP_KEY = "zip";
	private static final String AFFILIATION_KEY = "affiliation";
	private static final String NEWSLETTER_KEY = "acl-newsletter";
	
	private static final Log logger = LogFactory.getLog(AustynCityLimitsWorker.class);
	public AustynCityLimitsWorker(NameRequest requestObject, Boolean proxyEnabled, String formName) {
		super(requestObject, proxyEnabled, formName);
	}

	@Override
	public String getSubmitURL() {
		return SUBMIT_URL;
	}

	@Override
	public List<NameValuePair> buildNameValuePair(String... html) throws Exception {
		List<NameValuePair> pairs = new ArrayList<NameValuePair>();
		pairs.add(new BasicNameValuePair(FIRST_NAME_KEY, requestObject.getFirstName()));
		pairs.add(new BasicNameValuePair(LAST_NAME_KEY, requestObject.getLastName()));
		pairs.add(new BasicNameValuePair(EMAIL_KEY, requestObject.getEmail()));
		pairs.add(new BasicNameValuePair(CITY_KEY, requestObject.getCity()));
		pairs.add(new BasicNameValuePair(STATE_KEY, requestObject.getState()));
		pairs.add(new BasicNameValuePair(ZIP_KEY, requestObject.getZipCode()));
		pairs.add(new BasicNameValuePair(AFFILIATION_KEY, requestObject.getAffiliation()));
		pairs.add(new BasicNameValuePair(NEWSLETTER_KEY, "yes"));
		return pairs;
	}

	@Override
	public void addHeaders(HttpPost httpPost) {}

	@Override
	public long getSleepIntervalInMilliseconds() {
		return 500;
	}

	@Override
	public void buildResponse(Map<String, Object> retMap, String str) throws Exception {
		//Thanks, you've been registered.
		if(str.contains("Thanks, you've been registered")){
			retMap.put("success", new Boolean(true));
			retMap.put("response", "You have successfully registered");
		}else{
			logger.warn("Failed!!");
			logger.warn(str);
			retMap.put("success", new Boolean(false));
		}
	}

	@Override
	public Log getLogger() {
		return logger;
	}

	@Override
	public String getSiteURL() {
		return SITE_URL;
	}

}
