package org.atxgroup.worker;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.conn.HttpClientConnectionManager;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.atxgroup.http.pool.HttpConnectionPoolHelper;
import org.atxgroup.persistence.model.NameRequest;
import org.codehaus.jackson.map.ObjectMapper;

public class AmericanSongWriterWorker extends GenericWorker {
	private static int counter=0;
	private static final Log logger = LogFactory.getLog(AmericanSongWriterWorker.class);
	private static final String SITE_URL = "http://americansongwriter.us5.list-manage.com/subscribe/post?u=45cddc4191f84b97609049664&id=5233d3667a";
	private static final String SUBMIT_URL = "http://americansongwriter.us5.list-manage.com/subscribe/post";
	private static final String u = "45cddc4191f84b97609049664";
	private static final String id = "5233d3667a";
	private static final String U_KEY = "u";
	private static final String ID_KEY = "id";
	private static final String EMAIL_KEY = "MERGE0";
	private static final String FIRST_NAME_KEY = "MERGE1";
	private static final String LAST_NAME_KEY = "MERGE2";
	private static final String EMAIL_TYPE_KEY = "EMAILTYPE";
	private static final String EMAIL_TYPE_KEY_VAL = "html";
	private static final String SUBMIT_KEY = "submit";
	private static final String SUBMIT_KEY_VAL= "Subscribe to list";
	private static final String ERROR_XPATH = "//div[@class='errorText']/text()";
	private static final ObjectMapper objMapper = new ObjectMapper();
	private HttpClientContext httpContext;
	
	public AmericanSongWriterWorker(NameRequest requestObject, Boolean proxyEnabled, String formName) {
		super(requestObject, proxyEnabled, formName);
	}

	@Override
	public String doWork() throws Exception {
		counter++;
		logger.info("Starting to scrape for email: " + requestObject.getEmail());
		Map<String, Object> retMap = new HashMap<String, Object>();
		logger.info("Going to populate first & last name from name.");
		populateFirstAndLastName();
		try {
			HttpClientConnectionManager connectionManager = HttpConnectionPoolHelper.getConnectionManager();
			HttpClientContext context = HttpClientContext.create();
			CloseableHttpClient httpClient = geClosabletHttpClient(context, connectionManager);
			fireGetRequestForCookies(httpClient, getSiteURL(), context);
			HttpPost httpPost = new HttpPost(SUBMIT_URL);
			List<NameValuePair> pairs = new ArrayList<NameValuePair>();
			pairs.add(new BasicNameValuePair(U_KEY, u));
			pairs.add(new BasicNameValuePair(ID_KEY, id));
			pairs.add(new BasicNameValuePair(EMAIL_KEY, requestObject.getEmail()));
			pairs.add(new BasicNameValuePair(FIRST_NAME_KEY, requestObject.getFirstName()));
			pairs.add(new BasicNameValuePair(LAST_NAME_KEY, requestObject.getLastName()));
			pairs.add(new BasicNameValuePair(EMAIL_TYPE_KEY, EMAIL_TYPE_KEY_VAL));
			pairs.add(new BasicNameValuePair(SUBMIT_KEY, SUBMIT_KEY_VAL));
			pairs.add(new BasicNameValuePair("b_".concat(u).concat("_").concat(id), ""));
			httpPost.setEntity(new UrlEncodedFormEntity(pairs));
			String str = httpClient.execute(httpPost, new StringResponseHandler(), httpContext);
			retMap.put("response", str);
			if(str.contains("Almost finished"))
				retMap.put("success", new Boolean(true));
			else{
				String errorStr = null;
				try {
					errorStr = getXPathOutputFromHtml(str, ERROR_XPATH);
				} catch (Exception e) {
					logger.warn("Unable to parse the error html: "+str);
					e.printStackTrace();
				}
				retMap.put("success", new Boolean(false));
				if(errorStr == null || errorStr.isEmpty()){
					requestObject.setProcessingError("Unable to submit form[" + formName + "] for " + requestObject.getEmail()+". Please contact support!! Retry may not help!");
					logger.info("Failed, unable to get the error string!!");
				}else{
					if(errorStr.contains("already subscribed")){
						retMap.put("success", new Boolean(true));
						requestObject.setProcessingError(errorStr);
						logger.info("Failed!! But marked successfull as "+errorStr);
					}else
						logger.info("Failed!!"+errorStr);
					
				}
			}
			if(counter%5==0)
				Thread.sleep(1000 * 5 * 60);
			else
				Thread.sleep(1000);
		} catch (Exception e) {
			logger.error("Unable to submit form[" + formName + "] for " + requestObject.getEmail());
			logger.error("Error while submitting form[" + formName + "].", e);
			retMap.put("success", new Boolean(false));
			retMap.put("response", "Unable to register " + requestObject.getEmail() + " for American Song Writer. Please try re-uploading this file.");
			requestObject.setProcessingError("Unable to register " + requestObject.getEmail() + " for American Song Writer. Please try re-uploading this file.");
		}
			
		return objMapper.writeValueAsString(retMap);
	}

	@Override
	public String getSiteURL() {
		return SITE_URL;
	}

	@Override
	public Log getLogger() {
		return logger;
	}

}
