package org.atxgroup.worker;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.atxgroup.persistence.model.NameRequest;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

public class WindishAgencyWorker extends GimmeWorker {

	private static final Log logger = LogFactory.getLog(WindishAgencyWorker.class);
	private static final String SITE_URL = "http://gimme.io/SXSWindish14?embed=1&embed_url=http%3A%2F%2Fwww.windishagency.com%2Fsxsw&mode=popup&ts="+System.currentTimeMillis();
	public WindishAgencyWorker(NameRequest requestObject, Boolean proxyEnabled, String formName) {
		super(requestObject, proxyEnabled, formName);
	}

	@Override
	public String toJsonRequest(NameRequest requestObject) throws JsonGenerationException, JsonMappingException, IOException {
		Map<String, Object> jsonMap = new HashMap<String, Object>();
		jsonMap.put("action", requestObject.getAction());
		jsonMap.put("email", requestObject.getEmail());
		jsonMap.put("impression_id", requestObject.getImpression_id());
		jsonMap.put("name", requestObject.getName());
		jsonMap.put("security_hash", requestObject.getSecurity_hash());
		return new ObjectMapper().writeValueAsString(jsonMap);
	}

	@Override
	public String getSiteURL() {
		return SITE_URL;
	}
	
	@Override
	public Log getLogger() {
		return logger;
	}
}
