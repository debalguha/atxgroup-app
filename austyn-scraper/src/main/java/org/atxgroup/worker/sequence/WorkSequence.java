package org.atxgroup.worker.sequence;

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.protocol.HttpContext;
import org.atxgroup.persistence.model.NameRequest;

public interface WorkSequence {
	public String executeSequence(CloseableHttpClient httpClient, HttpContext httpContext, NameRequest nameRequest) throws Exception;
}
