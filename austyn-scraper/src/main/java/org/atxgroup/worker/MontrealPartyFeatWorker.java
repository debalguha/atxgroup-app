package org.atxgroup.worker;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.atxgroup.persistence.model.NameRequest;

public class MontrealPartyFeatWorker extends AbstractGetHTMLAndPostWorker {
	private static final Log logger = LogFactory.getLog(MontrealPartyFeatWorker.class);
	private static final String SITE_URL = "https://events.r20.constantcontact.com/register/eventReg?oeidk=a07e8zabwv7319b2355";
	private static final String SUBMIT_URL = "https://events.r20.constantcontact.com/register/eventReg?oeidk=a07e8zabwv7319b2355";
	private static final String FIRST_NAME_KEY = "registrant.nameFirst";
	private static final String LAST_NAME_KEY = "registrant.nameLast";
	private static final String EMAIL_KEY = "registrant.emailAddress";
	private static final String EMAIL_CHECK_KEY = "emailCheck";
	private static final String REG_PERSONAL_CUSTOM1_KEY = "registrant.personalCustom1";
	private static final String REG_PERSONAL_CUSTOM1_KEY_VAL = "N/A";
	
	private static final String PAGE_KEY = "_page";
	private static final String PAGE_KEY_VAL = "1";
	private static final String FINISH_KEY = "_finish";
	private static final String FINISH_KEY_VAL = "";
	private static final String SANDBOX_KEY = "sandbox";
	private static final String SANDBOX_KEY_VAL = "false";
	
	
	private static final Map<String, String> paramXpathMap = new HashMap<String, String>();
	static{
		paramXpathMap.put("registrant.id.eventUid", "//input[@name='registrant.id.eventUid']/@value");
		paramXpathMap.put("registrant.id.trackName", "//input[@name='registrant.id.trackName']/@value");
		paramXpathMap.put("registrant.id.subscriberSeqNo", "//input[@name='registrant.id.subscriberSeqNo']/@value");
		paramXpathMap.put("registrant.key", "//input[@name='registrant.key']/@value");
		paramXpathMap.put("degenerativeGuest[0].feeLabel", "//input[@name='degenerativeGuest[0].feeLabel']/@value");//select[@name="degenerativeGuest[0].count"]/option[@selected]/@value
		paramXpathMap.put("degenerativeGuest[0].count", "//select[@name='degenerativeGuest[0].count']/option[@selected]/@value");
		paramXpathMap.put("registrant.addRegistrantAsContact", "//input[@name='registrant.addRegistrantAsContact']/@value");
		paramXpathMap.put("_registrant.addRegistrantAsContact", "//input[@name='_registrant.addRegistrantAsContact']/@value");
		paramXpathMap.put("eventId", "//input[@name='eventId']/@value");
		paramXpathMap.put("eventKey", "//input[@name='eventKey']/@value");
		paramXpathMap.put("registrant.jmmlListId", "//input[@name='registrant.jmmlListId']/@value");
	}
	
	public MontrealPartyFeatWorker(NameRequest requestObject, Boolean proxyEnabled, String formName) {
		super(requestObject, proxyEnabled, formName);
	}
	
	@Override
	public List<NameValuePair> buildNameValuePair(String... html) throws Exception {
		List<NameValuePair> pairs = new ArrayList<NameValuePair>();
		pairs.add(new BasicNameValuePair(PAGE_KEY, PAGE_KEY_VAL));
		pairs.add(new BasicNameValuePair(FINISH_KEY, FINISH_KEY_VAL));
		pairs.add(new BasicNameValuePair(SANDBOX_KEY, SANDBOX_KEY_VAL));
		pairs.add(new BasicNameValuePair(REG_PERSONAL_CUSTOM1_KEY, REG_PERSONAL_CUSTOM1_KEY_VAL));
		
		pairs.add(new BasicNameValuePair(FIRST_NAME_KEY, requestObject.getFirstName()));
		pairs.add(new BasicNameValuePair(LAST_NAME_KEY, requestObject.getLastName()));
		pairs.add(new BasicNameValuePair(EMAIL_KEY, requestObject.getEmail()));
		pairs.add(new BasicNameValuePair(EMAIL_CHECK_KEY, requestObject.getEmail()));
		
		for(Map.Entry<String, String> entry : paramXpathMap.entrySet())
			pairs.add(new BasicNameValuePair(entry.getKey(), getXPathOutputFromHtml(html[0], entry.getValue())));
		logger.debug(pairs);
		return pairs;
	}

	@Override
	public Log getLogger() {
		return logger;
	}

	@Override
	public String getSiteURL() {
		return SITE_URL;
	}

	@Override
	public String getSubmitURL() {
		return SUBMIT_URL;
	}

	@Override
	public void addHeaders(HttpPost httpPost) {
		
	}

	@Override
	public long getSleepIntervalInMilliseconds() {
		return 500;
	}

	@Override
	public void buildResponse(Map<String, Object> retMap, String str) throws Exception {
		if(str.contains("You have successfully registered")){
			retMap.put("success", new Boolean(true));
			retMap.put("response", "You have successfully registered");
		}else{
			logger.warn("Failed!!");
			logger.warn(str);
			retMap.put("success", new Boolean(false));
		}
	}

}
