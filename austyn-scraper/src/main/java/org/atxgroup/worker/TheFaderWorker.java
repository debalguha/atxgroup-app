package org.atxgroup.worker;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.atxgroup.persistence.model.NameRequest;

public class TheFaderWorker extends AbstractGetCookieAndPostWorker {
	private static final Log logger = LogFactory
			.getLog(TheFaderWorker.class);
	private static String SITE_URL = "http://www.thefader.com/join/";
	private static final String SUBMIT_URL = "http://www.thefader.com/events_newsletter_signup.php";
	private static final String JOIN_STATE="Texas";
	
	public TheFaderWorker(NameRequest requestObject, Boolean proxyEnabled, String formName) {
		super(requestObject, proxyEnabled, formName);
	}

	@Override
	public String getSiteURL() {
		return SITE_URL;
	}
	
	@Override
	public Log getLogger() {
		return logger;
	}

	@Override
	public String getSubmitURL() {
		return SUBMIT_URL;
	}

	@Override
	public void addHeaders(HttpPost httpPost) {
		return;		
	}

	@Override
	public long getSleepIntervalInMilliseconds() {
		return 500;
	}

	@Override
	public void buildResponse(Map<String, Object> retMap, String str) throws Exception {
		retMap.put("response", str);
		if(str.equals("Thanks for signing up")){
			retMap.put("success", new Boolean(true));
			retMap.put("response", "Thanks for signing up");
		}else
			retMap.put("success", new Boolean(false));		
	}

	@Override
	public List<NameValuePair> buildNameValuePair(String... html) throws Exception {
		List<NameValuePair> pairs = new ArrayList<NameValuePair>();
		pairs.add(new BasicNameValuePair("newsletter[first_name]", requestObject.getFirstName()));
		pairs.add(new BasicNameValuePair("newsletter[last_name]", requestObject.getLastName()));
		pairs.add(new BasicNameValuePair("newsletter[email]", requestObject.getEmail()));
		pairs.add(new BasicNameValuePair("newsletter[state]", JOIN_STATE));
		return pairs;
	}	

}
