package org.atxgroup.worker;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.atxgroup.persistence.model.NameRequest;

public class WhatSceneWorker extends AbstractGetHTMLAndPostWorker{
	private static final Log logger = LogFactory.getLog(WhatSceneWorker.class);
	private static final String SITE_URL = "http://whatscene.net/";
	private static final String SUBMIT_URL = "https://whatscene.us7.list-manage.com/subscribe/post?u=2a681df264dfecde1063e5ec1&amp;id=0f6e0070c2";
	private static final String FIRST_NAME_KEY = "FNAME";
	private static final String LAST_NAME_KEY = "LNAME";
	private static final String EMAIL_KEY = "EMAIL";
	
	private static final String JUNK_KEY = "b_2a681df264dfecde1063e5ec1_0f6e0070c2";
	private static final String SUBSCRIBE_KEY = "subscribe";
	
	public WhatSceneWorker(NameRequest requestObject, Boolean proxyEnabled, String formName) {
		super(requestObject, proxyEnabled, formName);
	}

	@Override
	public String getSubmitURL() {
		return SUBMIT_URL;
	}

	@Override
	public List<NameValuePair> buildNameValuePair(String... html) throws Exception {
		List<NameValuePair> pairs = new ArrayList<NameValuePair>();
		pairs.add(new BasicNameValuePair(FIRST_NAME_KEY, requestObject.getFirstName()));
		pairs.add(new BasicNameValuePair(LAST_NAME_KEY, requestObject.getLastName()));
		pairs.add(new BasicNameValuePair(EMAIL_KEY, requestObject.getEmail()));
		
		pairs.add(new BasicNameValuePair(JUNK_KEY, ""));
		pairs.add(new BasicNameValuePair(SUBSCRIBE_KEY, "Subscribe"));
		
		logger.debug(pairs);
		return pairs;
	}

	@Override
	public void addHeaders(HttpPost httpPost) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public long getSleepIntervalInMilliseconds() {
		return 500;
	}

	@Override
	public void buildResponse(Map<String, Object> retMap, String str) throws Exception {
		if(str.contains("Almost finished")){
			retMap.put("success", new Boolean(true));
			retMap.put("response", "Thanks for signing up.");
		}else{
			logger.warn("Failed!!");
			logger.debug(str);
			retMap.put("success", new Boolean(false));
		}		
	}

	@Override
	public Log getLogger() {
		return logger;
	}

	@Override
	public String getSiteURL() {
		return SITE_URL;
	}
	
	private static final Map<String, String> stateMap = new HashMap<String, String>();
	static{
		stateMap.put("ALABAMA", "AL");
		stateMap.put("ALASKA", "AK");
		stateMap.put("AMERICAN SAMOA", "AS");
		stateMap.put("ARIZONA", "AZ");
		stateMap.put("ARKANSAS", "AR");
		stateMap.put("CALIFORNIA", "CA");
		stateMap.put("COLORADO", "CO");
		stateMap.put("CONNECTICUT", "CT");
		stateMap.put("DELAWARE", "DE");
		stateMap.put("DIST. OF COLUMBIA", "DC");
		stateMap.put("FLORIDA", "FL");
		stateMap.put("GEORGIA", "GA");
		stateMap.put("GUAM", "GU");
		stateMap.put("HAWAII", "HI");
		stateMap.put("IDAHO", "ID");
		stateMap.put("ILLINOIS", "IL");
		stateMap.put("INDIANA", "IN");
		stateMap.put("IOWA", "IA");
		stateMap.put("KANSAS", "KS");
		stateMap.put("KENTUCKY", "KY");
		stateMap.put("LOUISIANA", "LA");
		stateMap.put("MAINE", "ME");
		stateMap.put("MARYLAND", "MD");
		stateMap.put("MARSHALL ISLANDS", "MH");
		stateMap.put("MASSACHUSETTS", "MA");
		stateMap.put("MICHIGAN", "MI");
		stateMap.put("MICRONESIA", "FM");
		stateMap.put("MINNESOTA", "MN");
		stateMap.put("MISSISSIPPI", "MS");
		stateMap.put("MISSOURI", "MO");
		stateMap.put("MONTANA", "MT");
		stateMap.put("NEBRASKA", "NE");
		stateMap.put("NEVADA", "NV");
		stateMap.put("NEW HAMPSHIRE", "NH");
		stateMap.put("NEW JERSEY", "NJ");
		stateMap.put("NEW MEXICO", "NM");
		stateMap.put("NEW YORK", "NY");
		stateMap.put("NORTH CAROLINA", "NC");
		stateMap.put("NORTH DAKOTA", "ND");
		stateMap.put("NORTHERN MARIANAS", "MP");
		stateMap.put("OHIO", "OH");
		stateMap.put("OKLAHOMA", "OK");
		stateMap.put("OREGON", "OR");
		stateMap.put("PALAU", "PW");
		stateMap.put("PENNSYLVANIA", "PA");
		stateMap.put("PUERTO RICO", "PR");
		stateMap.put("RHODE ISLAND", "RI");
		stateMap.put("SOUTH CAROLINA", "SC");
		stateMap.put("SOUTH DAKOTA", "SD");
		stateMap.put("TENNESSEE", "TN");
		stateMap.put("TEXAS", "TX");
		stateMap.put("UTAH", "UT");
		stateMap.put("VERMONT", "VT");
		stateMap.put("VIRGINIA", "VA");
		stateMap.put("VIRGIN ISLANDS", "VI");
		stateMap.put("WASHINGTON", "WA");
		stateMap.put("WEST VIRGINIA", "WV");
		stateMap.put("WISCONSIN", "WI");
		stateMap.put("WYOMING", "WY");
	}

}
