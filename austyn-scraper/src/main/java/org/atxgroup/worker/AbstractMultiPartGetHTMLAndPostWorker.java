package org.atxgroup.worker;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.conn.HttpClientConnectionManager;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.atxgroup.http.pool.HttpConnectionPoolHelper;
import org.atxgroup.persistence.model.NameRequest;

public abstract class AbstractMultiPartGetHTMLAndPostWorker extends AbstractGetHTMLAndPostWorker {

	public AbstractMultiPartGetHTMLAndPostWorker(NameRequest requestObject, Boolean proxyEnabled, String formName) {
		super(requestObject, proxyEnabled, formName);
	}

	@Override
	public String doWork() throws Exception {
		getLogger().info("Starting to scrape for email: "+requestObject.getEmail());
		Map<String, Object> retMap = new HashMap<String, Object>();
		populateFirstAndLastName();
		CloseableHttpClient httpClient = null;
		try{
			HttpClientConnectionManager connectionManager = HttpConnectionPoolHelper.getConnectionManager();
			httpContext = HttpClientContext.create();
			httpClient = geClosabletHttpClient(httpContext, connectionManager);
			String html = fireGetRequestForCookies(httpClient, getSiteURL(), httpContext);
			HttpPost httpPost = new HttpPost(getSubmitURL());
			List<NameValuePair> pairs = buildNameValuePair(html);
			MultipartEntityBuilder builder = MultipartEntityBuilder.create();
			for(NameValuePair pair : pairs)
				builder.addTextBody(pair.getName(), pair.getValue());
			httpPost.setEntity(builder.build());
			addHeaders(httpPost);
			String str = httpClient.execute(httpPost, new StringResponseHandler(), httpContext);
			buildResponse(retMap, str);
			Thread.sleep(getSleepIntervalInMilliseconds());	
		}catch(Exception e){
			getLogger().error("Unable to submit form["+formName+"] for "+requestObject.getEmail());
			getLogger().error("Error while submitting form["+formName+"].", e);
    		retMap.put("success", new Boolean(false));
    		retMap.put("response", "Unable to register "+requestObject.getEmail()+" for the the fader. Please try re-uploading this file.");
    		requestObject.setProcessingError("Unable to register "+requestObject.getEmail()+" for the Green Room Party. Please try re-uploading this file.");
		}
		return objMapper.writeValueAsString(retMap);	
	}
}
