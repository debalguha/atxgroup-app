package org.atxgroup.worker;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HttpContext;
import org.atxgroup.persistence.model.NameRequest;
import org.atxgroup.worker.sequence.WorkSequence;

public class RaiseTheBarSequentialWorker extends AbstractSequentialWorker {
	private static final Log logger = LogFactory.getLog(RaiseTheBarSequentialWorker.class);
	private static final String SUBMIT_URL = "http://raisethebarsxsw.rsvpify.com/";
	private static final String SITE_URL = "http://raisethebarsxsw.rsvpify.com/";

	public RaiseTheBarSequentialWorker(NameRequest requestObject, Boolean proxyEnabled, String formName) {
		super(requestObject, proxyEnabled, formName);
	}

	@Override
	public Queue<WorkSequence> getWorkSequence() {
		Queue<WorkSequence> sequence = new LinkedList<WorkSequence>();
		sequence.add(new WorkSequence() {

			@Override
			public String executeSequence(CloseableHttpClient httpClient, HttpContext httpContext, NameRequest nameRequest) throws Exception {
				HttpPost httpPost = new HttpPost(getSubmitURL());
				httpPost.setEntity(new UrlEncodedFormEntity(buildNameValuePair()));
				addHeaders(httpPost);
				return httpClient.execute(httpPost, new StringResponseHandler(), httpContext);
			}

			public List<NameValuePair> buildNameValuePair() throws Exception {
				List<NameValuePair> pairs = new ArrayList<NameValuePair>();
				pairs.add(new BasicNameValuePair("email", requestObject.getEmail()));
				pairs.add(new BasicNameValuePair("guest_count", String.valueOf(1)));
				return pairs;
			}
		});
		sequence.add(new WorkSequence() {

			@Override
			public String executeSequence(CloseableHttpClient httpClient, HttpContext httpContext, NameRequest nameRequest) throws Exception {
				HttpPost httpPost = new HttpPost(getSubmitURL());
				httpPost.setEntity(new UrlEncodedFormEntity(buildNameValuePair()));
				addHeaders(httpPost);
				return httpClient.execute(httpPost, new StringResponseHandler(), httpContext);
			}

			public List<NameValuePair> buildNameValuePair() throws Exception {
				List<NameValuePair> pairs = new ArrayList<NameValuePair>();
				pairs.add(new BasicNameValuePair("guest_list_guest_id[]", ""));
				pairs.add(new BasicNameValuePair("guest_list_plus_one_allowed[]", ""));
				pairs.add(new BasicNameValuePair("guest_list_is_plus_one[]", ""));
				pairs.add(new BasicNameValuePair("ignore_guest[]", "0"));
				pairs.add(new BasicNameValuePair("first_name[]", requestObject.getFirstName()));
				pairs.add(new BasicNameValuePair("last_name[]", requestObject.getLastName()));
				pairs.add(new BasicNameValuePair("accepted[]", "1"));
				pairs.add(new BasicNameValuePair("title[]", "Mr."));
				return pairs;
			}
		});
		sequence.add(new WorkSequence() {

			@Override
			public String executeSequence(CloseableHttpClient httpClient, HttpContext httpContext, NameRequest nameRequest) throws Exception {
				HttpPost httpPost = new HttpPost(getSubmitURL());
				httpPost.setEntity(new UrlEncodedFormEntity(buildNameValuePair()));
				addHeaders(httpPost);
				return httpClient.execute(httpPost, new StringResponseHandler(), httpContext);
			}

			public List<NameValuePair> buildNameValuePair() throws Exception {
				List<NameValuePair> pairs = new ArrayList<NameValuePair>();
				pairs.add(new BasicNameValuePair("note", "NA"));
				return pairs;
			}
		});
		return sequence;
	}

	@Override
	public String getSubmitURL() {
		return SUBMIT_URL;
	}

	@Override
	public List<NameValuePair> buildNameValuePair(String... html) throws Exception {
		return null;
	}

	@Override
	public void addHeaders(HttpPost httpPost) {
		httpPost.addHeader(new BasicHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.76 Safari/537.36"));
	}

	@Override
	public long getSleepIntervalInMilliseconds() {
		return 2000;
	}

	@Override
	public void buildResponse(Map<String, Object> retMap, String response) throws Exception {
		if(response.contains("We've received your RSVP")){
			retMap.put("success", new Boolean(true));
			retMap.put("response", "We've received your RSVP.");
		} else{
			logger.error("Unable to submit form for " + requestObject.getEmail() + ", Response: "+response);
			retMap.put("success", new Boolean(false));
			retMap.put("response", "Unable to register " + requestObject.getEmail() + " for RAISE THE BAR DAY PARTY!!.");
		}
	}

	@Override
	public Log getLogger() {
		return logger;
	}

	@Override
	public String getSiteURL() {
		return SITE_URL;
	}

}
