package org.atxgroup.worker;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.atxgroup.persistence.model.NameRequest;

public class ReliveAustynWorker extends AbstractGetCookieAndPostWorker {
	private static final Log logger = LogFactory.getLog(ReliveAustynWorker.class);
	private static final String SITE_URL = "https://pages.umusic-mail.com/ingroovessxsw2014";
	private static final String SUBMIT_URL = "https://pages.umusic-mail.com/ingroovessxsw2014/thanks/?src=&amp;replyTo=fontana@umgd.umusic-mail.com";
	private static final String FIRST_NAME_KEY = "firstname";
	private static final String LAST_NAME_KEY = "lastname";
	private static final String EMAIL_KEY = "email";
	private static final String COMPANY_KEY = "company";
	private static final String CLIENT_REQUEST_KEY = "clientRequest";
	private static String CLIENT_REQUEST_VAL = "true";
	public ReliveAustynWorker(NameRequest requestObject, Boolean proxyEnabled, String formName) {
		super(requestObject, proxyEnabled, formName);
	}

	@Override
	public String getSubmitURL() {
		return SUBMIT_URL;
	}

	@Override
	public void addHeaders(HttpPost httpPost) {

	}

	@Override
	public long getSleepIntervalInMilliseconds() {
		return 500;
	}

	@Override
	public void buildResponse(Map<String, Object> retMap, String str) throws Exception {
		retMap.put("response", str);
		if(str.contains("Thank you for your RSVP")){
			retMap.put("success", new Boolean(true));
			retMap.put("response", "Thank you for your RSVP");
		}else
			retMap.put("success", new Boolean(false));		
	}

	@Override
	public List<NameValuePair> buildNameValuePair(String... html) throws Exception {
		List<NameValuePair> pairs = new ArrayList<NameValuePair>();
		pairs.add(new BasicNameValuePair(FIRST_NAME_KEY, requestObject.getFirstName()));
		pairs.add(new BasicNameValuePair(LAST_NAME_KEY, requestObject.getLastName()));
		pairs.add(new BasicNameValuePair(EMAIL_KEY, requestObject.getEmail()));
		pairs.add(new BasicNameValuePair(COMPANY_KEY, requestObject.getAffiliation()));
		pairs.add(new BasicNameValuePair(CLIENT_REQUEST_KEY, CLIENT_REQUEST_VAL));
		return pairs;
	}

	@Override
	public Log getLogger() {
		return logger;
	}

	@Override
	public String getSiteURL() {
		return SITE_URL;
	}

}
