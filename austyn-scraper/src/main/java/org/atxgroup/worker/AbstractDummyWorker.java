package org.atxgroup.worker;

import java.util.HashMap;
import java.util.Map;

import org.atxgroup.persistence.model.NameRequest;

public abstract class AbstractDummyWorker extends AbstractGetHTMLAndPostWorker{

	public AbstractDummyWorker(NameRequest requestObject, Boolean proxyEnabled, String formName) {
		super(requestObject, proxyEnabled, formName);
	}
	
	@Override
	public String doWork() throws Exception {
		Map<String, Object> retMap = new HashMap<String, Object>();
		buildResponse(retMap, provideDummyString());
		return objMapper.writeValueAsString(retMap);
	}
	
	public abstract String provideDummyString();

}
