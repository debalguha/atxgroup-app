package org.atxgroup.worker;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.atxgroup.persistence.model.NameRequest;

public class AustynPartyWorker extends GimmeWorker {
	private static final String SITE_URL = "http://gimme.io/austinpartyweekend?embed=1&embed_url=http%3A%2F%2Faustinpartyweekend.com%2F&mode=popup&ts=Fri%20Jan%2010%202014%2002:01:34%20GMT+0530%20(India%20Standard%20Time)"; 
	public AustynPartyWorker(NameRequest requestObject, Boolean proxyEnabled, String formName) {
		super(requestObject, proxyEnabled, formName);
	}
	private static final Log logger = LogFactory.getLog(AustynPartyWorker.class);

	@Override
	public String getSiteURL() {
		return SITE_URL;
	}

	@Override
	public Log getLogger() {
		return logger;
	}
}
