package org.atxgroup.worker;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.Consts;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.conn.HttpClientConnectionManager;
import org.apache.http.cookie.Cookie;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.atxgroup.http.pool.HttpConnectionPoolHelper;
import org.atxgroup.persistence.model.NameRequest;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

public abstract class GimmeWorker extends GenericWorker{
	public GimmeWorker(NameRequest requestObject, Boolean proxyEnabled, String formName) {
		super(requestObject, proxyEnabled, formName);
	}
	public static final String GIMME_RSVP_API_URL = "http://gimme.io/api/rsvp";
	
	public String toJsonRequest(NameRequest requestObject) throws JsonGenerationException, JsonMappingException, IOException {
		Map<String, Object> jsonMap = new HashMap<String, Object>();
		jsonMap.put("action", requestObject.getAction());
		jsonMap.put("email", requestObject.getEmail());
		jsonMap.put("impression_id", requestObject.getImpression_id());
		jsonMap.put("name", requestObject.getName());
		jsonMap.put("security_hash", requestObject.getSecurity_hash());
		jsonMap.put("subscribe", requestObject.isSubscribe());

		return new ObjectMapper().writeValueAsString(jsonMap);
	}
	
	@Override
	public String doWork() throws Exception {
		HttpClientConnectionManager connectionManager = HttpConnectionPoolHelper.getConnectionManager();
		HttpClientContext context = HttpClientContext.create();
		CloseableHttpClient httpClient = geClosabletHttpClient(context, connectionManager);
		createRequestObjectBySendingGETRequest(httpClient, context);
		String jsonObject = toJsonRequest(requestObject);
		StringEntity entity = new StringEntity(jsonObject, ContentType.create("application/json", Consts.UTF_8));
		HttpPut httpput = new HttpPut(GIMME_RSVP_API_URL);
		httpput.setEntity(entity);
		return httpClient.execute(httpput, new StringResponseHandler(), context);
	}
	
	protected void createRequestObjectBySendingGETRequest(CloseableHttpClient httpClient, HttpClientContext context) throws ClientProtocolException, IOException {
		HttpGet httpget = new HttpGet(getSiteURL());
		httpClient.execute(httpget, new StringResponseHandler(), context);
		String imp_id = "";
		String imp_key = "";
		List<Cookie> cookies = context.getCookieStore().getCookies();
		for (Cookie cookie : cookies) {
			if (cookie.getName().equalsIgnoreCase("gimme_imp_id"))
				imp_id = cookie.getValue();
			if (cookie.getName().equalsIgnoreCase("gimme_imp_key"))
				imp_key = cookie.getValue();
		}
		requestObject.setImpression_id(imp_id);
		requestObject.setAction("rsvp");
		requestObject.setSecurity_hash(imp_key);
		requestObject.setSubscribe(false);
	}	
}
