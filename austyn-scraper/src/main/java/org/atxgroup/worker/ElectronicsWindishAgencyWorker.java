package org.atxgroup.worker;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.atxgroup.persistence.model.NameRequest;

public class ElectronicsWindishAgencyWorker extends WindishAgencyWorker {
	private static final Log logger = LogFactory.getLog(ElectronicsWindishAgencyWorker.class);
	private static final String SITE_URL = "http://gimme.io/SXSWindishElectronic?embed=1&embed_url=http%3A%2F%2Fwww.windishagency.com%2Fsxsw&mode=popup&ts=1393509467592";
	
	public ElectronicsWindishAgencyWorker(NameRequest requestObject, Boolean proxyEnabled, String formName) {
		super(requestObject, proxyEnabled, formName);
	}

	@Override
	public Log getLogger() {
		return logger;
	}

	@Override
	public String getSiteURL() {
		return SITE_URL;
	}

}
