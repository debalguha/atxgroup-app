package org.atxgroup.worker;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.Header;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.atxgroup.persistence.model.NameRequest;
import org.codehaus.jackson.JsonNode;

public class JumpStartTexasWorker extends AbstractGetKeyAndPostWorker {
	private static final Log logger = LogFactory.getLog(JumpStartTexasWorker.class);
	private static String SITE_URL = "http://www.jstx.co/reg";
	private static final String SUBMIT_URL = "http://www.jstx.co/api/form/SaveFormSubmission?crumb=";
	private static final String KEY_URL = "http://www.jstx.co/api/form/FormSubmissionKey?crumb=";
	private String formId = "52e562c5e4b06c2b5ce36e89";
	private String collectionId = "52e562c5e4b06c2b5ce36e88";
	private static final String NAME_LIST_KEY = "name-yui_3_10_1_1_1390778221044_42175";
	private static final String MAIL_LIST_KEY = "email-yui_3_10_1_1_1370983560411_128491";
	private static final String CHECKBOX_KEY = "checkbox-yui_3_10_1_1_1390778221044_43523";
	private static final String CHECKBOX_KEY_VAL = "Don't keep me up-to-date about Jumpstart Texas";
	public JumpStartTexasWorker(NameRequest requestObject, Boolean proxyEnabled, String formName) {
		super(requestObject, proxyEnabled, formName);
	}

	@Override
	public void addHeaders(HttpPost httpPost) {
		Header hostHeader = new BasicHeader("Host", "www.jstx.co");
		Header originHeader = new BasicHeader("Origin", "http://www.jstx.co");
		Header pragmaHeader = new BasicHeader("pragma", "no-cache");
		Header refererHeader = new BasicHeader("Referer", "http://www.jstx.co/reg");
		Header userAgentHeader = new BasicHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.107 Safari/537.36");
		Header requestedWithHeader = new BasicHeader("X-Requested-With", "XMLHttpRequest");
		Header acceptHeader = new BasicHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
		Header acceptLanguage = new BasicHeader("Accept-Language", "en-US,en;q=0.5");
		Header cacheControlHeader = new BasicHeader("Cache-Control", "no-cache");
		Header contentTypeHeader = new BasicHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
		httpPost.addHeader(hostHeader);
		httpPost.addHeader(originHeader);
		httpPost.addHeader(pragmaHeader);
		httpPost.addHeader(refererHeader);
		httpPost.addHeader(userAgentHeader);
		httpPost.addHeader(requestedWithHeader);
		httpPost.addHeader(acceptHeader);
		httpPost.addHeader(acceptLanguage);
		httpPost.addHeader(cacheControlHeader);
		httpPost.addHeader(contentTypeHeader);
	}

	@Override
	public String getSiteURL() {
		return SITE_URL;
	}	
	
	@Override
	public Log getLogger() {
		return logger;
	}

	@Override
	public String getSubmitURL() {
		return SUBMIT_URL.concat(getCrumbFromCookie());
	}

	@Override
	public long getSleepIntervalInMilliseconds() {
		return 500;
	}

	@Override
	public void buildResponse(Map<String, Object> retMap, String str) throws Exception {
		JsonNode response = objMapper.readTree(str);
		if (response.has("error")) {
			retMap.put("success", response.get("success").asBoolean(false));
			retMap.put("response", "Error while submittig this record. Please re-upload this form.");
			requestObject.setProcessingError("Error while submittig this record. Please re-upload this form.");
		} else {
			retMap.put("success", response.get("success").asBoolean(false));
			retMap.put("response", "Response exists.");
		}
	}

	@Override
	public List<NameValuePair> buildNameValuePair(String... html) throws Exception {
		List<NameValuePair> pairs = new ArrayList<NameValuePair>();
		pairs.add(new BasicNameValuePair("formId", formId));
		pairs.add(new BasicNameValuePair("collectionId", collectionId));
		Map<String, Object> postReqMap = new TreeMap<String, Object>(new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {
				return o2.compareTo(o1);
			}
		});
		List<String> nameList = new ArrayList<String>();
		nameList.add(requestObject.getFirstName());
		nameList.add(requestObject.getLastName());
		postReqMap.put(NAME_LIST_KEY, nameList);
		postReqMap.put(MAIL_LIST_KEY, requestObject.getEmail());
		List<String> checkBoValList = new ArrayList<String>();
		checkBoValList.add(CHECKBOX_KEY_VAL);
		postReqMap.put(CHECKBOX_KEY, checkBoValList);
		pairs.add(new BasicNameValuePair("form", objMapper.writeValueAsString(postReqMap)));
		return pairs;
	}

	@Override
	public String getKeyURL() {
		return KEY_URL;
	}	
}
