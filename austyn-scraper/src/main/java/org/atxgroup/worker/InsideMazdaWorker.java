package org.atxgroup.worker;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.atxgroup.persistence.model.NameRequest;

public class InsideMazdaWorker extends AbstractMultiPartGetHTMLAndPostWorker {
	private static final Log logger = LogFactory.getLog(InsideMazdaWorker.class);
	private static final String SITE_URL = "http://insidemazda.mazdausa.com/sxsw/";
	private static final String SUBMIT_URL = "http://insidemazda.mazdausa.com/sxsw/";
	private static final String FIRST_NAME_KEY = "input_7";
	private static final String LAST_NAME_KEY = "input_8";
	private static final String EMAIL_KEY = "input_3";
	private static final String TERMS_KEY = "input_9.1";
	private static final String BLANK_KEY_X = "x";
	private static final String BLANK_KEY_Y = "y";
	
	private static final String ZIP_KEY = "input_6";
	private static final Map<String, String> paramXpathMap = new HashMap<String, String>();
	static{
		paramXpathMap.put("state_2", "//input[@name='state_2']/@value");
		paramXpathMap.put("is_submit_2", "//input[@name='is_submit_2']/@value");
		paramXpathMap.put("gform_submit", "//input[@name='gform_submit']/@value");
		paramXpathMap.put("gform_unique_id", "//input[@name='gform_unique_id']/@value");
		paramXpathMap.put("state_2", "//input[@name='state_2']/@value");
		paramXpathMap.put("gform_target_page_number_2", "//input[@name='gform_target_page_number_2']/@value");
		paramXpathMap.put("gform_source_page_number_2", "//input[@name='gform_source_page_number_2']/@value");
		paramXpathMap.put("gform_field_values", "//input[@name='gform_field_values']/@value");
	}
	public InsideMazdaWorker(NameRequest requestObject, Boolean proxyEnabled, String formName) {
		super(requestObject, proxyEnabled, formName);
	}

	@Override
	public String getSubmitURL() {
		return SUBMIT_URL;
	}

	@Override
	public List<NameValuePair> buildNameValuePair(String... html) throws Exception {
		Random random = new Random();
		List<NameValuePair> pairs = new ArrayList<NameValuePair>();
		pairs.add(new BasicNameValuePair(FIRST_NAME_KEY, requestObject.getFirstName()));
		pairs.add(new BasicNameValuePair(LAST_NAME_KEY, requestObject.getLastName()));
		pairs.add(new BasicNameValuePair(EMAIL_KEY, requestObject.getEmail()));
		
		pairs.add(new BasicNameValuePair(ZIP_KEY, requestObject.getZipCode()));
		pairs.add(new BasicNameValuePair(TERMS_KEY, "y"));
		pairs.add(new BasicNameValuePair(BLANK_KEY_X, String.valueOf(random.nextInt(999))));
		pairs.add(new BasicNameValuePair(BLANK_KEY_Y, String.valueOf(random.nextInt(999))));
		for(Map.Entry<String, String> entry : paramXpathMap.entrySet())
			pairs.add(new BasicNameValuePair(entry.getKey(), getXPathOutputFromHtml(html[0], entry.getValue())));
		logger.debug(pairs);
		return pairs;
	}

	@Override
	public void addHeaders(HttpPost httpPost) {
		// TODO Auto-generated method stub

	}

	@Override
	public long getSleepIntervalInMilliseconds() {
		return 500;
	}

	@Override
	public void buildResponse(Map<String, Object> retMap, String response) throws Exception {
		if(httpContext.getResponse().getStatusLine().getStatusCode()==302){
			retMap.put("success", new Boolean(true));
			retMap.put("response", "Thanks for signing up.");
		}else{
			logger.warn("Failed!!");
			logger.debug(response);
			retMap.put("success", new Boolean(false));
		}	
	}

	@Override
	public Log getLogger() {
		return logger;
	}

	@Override
	public String getSiteURL() {
		return SITE_URL;
	}

}
