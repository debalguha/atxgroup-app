package org.atxgroup.worker;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.conn.HttpClientConnectionManager;
import org.apache.http.impl.client.CloseableHttpClient;
import org.atxgroup.http.pool.HttpConnectionPoolHelper;
import org.atxgroup.persistence.model.NameRequest;

public abstract class AbstractGetHTMLAndPostWorker extends AbstractGetCookieAndPostWorker{


	public AbstractGetHTMLAndPostWorker(NameRequest requestObject, Boolean proxyEnabled, String formName) {
		super(requestObject, proxyEnabled, formName);
	}

	@Override
	public String doWork() throws Exception {
		getLogger().info("Starting to scrape for email: "+requestObject.getEmail());
		Map<String, Object> retMap = new HashMap<String, Object>();
		populateFirstAndLastName();
		CloseableHttpClient httpClient = null;
		String responseStr = null;
		try{
			HttpClientConnectionManager connectionManager = HttpConnectionPoolHelper.getConnectionManager();
			httpContext = HttpClientContext.create();
			httpClient = geClosabletHttpClient(httpContext, connectionManager);
			String html = fireGetRequestForCookies(httpClient, getSiteURL(), httpContext);
			HttpPost httpPost = new HttpPost(getSubmitURL());
			List<NameValuePair> pairs = buildNameValuePair(html);
			httpPost.setEntity(new UrlEncodedFormEntity(pairs));
			addHeaders(httpPost);
			responseStr = httpClient.execute(httpPost, new StringResponseHandler(), httpContext);
			buildResponse(retMap, responseStr);
			Thread.sleep(getSleepIntervalInMilliseconds());	
		}catch(Exception e){
			getLogger().error("Unable to submit form["+formName+"] for "+requestObject.getEmail());
			getLogger().error("Error while submitting form["+formName+"].", e);
			getLogger().error(responseStr);
    		retMap.put("success", new Boolean(false));
    		retMap.put("response", "Unable to register "+requestObject.getEmail()+" for the the fader. Please try re-uploading this file.");
    		requestObject.setProcessingError("Unable to register "+requestObject.getEmail()+" for the Green Room Party. Please try re-uploading this file.");
		}
		return objMapper.writeValueAsString(retMap);	
	}

}
