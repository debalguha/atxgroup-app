package org.atxgroup.worker;

public class ResultObject implements Comparable<ResultObject>{
	private String name;
	private String email;
	private String affiliation;
	private String groupName;
	private String firstName;
	private String lastName;
	private String form;
	private String city;
	private String state;
	private String zipCode;
	private boolean success;
	private boolean processed;
	private boolean preExists;
	private String processingError = "None";
	private boolean finished = false;
	private int retryNum;
	public boolean isProcessed() {
		return processed;
	}
	public void setProcessed(boolean processed) {
		this.processed = processed;
	}
	public boolean isPreExists() {
		return preExists;
	}
	public void setPreExists(boolean preExists) {
		this.preExists = preExists;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getProcessingError() {
		return processingError;
	}
	public void setProcessingError(String processingError) {
		this.processingError = processingError;
	}
	
	@Override
	public boolean equals(Object obj) {
		return email.equals(((ResultObject)obj).email);
	}
	@Override
	public int compareTo(ResultObject obj) {
		return name.compareTo(obj.name);
	}
	public String getAffiliation() {
		return affiliation;
	}
	public void setAffiliation(String affiliation) {
		this.affiliation = affiliation;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public boolean isFinished() {
		return finished;
	}
	public void setFinished(boolean finished) {
		this.finished = finished;
	}
	public int getRetryNum() {
		return retryNum;
	}
	public void setRetryNum(int retryNum) {
		this.retryNum = retryNum;
	}
	public String getForm() {
		return form;
	}
	public void setForm(String form) {
		this.form = form;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	
}
