package org.atxgroup.worker;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.conn.HttpClientConnectionManager;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.CloseableHttpClient;
import org.atxgroup.http.pool.HttpConnectionPoolHelper;
import org.atxgroup.persistence.model.NameRequest;
import org.codehaus.jackson.map.ObjectMapper;

public abstract class AbstractGetCookieAndPostWorker extends GenericWorker {
	protected HttpClientContext httpContext;
	protected static final ObjectMapper objMapper = new ObjectMapper();
	protected static final String CRUMB_COOKIE = "crumb";
	public AbstractGetCookieAndPostWorker(NameRequest requestObject, Boolean proxyEnabled, String formName) {
		super(requestObject, proxyEnabled, formName);
	}

	public abstract String getSubmitURL();
	@Override
	public String doWork() throws Exception {
		getLogger().info("Starting to scrape for email: "+requestObject.getEmail());
		Map<String, Object> retMap = new HashMap<String, Object>();
		populateFirstAndLastName();
		CloseableHttpClient httpClient = null;
		try{
			HttpClientConnectionManager connectionManager = HttpConnectionPoolHelper.getConnectionManager();
			httpContext = HttpClientContext.create();
			httpClient = geClosabletHttpClient(httpContext, connectionManager);
			fireGetRequestForCookies(httpClient, getSiteURL(), httpContext);
			String str = firePostRequestForResponse(httpClient, new HttpPost(getSubmitURL()));
			buildResponse(retMap, str);
			Thread.sleep(getSleepIntervalInMilliseconds());	
		}catch(Exception e){
			getLogger().error("Unable to submit form["+formName+"] for "+requestObject.getEmail());
			getLogger().error("Error while submitting form["+formName+"].", e);
    		retMap.put("success", new Boolean(false));
    		retMap.put("response", "Unable to register "+requestObject.getEmail()+" for the the fader. Please try re-uploading this file.");
    		requestObject.setProcessingError("Unable to register "+requestObject.getEmail()+" for the Green Room Party. Please try re-uploading this file.");
		}
		return objMapper.writeValueAsString(retMap);		
	}
	
	protected String firePostRequestForResponse(CloseableHttpClient httpClient, HttpPost httpPost) throws Exception {
		List<NameValuePair> pairs = buildNameValuePair();
		httpPost.setEntity(new UrlEncodedFormEntity(pairs));
		addHeaders(httpPost);
		return httpClient.execute(httpPost, new StringResponseHandler(), httpContext);
	}

	public abstract List<NameValuePair> buildNameValuePair(String ...html)throws Exception;
	public abstract void addHeaders(HttpPost httpPost);
	public abstract long getSleepIntervalInMilliseconds();
	public abstract void buildResponse(Map<String, Object> retMap, String response) throws Exception;
	protected String getCrumbFromCookie() {
		List<Cookie> cookies = httpContext.getCookieStore().getCookies();
		Cookie toRturn = null;
		for (Cookie cookie : cookies) {
			System.out.println(cookie.getName() + "--" + cookie.getValue());
			if (cookie.getName().equalsIgnoreCase(CRUMB_COOKIE))
				toRturn = cookie;
		}
		return toRturn.getValue();
	}
}
