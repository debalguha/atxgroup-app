package org.atxgroup.worker;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.Header;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.atxgroup.persistence.model.NameRequest;
import org.codehaus.jackson.JsonNode;

public class TheNakedGrapeMusicWorker extends AbstractGetHTMLAndPostWorker {

	private static Log logger = LogFactory.getLog(TheNakedGrapeMusicWorker.class);
	private static final String SITE_URL = "http://thenakedgrapemusicbox.com/";
	private static final String SUBMIT_URL = "http://thenakedgrapemusicbox.com/";
	
	private static final String FIRST_NAME_KEY = "first-name";
	private static final String LAST_NAME_KEY = "last-name";
	private static final String EMAIL_KEY = "email";
	private static final String EMAIL_CHECK_KEY = "confirm-email";
	private static final String COMPANY_KEY = "company";
	private static final String AFFILIATION_KEY = "affiliation";
	private static final String GUESTS_KEY = "guests";
	private static final String GUESTS_KEY_VAL = "1";
	private static final String WPCF_IS_AJAX_CALL_KEY = "_wpcf7_is_ajax_call";
	private static final String WPCF_IS_AJAX_CALL_KEY_VAL = "1";
	
	private static final Map<String, String> paramXpathMap = new HashMap<String, String>();
	static{
		paramXpathMap.put("_wpcf7", "//input[@name='_wpcf7']/@value");
		paramXpathMap.put("_wpcf7_version", "//input[@name='_wpcf7_version']/@value");
		paramXpathMap.put("_wpcf7_locale", "//input[@name='_wpcf7_locale']/@value");
		paramXpathMap.put("_wpcf7_unit_tag", "//input[@name='_wpcf7_unit_tag']/@value");
		paramXpathMap.put("_wpnonce", "//input[@name='_wpnonce']/@value");
	}
	
	public TheNakedGrapeMusicWorker(NameRequest requestObject, Boolean proxyEnabled, String formName) {
		super(requestObject, proxyEnabled, formName);
	}

	@Override
	public String getSubmitURL() {
		return SUBMIT_URL;
	}

	@Override
	public List<NameValuePair> buildNameValuePair(String... html) throws Exception {
		List<NameValuePair> pairs = new ArrayList<NameValuePair>();
		pairs.add(new BasicNameValuePair(FIRST_NAME_KEY, requestObject.getFirstName()));
		pairs.add(new BasicNameValuePair(LAST_NAME_KEY, requestObject.getLastName()));
		pairs.add(new BasicNameValuePair(EMAIL_KEY, requestObject.getEmail()));
		pairs.add(new BasicNameValuePair(EMAIL_CHECK_KEY, requestObject.getEmail()));
		
		pairs.add(new BasicNameValuePair(COMPANY_KEY, requestObject.getAffiliation()));
		pairs.add(new BasicNameValuePair(AFFILIATION_KEY, requestObject.getAffiliation()));
		pairs.add(new BasicNameValuePair(GUESTS_KEY, GUESTS_KEY_VAL));
		pairs.add(new BasicNameValuePair(WPCF_IS_AJAX_CALL_KEY, WPCF_IS_AJAX_CALL_KEY_VAL));
		for(Map.Entry<String, String> entry : paramXpathMap.entrySet())
			pairs.add(new BasicNameValuePair(entry.getKey(), getXPathOutputFromHtml(html[0], entry.getValue())));
		logger.debug(pairs);
		return pairs;
	}

	@Override
	public void addHeaders(HttpPost httpPost) {
		Header hostHeader = new BasicHeader("Host", "thenakedgrapemusicbox.com");
		Header originHeader = new BasicHeader("Origin", "http://thenakedgrapemusicbox.com");
		Header refererHeader = new BasicHeader("Referer", "http://thenakedgrapemusicbox.com/");
		Header userAgentHeader = new BasicHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.146 Safari/537.36");
		Header requestedWithHeader = new BasicHeader("X-Requested-With", "XMLHttpRequest");
		Header acceptHeader = new BasicHeader("Accept", "application/json, text/javascript, */*; q=0.01");
		Header acceptEncoding = new BasicHeader("Accept", "gzip,deflate,sdch");
		Header acceptLanguage = new BasicHeader("Accept-Language", "en-US,en;q=0.8");
		Header xRequestedWithHeader = new BasicHeader("X-Requested-With", "XMLHttpRequest");
		Header contentTypeHeader = new BasicHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
		httpPost.addHeader(hostHeader);
		httpPost.addHeader(originHeader);
		httpPost.addHeader(refererHeader);
		httpPost.addHeader(userAgentHeader);
		httpPost.addHeader(requestedWithHeader);
		httpPost.addHeader(acceptHeader);
		httpPost.addHeader(acceptLanguage);
		httpPost.addHeader(contentTypeHeader);
		httpPost.addHeader(xRequestedWithHeader);
		httpPost.addHeader(acceptEncoding);
	}

	@Override
	public long getSleepIntervalInMilliseconds() {
		return 500;
	}

	@Override
	public void buildResponse(Map<String, Object> retMap, String response) throws Exception {
		JsonNode respNode = objMapper.readTree(response);
		if(respNode.get("mailSent").asBoolean(false)){
			retMap.put("success", new Boolean(true));
		}else{
			logger.info("Failed!!");
			logger.info(response);
			retMap.put("success", new Boolean(false));
		}
		retMap.put("response", response);
	}

	@Override
	public Log getLogger() {
		return logger;
	}

	@Override
	public String getSiteURL() {
		return SITE_URL;
	}

}
