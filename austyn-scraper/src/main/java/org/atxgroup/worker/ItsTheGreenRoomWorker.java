package org.atxgroup.worker;


import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.Header;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.atxgroup.persistence.model.NameRequest;
import org.codehaus.jackson.JsonNode;

public class ItsTheGreenRoomWorker extends AbstractGetKeyAndPostWorker {
	private static final Log logger = LogFactory.getLog(ItsTheGreenRoomWorker.class);
	private static String SITE_URL = "http://www.itsthegreenroom.com/rsvp/";
	private static final String SUBMIT_URL = "http://www.itsthegreenroom.com/api/form/SaveFormSubmission?crumb=";
	private static final String KEY_URL = "http://www.itsthegreenroom.com/api/form/FormSubmissionKey?crumb=";
	private String formId = "52acb595e4b0e46ab6258b1d";
	private String collectionId = "52acb575e4b0324b1f6347a2";
	private static final String NAME_LIST_KEY = "name-yui_3_10_1_1_1387050317539_12258";
	private static final String MAIL_LIST_KEY = "email-yui_3_10_1_1_1387050317539_12573";

	public ItsTheGreenRoomWorker(NameRequest requestObject, Boolean proxyEnabled, String formName) {
		super(requestObject, proxyEnabled, formName);
	}

	public void addHeaders(HttpPost httpPost) {
		Header hostHeader = new BasicHeader("Host", "www.itsthegreenroom.com");
		Header pragmaHeader = new BasicHeader("pragma", "no-cache");
		Header refererHeader = new BasicHeader("Referer", "http://www.itsthegreenroom.com/rsvp/");
		Header userAgentHeader = new BasicHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:26.0) Gecko/20100101 Firefox/26.0");
		Header requestedWithHeader = new BasicHeader("X-Requested-With", "XMLHttpRequest");
		Header acceptHeader = new BasicHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
		Header acceptLanguage = new BasicHeader("Accept-Language", "en-US,en;q=0.5");
		Header cacheControlHeader = new BasicHeader("Cache-Control", "no-cache");
		Header contentTypeHeader = new BasicHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
		httpPost.addHeader(hostHeader);
		httpPost.addHeader(pragmaHeader);
		httpPost.addHeader(refererHeader);
		httpPost.addHeader(userAgentHeader);
		httpPost.addHeader(requestedWithHeader);
		httpPost.addHeader(acceptHeader);
		httpPost.addHeader(acceptLanguage);
		httpPost.addHeader(cacheControlHeader);
		httpPost.addHeader(contentTypeHeader);
	}

	@Override
	public String getSiteURL() {
		return SITE_URL;
	}
	
	@Override
	public Log getLogger() {
		return logger;
	}

	@Override
	public String getSubmitURL() {
		return SUBMIT_URL.concat(getCrumbFromCookie());
	}

	@Override
	public long getSleepIntervalInMilliseconds() {
		return 500;
	}

	@Override
	public void buildResponse(Map<String, Object> retMap, String str) throws Exception {
		JsonNode response = objMapper.readTree(str);
		if (response.has("error")) {
			retMap.put("success", response.get("success").asBoolean(false));
			retMap.put("response", "Error while submittig this record. Please re-upload this form.");
			requestObject.setProcessingError("Error while submittig this record. Please re-upload this form.");
		} else {
			retMap.put("success", response.get("success").asBoolean(false));
			retMap.put("response", "Response exists.");
		}		
	}

	@Override
	public List<NameValuePair> buildNameValuePair(String... html) throws Exception {
		List<NameValuePair> pairs = new ArrayList<NameValuePair>();
		pairs.add(new BasicNameValuePair("formId", formId));
		pairs.add(new BasicNameValuePair("collectionId", collectionId));
		Map<String, Object> postReqMap = new TreeMap<String, Object>(new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {
				return o2.compareTo(o1);
			}
		});
		List<String> nameList = new ArrayList<String>();
		nameList.add(requestObject.getFirstName());
		nameList.add(requestObject.getLastName());
		postReqMap.put(NAME_LIST_KEY, nameList);
		postReqMap.put(MAIL_LIST_KEY, requestObject.getEmail());
		pairs.add(new BasicNameValuePair("form", objMapper.writeValueAsString(postReqMap)));
		return pairs;
	}

	@Override
	public String getKeyURL() {
		return KEY_URL;
	}	
}
