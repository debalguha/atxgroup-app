package org.atxgroup.worker;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.atxgroup.persistence.model.NameRequest;

public class AustynCityLimitsWorker2016 extends AbstractGetHTMLAndPostWorker {
	private static final String SITE_URL = "https://docs.google.com/forms/d/1o0_qRvUnAUpYc0GmjmqNvBTskKgA8srgETka4_OwIbg/viewform?embedded=true";
	private static final String SUBMIT_URL = "https://docs.google.com/forms/d/1o0_qRvUnAUpYc0GmjmqNvBTskKgA8srgETka4_OwIbg/formResponse?embedded=true";
	
	private static final String FIRST_NAME_KEY = "entry.1913155513";
	private static final String LAST_NAME_KEY = "entry.1533200977";
	private static final String EMAIL_KEY = "entry.1074980097";
	private static final String CITY_KEY = "entry.1612998476";
	private static final String STATE_KEY = "entry.1984515405";
	private static final String ZIP_KEY = "entry.8733298";
	private static final String AFFILIATION_KEY = "entry.387144029";
	private static final String SIGN_UP_KEY = "entry.1826590217";
	private static final String SIGN_UP_VALUE = "Sign me up to receive future mailings from Austin City Limits TV";
	
	private static final Map<String, String> paramXpathMap = new HashMap<String, String>();
	static{
		paramXpathMap.put("draftResponse", "//input[@name='draftResponse']/@value");
		paramXpathMap.put("pageHistory", "//input[@name='pageHistory']/@value");
		paramXpathMap.put("fvv", "//input[@name='fvv']/@value");
		paramXpathMap.put("fbzx", "//input[@name='fbzx']/@value");
	}
	
	private static final Log logger = LogFactory.getLog(AustynCityLimitsWorker2016.class);
	public AustynCityLimitsWorker2016(NameRequest requestObject, Boolean proxyEnabled, String formName) {
		super(requestObject, proxyEnabled, formName);
	}

	@Override
	public String getSubmitURL() {
		return SUBMIT_URL;
	}

	@Override
	public List<NameValuePair> buildNameValuePair(String... html) throws Exception {
		List<NameValuePair> pairs = new ArrayList<NameValuePair>();
		pairs.add(new BasicNameValuePair(FIRST_NAME_KEY, requestObject.getFirstName()));
		pairs.add(new BasicNameValuePair(LAST_NAME_KEY, requestObject.getLastName()));
		pairs.add(new BasicNameValuePair(EMAIL_KEY, requestObject.getEmail()));
		pairs.add(new BasicNameValuePair(CITY_KEY, requestObject.getCity()));
		pairs.add(new BasicNameValuePair(STATE_KEY, requestObject.getState()));
		pairs.add(new BasicNameValuePair(ZIP_KEY, requestObject.getZipCode()));
		pairs.add(new BasicNameValuePair(AFFILIATION_KEY, requestObject.getAffiliation()));
		pairs.add(new BasicNameValuePair(SIGN_UP_KEY, SIGN_UP_VALUE));
		for(Map.Entry<String, String> entry : paramXpathMap.entrySet())
			pairs.add(new BasicNameValuePair(entry.getKey(), getXPathOutputFromHtml(html[0], entry.getValue())));
		logger.debug(pairs);
		return pairs;
	}

	@Override
	public void addHeaders(HttpPost httpPost) {}

	@Override
	public long getSleepIntervalInMilliseconds() {
		return 500;
	}

	@Override
	public void buildResponse(Map<String, Object> retMap, String str) throws Exception {
		if(str.contains("Thanks, you&#39;ve been registered")){
			retMap.put("success", new Boolean(true));
			retMap.put("response", "Thanks, you've been registered");
		}else{
			logger.warn("Failed!!");
			logger.info(str);
			retMap.put("success", new Boolean(false));
		}	
	}

	@Override
	public Log getLogger() {
		return logger;
	}

	@Override
	public String getSiteURL() {
		return SITE_URL;
	}

}
