package org.atxgroup.worker;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.atxgroup.persistence.model.NameRequest;

public class BrowniesAndLemonadeWorker extends AbstractGetHTMLAndPostWorker{
	private static final Log logger = LogFactory.getLog(BrowniesAndLemonadeWorker.class);
	private static final String SITE_URL = "https://docs.google.com/forms/d/1kYNGJQZizOGMcXANomzI87VU8uNMBD85F5sNcJtq-b8/viewform?embedded=true";
	private static final String SUBMIT_URL = "https://docs.google.com/forms/d/1kYNGJQZizOGMcXANomzI87VU8uNMBD85F5sNcJtq-b8/formResponse?embedded=true";
	private static final String FIRST_NAME_KEY = "entry.1260998579";
	private static final String LAST_NAME_KEY = "entry.278886708";
	private static final String EMAIL_KEY = "entry.567853178";
	
	private static final Map<String, String> paramXpathMap = new HashMap<String, String>();
	static{
		paramXpathMap.put("draftResponse", "//input[@name='draftResponse']/@value");
		paramXpathMap.put("pageHistory", "//input[@name='pageHistory']/@value");
		paramXpathMap.put("fvv", "//input[@name='fvv']/@value");
		paramXpathMap.put("fbzx", "//input[@name='fbzx']/@value");
	}
	
	public BrowniesAndLemonadeWorker(NameRequest requestObject, Boolean proxyEnabled, String formName) {
		super(requestObject, proxyEnabled, formName);
	}

	@Override
	public String getSubmitURL() {
		return SUBMIT_URL;
	}

	@Override
	public List<NameValuePair> buildNameValuePair(String... html) throws Exception {
		List<NameValuePair> pairs = new ArrayList<NameValuePair>();
		pairs.add(new BasicNameValuePair(FIRST_NAME_KEY, requestObject.getFirstName()));
		pairs.add(new BasicNameValuePair(LAST_NAME_KEY, requestObject.getLastName()));
		pairs.add(new BasicNameValuePair(EMAIL_KEY, requestObject.getEmail()));
		for(Map.Entry<String, String> entry : paramXpathMap.entrySet())
			pairs.add(new BasicNameValuePair(entry.getKey(), getXPathOutputFromHtml(html[0], entry.getValue())));
		logger.debug(pairs);
		return pairs;
	}

	@Override
	public void addHeaders(HttpPost httpPost) {
		
	}

	@Override
	public long getSleepIntervalInMilliseconds() {
		return 500;
	}

	@Override
	public void buildResponse(Map<String, Object> retMap, String str) throws Exception {
		if(str.contains("Thanks for signing up")){
			retMap.put("success", new Boolean(true));
			retMap.put("response", "Thanks for signing up.");
		}else{
			logger.warn("Failed!!");
			logger.warn(str);
			retMap.put("success", new Boolean(false));
		}
	}

	@Override
	public Log getLogger() {
		return logger;
	}

	@Override
	public String getSiteURL() {
		return SITE_URL;
	}
}
