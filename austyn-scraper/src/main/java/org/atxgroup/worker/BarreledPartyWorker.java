package org.atxgroup.worker;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.atxgroup.persistence.model.NameRequest;

public class BarreledPartyWorker extends AbstractGetHTMLAndPostWorker{
	private static final Log logger = LogFactory.getLog(BarreledPartyWorker.class);
	private static final String SITE_URL = "https://docs.google.com/forms/d/1w1yRGGNVuNWRrtHD-IZ5PFaOk1VPQ7MT_zyV74eyXx4/viewform";
	private static final String SUBMIT_URL = "https://docs.google.com/forms/d/1w1yRGGNVuNWRrtHD-IZ5PFaOk1VPQ7MT_zyV74eyXx4/formResponse";
	private static final String FIRST_NAME_KEY = "entry.704227368";
	private static final String LAST_NAME_KEY = "entry.1622849377";
	private static final String EMAIL_KEY = "entry.1082272241";
	
	private static final String CITY_KEY = "entry.1265008087";
	private static final String STATE_KEY = "entry.1099108734";
	private static final String ZIP_KEY = "entry.1671230119";
	private static final String AFFILIATION_KEY = "entry.1482181998";
	
	private static final Map<String, String> paramXpathMap = new HashMap<String, String>();
	static{
		paramXpathMap.put("draftResponse", "//input[@name='draftResponse']/@value");
		paramXpathMap.put("pageHistory", "//input[@name='pageHistory']/@value");
		paramXpathMap.put("fvv", "//input[@name='fvv']/@value");
		paramXpathMap.put("fbzx", "//input[@name='fbzx']/@value");
	}
	
	public BarreledPartyWorker(NameRequest requestObject, Boolean proxyEnabled, String formName) {
		super(requestObject, proxyEnabled, formName);
	}

	@Override
	public String getSubmitURL() {
		return SUBMIT_URL;
	}

	@Override
	public List<NameValuePair> buildNameValuePair(String... html) throws Exception {
		List<NameValuePair> pairs = new ArrayList<NameValuePair>();
		pairs.add(new BasicNameValuePair(FIRST_NAME_KEY, requestObject.getFirstName()));
		pairs.add(new BasicNameValuePair(LAST_NAME_KEY, requestObject.getLastName()));
		pairs.add(new BasicNameValuePair(EMAIL_KEY, requestObject.getEmail()));
		
		pairs.add(new BasicNameValuePair(CITY_KEY, requestObject.getCity()));
		pairs.add(new BasicNameValuePair(STATE_KEY, stateMap.get(requestObject.getState().toUpperCase())));
		pairs.add(new BasicNameValuePair(ZIP_KEY, requestObject.getZipCode()));
		pairs.add(new BasicNameValuePair(AFFILIATION_KEY, requestObject.getAction()));
		
		for(Map.Entry<String, String> entry : paramXpathMap.entrySet())
			pairs.add(new BasicNameValuePair(entry.getKey(), getXPathOutputFromHtml(html[0], entry.getValue())));
		logger.debug(pairs);
		return pairs;
	}

	@Override
	public void addHeaders(HttpPost httpPost) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public long getSleepIntervalInMilliseconds() {
		return 500;
	}

	@Override
	public void buildResponse(Map<String, Object> retMap, String str) throws Exception {
		if(str.contains("Thanks for your RSVP")){
			retMap.put("success", new Boolean(true));
			retMap.put("response", "Thanks for signing up.");
		}else{
			logger.warn("Failed!!");
			logger.debug(str);
			retMap.put("success", new Boolean(false));
		}		
	}

	@Override
	public Log getLogger() {
		return logger;
	}

	@Override
	public String getSiteURL() {
		return SITE_URL;
	}
	
	private static final Map<String, String> stateMap = new HashMap<String, String>();
	static{
		stateMap.put("ALABAMA", "AL");
		stateMap.put("ALASKA", "AK");
		stateMap.put("AMERICAN SAMOA", "AS");
		stateMap.put("ARIZONA", "AZ");
		stateMap.put("ARKANSAS", "AR");
		stateMap.put("CALIFORNIA", "CA");
		stateMap.put("COLORADO", "CO");
		stateMap.put("CONNECTICUT", "CT");
		stateMap.put("DELAWARE", "DE");
		stateMap.put("DIST. OF COLUMBIA", "DC");
		stateMap.put("FLORIDA", "FL");
		stateMap.put("GEORGIA", "GA");
		stateMap.put("GUAM", "GU");
		stateMap.put("HAWAII", "HI");
		stateMap.put("IDAHO", "ID");
		stateMap.put("ILLINOIS", "IL");
		stateMap.put("INDIANA", "IN");
		stateMap.put("IOWA", "IA");
		stateMap.put("KANSAS", "KS");
		stateMap.put("KENTUCKY", "KY");
		stateMap.put("LOUISIANA", "LA");
		stateMap.put("MAINE", "ME");
		stateMap.put("MARYLAND", "MD");
		stateMap.put("MARSHALL ISLANDS", "MH");
		stateMap.put("MASSACHUSETTS", "MA");
		stateMap.put("MICHIGAN", "MI");
		stateMap.put("MICRONESIA", "FM");
		stateMap.put("MINNESOTA", "MN");
		stateMap.put("MISSISSIPPI", "MS");
		stateMap.put("MISSOURI", "MO");
		stateMap.put("MONTANA", "MT");
		stateMap.put("NEBRASKA", "NE");
		stateMap.put("NEVADA", "NV");
		stateMap.put("NEW HAMPSHIRE", "NH");
		stateMap.put("NEW JERSEY", "NJ");
		stateMap.put("NEW MEXICO", "NM");
		stateMap.put("NEW YORK", "NY");
		stateMap.put("NORTH CAROLINA", "NC");
		stateMap.put("NORTH DAKOTA", "ND");
		stateMap.put("NORTHERN MARIANAS", "MP");
		stateMap.put("OHIO", "OH");
		stateMap.put("OKLAHOMA", "OK");
		stateMap.put("OREGON", "OR");
		stateMap.put("PALAU", "PW");
		stateMap.put("PENNSYLVANIA", "PA");
		stateMap.put("PUERTO RICO", "PR");
		stateMap.put("RHODE ISLAND", "RI");
		stateMap.put("SOUTH CAROLINA", "SC");
		stateMap.put("SOUTH DAKOTA", "SD");
		stateMap.put("TENNESSEE", "TN");
		stateMap.put("TEXAS", "TX");
		stateMap.put("UTAH", "UT");
		stateMap.put("VERMONT", "VT");
		stateMap.put("VIRGINIA", "VA");
		stateMap.put("VIRGIN ISLANDS", "VI");
		stateMap.put("WASHINGTON", "WA");
		stateMap.put("WEST VIRGINIA", "WV");
		stateMap.put("WISCONSIN", "WI");
		stateMap.put("WYOMING", "WY");
	}

}
