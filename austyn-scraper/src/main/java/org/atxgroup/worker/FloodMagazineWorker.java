package org.atxgroup.worker;

import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.client.entity.EntityBuilder;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.message.BasicNameValuePair;
import org.atxgroup.persistence.model.NameRequest;
import org.codehaus.jackson.map.ObjectMapper;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatterBuilder;

public class FloodMagazineWorker extends GenericWorker{

	private static final Log logger = LogFactory.getLog(FloodMagazineWorker.class);
	private static String postalCode = "78701";
	private static int numberOfPeopleAttending = 2;
	private static final String SUBMIT_URL = "https://madmimi.com/signups/subscribe/133844.json?callback=jsonp_callback_72668";
	private static String FORM_ID = "33524368359159";
	private static final ObjectMapper mapper = new ObjectMapper();
	public FloodMagazineWorker(NameRequest requestObject, Boolean proxyEnabled, String formName) {
		super(requestObject, proxyEnabled, formName);
	}
	@Override
	public Log getLogger() {
		return logger;
	}
	@Override
	public String doWork() throws Exception {
		Random random = new Random();
		logger.info("Starting to scrape for "+requestObject.getEmail());
		Map<String, Object> retMap = new HashMap<String, Object>();
		try{
			EntityBuilder.create().setParameters(new BasicNameValuePair("signup[how_did_you_hear_about_this_showcase]", "Friends"))
			.setParameters(new BasicNameValuePair("signup[birthday]", DateTimeFormat.forPattern("dd/MM/yyyy").print(new DateTime().minusYears(random.nextInt(15)).minusYears(20).minusMonths(new Random().nextInt(12)).minusDays(random.nextInt(7)).getMillis())))
			.setParameters(new BasicNameValuePair("signup[phone]", String.valueOf((long) Math.floor(Math.random() * 9000000000L) + 1000000000L)))
			.setParameters(new BasicNameValuePair("signup[zip]", requestObject.getZipCode()))
			.setParameters(new BasicNameValuePair("signup[state]", requestObject.getState()))
			.setParameters(new BasicNameValuePair("signup[city]", requestObject.getCity()))
			//.setParameters(new BasicNameValuePair("signup[address]", requestObject.getA))
			.setParameters(new BasicNameValuePair("signup[first_name]", requestObject.getFirstName()))
			.setParameters(new BasicNameValuePair("signup[first_name]", requestObject.getFirstName()))
			.setParameters(new BasicNameValuePair("signup[first_name]", requestObject.getFirstName()))
			.setParameters(new BasicNameValuePair("signup[first_name]", requestObject.getFirstName()));
		} catch (Throwable e) {
			logger.error("Unable to submit form["+formName+"] for "+requestObject.getEmail());
			logger.error("Error while submitting form["+formName+"].", e);
			retMap.put("success", new Boolean(false));
			retMap.put("response", "Unable to register " + requestObject.getEmail() + " for the Colorado Music Party.");
		}
		return null;
	}
	@Override
	public String getSiteURL() {
		return null;
	}

}
