package org.atxgroup.worker;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.atxgroup.persistence.model.NameRequest;

public class GrownKidsRadioWorker extends AbstractGetHTMLAndPostWorker {

	private static final Log logger = LogFactory.getLog(GrownKidsRadioWorker.class);
	private static final String SUBMIT_URL = "https://docs.google.com/forms/d/1xJp4A3nWFlM2Vte9L9rIQQuxchQAy85UBRz5b8LyWWc/formResponse?embedded=true";
	private static final String SITE_URL = "https://docs.google.com/forms/d/1xJp4A3nWFlM2Vte9L9rIQQuxchQAy85UBRz5b8LyWWc/viewform?embedded=true";
	
	private static final String NAME_KEY = "entry.736457519";
	private static final String EMAIL_KEY = "entry.367062972";
	private static final String STATE_KEY = "entry.404171714";
	private static final String STATE_KEY_VAL = "TX";

	
	private static final Map<String, String> paramXpathMap = new HashMap<String, String>();
	static{
		paramXpathMap.put("draftResponse", "//input[@name='draftResponse']/@value");
		paramXpathMap.put("pageHistory", "//input[@name='pageHistory']/@value");
		paramXpathMap.put("fromEmail", "//input[@name='fromEmail']/@value");
		paramXpathMap.put("fbzx", "//input[@name='fbzx']/@value");
	}
	
	public GrownKidsRadioWorker(NameRequest requestObject, Boolean proxyEnabled, String formName) {
		super(requestObject, proxyEnabled, formName);
	}

	@Override
	public List<NameValuePair> buildNameValuePair(String... html) throws Exception {
		List<NameValuePair> pairs = new ArrayList<NameValuePair>();
		pairs.add(new BasicNameValuePair(NAME_KEY, requestObject.getName()));
		pairs.add(new BasicNameValuePair(EMAIL_KEY, requestObject.getEmail()));
		pairs.add(new BasicNameValuePair(STATE_KEY, STATE_KEY_VAL));
		for(Map.Entry<String, String> entry : paramXpathMap.entrySet())
			pairs.add(new BasicNameValuePair(entry.getKey(), getXPathOutputFromHtml(html[0], entry.getValue())));
		return pairs;
	}
	
	@Override
	public String getSubmitURL() {
		return SUBMIT_URL;
	}

	@Override
	public void addHeaders(HttpPost httpPost) {

	}

	@Override
	public long getSleepIntervalInMilliseconds() {
		return 500;
	}

	@Override
	public void buildResponse(Map<String, Object> retMap, String response) throws Exception {
		if(response.contains("Your RSVP has been recorded")){
			retMap.put("success", new Boolean(true));
			retMap.put("response", "You have successfully registered");
		}else{
			logger.warn("Failed!!");
			logger.warn(response);
			retMap.put("success", new Boolean(false));
		}
	}

	@Override
	public Log getLogger() {
		return logger;
	}

	@Override
	public String getSiteURL() {
		return SITE_URL;
	}

}
