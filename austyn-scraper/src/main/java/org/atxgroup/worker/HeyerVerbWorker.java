package org.atxgroup.worker;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.atxgroup.persistence.model.NameRequest;

public class HeyerVerbWorker extends AbstractGetCookieAndPostWorker {
	private static final String SITE_URL = "https://docs.google.com/spreadsheet/embeddedform?formkey=dHZXYi0wM3liODhwLXBLV3JTV0VVR2c6MA";
	private static final String SUBMIT_URL = "https://docs.google.com/spreadsheet/formResponse?formkey=dHZXYi0wM3liODhwLXBLV3JTV0VVR2c6MA&amp;embedded=true&amp;ifq";
	private static String city="Austin";
	private static String state="Texas";
	private static String zipCode = "78701";
	private static final Log logger = LogFactory.getLog(HeyerVerbWorker.class);
	public HeyerVerbWorker(NameRequest requestObject, Boolean proxyEnabled, String formName) {
		super(requestObject, proxyEnabled, formName);
	}

	@Override
	public String getSiteURL() {
		return SITE_URL;
	}
	
	@Override
	public Log getLogger() {
		return logger;
	}

	@Override
	public String getSubmitURL() {
		return SUBMIT_URL;
	}

	@Override
	public long getSleepIntervalInMilliseconds() {
		return 1000;
	}

/*	@Override
	public String doWork() throws Exception {
		Map<String, Object> retMap = new HashMap<String, Object>();
		buildResponse(retMap, "Your response has been recorded");
		return objMapper.writeValueAsString(retMap);
	}*/
	
	@Override
	public void buildResponse(Map<String, Object> retMap, String str) throws Exception {
		if(str.contains("Your response has been recorded")){
			retMap.put("success", new Boolean(true));
			retMap.put("response", "Your response has been recorded");
		}else{
			logger.warn("Failed!!");
			logger.warn(str);
			retMap.put("success", new Boolean(false));
		}					
	}

	@Override
	public List<NameValuePair> buildNameValuePair(String... html) throws Exception {
		List<NameValuePair> pairs = new ArrayList<NameValuePair>();
		pairs.add(new BasicNameValuePair("entry.0.single", requestObject.getFirstName()));
		pairs.add(new BasicNameValuePair("entry.14.single", requestObject.getLastName()));
		pairs.add(new BasicNameValuePair("entry.1.single", requestObject.getEmail()));
		pairs.add(new BasicNameValuePair("entry.12.single", city));
		pairs.add(new BasicNameValuePair("entry.6.single", state));
		pairs.add(new BasicNameValuePair("entry.8.single", zipCode));
		pairs.add(new BasicNameValuePair("entry.2.group", "No Thank You."));
		pairs.add(new BasicNameValuePair("pageNumber", "0"));
		pairs.add(new BasicNameValuePair("backupCache", ""));
		pairs.add(new BasicNameValuePair("submit", "Submit"));
		return pairs;
	}

	@Override
	public void addHeaders(HttpPost httpPost) {
		return;
	}

}
