package org.atxgroup.worker;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.conn.HttpClientConnectionManager;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.atxgroup.http.pool.HttpConnectionPoolHelper;
import org.atxgroup.persistence.model.NameRequest;
import org.codehaus.jackson.map.ObjectMapper;

public class ColoradoMusicPartyWorker extends GenericWorker {

	private static final Log logger = LogFactory.getLog(ColoradoMusicPartyWorker.class);
	private static String postalCode = "78701";
	private static int numberOfPeopleAttending = 2;
	private static final String SUBMIT_URL = "http://submit.jotform.us/submit/33524368359159/";
	private static String FORM_ID = "33524368359159";
	private static final ObjectMapper mapper = new ObjectMapper();

	public ColoradoMusicPartyWorker(NameRequest requestObject, Boolean proxyEnabled, String formName) {
		super(requestObject, proxyEnabled, formName);
	}

	@Override
	public String doWork() throws Exception {
		logger.info("Starting to scrape.");
		Map<String, Object> retMap = new HashMap<String, Object>();
		try {
			populateFirstAndLastName();
			if (submitFormAndParseResult()) {
				logger.info("Successfully submitted form for " + requestObject.getEmail());
				retMap.put("success", new Boolean(true));
				retMap.put("response", "You are registered for the Colorado Music Party.");
			} else {
				logger.error("Unable to submit form for " + requestObject.getEmail());
				retMap.put("success", new Boolean(false));
				retMap.put("response", "Unable to register " + requestObject.getEmail() + " for the Colorado Music Party.");
			}// You are registered for the Colorado Music Party
		} catch (Exception e) {
			logger.error("Unable to submit form["+formName+"] for "+requestObject.getEmail());
			logger.error("Error while submitting form["+formName+"].", e);
			retMap.put("success", new Boolean(false));
			retMap.put("response", "Unable to register " + requestObject.getEmail() + " for the Colorado Music Party.");
		}
		return mapper.writeValueAsString(retMap);
	}

	private boolean submitFormAndParseResult() throws IOException {
		logger.info("Going to submit the form.");
		HttpClientConnectionManager connectionManager = HttpConnectionPoolHelper.getConnectionManager();
		HttpClientContext context = HttpClientContext.create();
		CloseableHttpClient httpClient = geClosabletHttpClient(context, connectionManager);
		HttpPost httpPost = new HttpPost(SUBMIT_URL);
		List<NameValuePair> pairs = new ArrayList<NameValuePair>();
		pairs.add(new BasicNameValuePair("formID", FORM_ID));
		pairs.add(new BasicNameValuePair("q10_email", requestObject.getEmail()));
		pairs.add(new BasicNameValuePair("q13_affiliation", requestObject.getAffiliation()));
		pairs.add(new BasicNameValuePair("q15_postalzipCode", postalCode));
		pairs.add(new BasicNameValuePair("q19_keepMe19[]", "Please add me to your email list for invites and info"));
		pairs.add(new BasicNameValuePair("q20_stayIn", ""));
		pairs.add(new BasicNameValuePair("q22_stayIn22", ""));
		pairs.add(new BasicNameValuePair("q5_Number_of_people_attending_", String.valueOf(numberOfPeopleAttending)));
		pairs.add(new BasicNameValuePair("q9_fullName[first]", requestObject.getFirstName()));
		pairs.add(new BasicNameValuePair("q9_fullName[last]", requestObject.getLastName()));
		pairs.add(new BasicNameValuePair("simple_spc", FORM_ID.concat("-").concat(FORM_ID)));
		pairs.add(new BasicNameValuePair("website", ""));
		httpPost.setEntity(new UrlEncodedFormEntity(pairs));
		String str = httpClient.execute(httpPost, new StringResponseHandler(), context);
		if(str.contains("You are registered for the Colorado Music Party"))
			return true;
		logger.warn("Failed!!");
		logger.warn(str);
		return false;
	}

	@Override
	public String getSiteURL() {
		return null;
	}

	@Override
	public Log getLogger() {
		return logger;
	}
}
