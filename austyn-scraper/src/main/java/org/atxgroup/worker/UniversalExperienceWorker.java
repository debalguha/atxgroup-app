package org.atxgroup.worker;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.atxgroup.persistence.model.NameRequest;

public class UniversalExperienceWorker extends AbstractGetCookieAndPostWorker {
	private static final Log logger = LogFactory.getLog(UniversalExperienceWorker.class);
	private static final String SITE_URL = "https://pages.umusic-mail.com/fontanasxsw2014/";
	private static final String SUBMIT_URL = "https://pages.umusic-mail.com/fontanasxsw2014/thanks/?src=&amp;replyTo=fontana@umgd.umusic-mail.com";
	private static final String FIRST_NAME_KEY = "firstname";
	private static final String LAST_NAME_KEY = "lastname";
	private static final String EMAIL_KEY = "email";
	private static final String COMPANY_KEY = "company";
	private static final String OPTIN_KEY = "optin1";
	private static final String OPTIN_VAL = "true";
	private static final String CLIENT_REQUEST_KEY = "clientRequest";
	private static String CLIENT_REQUEST_VAL = "true";
	
	public UniversalExperienceWorker(NameRequest requestObject, Boolean proxyEnabled, String formName) {
		super(requestObject, proxyEnabled, formName);
	}

	@Override
	public List<NameValuePair> buildNameValuePair(String... html) throws Exception {
		List<NameValuePair> pairs = new ArrayList<NameValuePair>();
		pairs.add(new BasicNameValuePair(FIRST_NAME_KEY, requestObject.getFirstName()));
		pairs.add(new BasicNameValuePair(LAST_NAME_KEY, requestObject.getLastName()));
		pairs.add(new BasicNameValuePair(EMAIL_KEY, requestObject.getEmail()));
		pairs.add(new BasicNameValuePair(COMPANY_KEY, requestObject.getAffiliation()));
		pairs.add(new BasicNameValuePair(CLIENT_REQUEST_KEY, CLIENT_REQUEST_VAL));
		pairs.add(new BasicNameValuePair(OPTIN_KEY, OPTIN_VAL));
		return pairs;
	}

	/*@Override
	public String doWork() throws Exception {
		return "{\"success\": true, \"response\": \"submitted\"}";
	}*/
	
	@Override
	public Log getLogger() {
		return logger;
	}

	@Override
	public String getSiteURL() {
		return SITE_URL;
	}

	@Override
	public String getSubmitURL() {
		return SUBMIT_URL;
	}

	@Override
	public void addHeaders(HttpPost httpPost) {
		return;
	}

	@Override
	public long getSleepIntervalInMilliseconds() {
		return 500;
	}

	@Override
	public void buildResponse(Map<String, Object> retMap, String str) throws Exception {
		retMap.put("response", str);
		if(str.contains("Thank you for your RSVP")){
			retMap.put("success", new Boolean(true));
			retMap.put("response", "Thank you for your RSVP");
		}else
			retMap.put("success", new Boolean(false));		
	}

}

