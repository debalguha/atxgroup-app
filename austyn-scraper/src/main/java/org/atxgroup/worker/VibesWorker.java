package org.atxgroup.worker;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.atxgroup.persistence.model.NameRequest;

public class VibesWorker extends GimmeWorker {
	private static final Log logger = LogFactory.getLog(VibesWorker.class);
	private static final String SITE_URL = "http://gimme.io/sxvibes2014?embed=1&embed_url=http%3A%2F%2Fwww.lndrnrs.com%2F&mode=popup&ts="+System.currentTimeMillis();
	public VibesWorker(NameRequest requestObject, Boolean proxyEnabled, String formName) {
		super(requestObject, proxyEnabled, formName);
	}

	@Override
	public String getSiteURL() {
		return SITE_URL;
	}

	@Override
	public Log getLogger() {
		return logger;
	}

}
