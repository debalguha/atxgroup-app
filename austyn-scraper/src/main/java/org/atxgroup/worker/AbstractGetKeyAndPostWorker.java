package org.atxgroup.worker;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.conn.HttpClientConnectionManager;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.atxgroup.http.pool.HttpConnectionPoolHelper;
import org.atxgroup.persistence.model.NameRequest;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonProcessingException;

public abstract class AbstractGetKeyAndPostWorker extends AbstractGetCookieAndPostWorker {

	public AbstractGetKeyAndPostWorker(NameRequest requestObject, Boolean proxyEnabled, String formName) {
		super(requestObject, proxyEnabled, formName);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public String doWork() throws Exception {
		getLogger().info("Starting to scrape for email: "+requestObject.getEmail());
		Map<String, Object> retMap = new HashMap<String, Object>();
		populateFirstAndLastName();
		CloseableHttpClient httpClient = null;
		try{
			HttpClientConnectionManager connectionManager = HttpConnectionPoolHelper.getConnectionManager();
			httpContext = HttpClientContext.create();
			httpClient = geClosabletHttpClient(httpContext, connectionManager);
			fireGetRequestForCookies(httpClient, getSiteURL(), httpContext);
			String key = handleKey(firePostRequestForKey(httpClient, getKeyURL().concat(getCrumbFromCookie()), httpContext));
			HttpPost httpPost = new HttpPost(getSubmitURL());
			List<NameValuePair> pairs = buildNameValuePair();
			pairs.add(new BasicNameValuePair("key", key));
			httpPost.setEntity(new UrlEncodedFormEntity(pairs));
			addHeaders(httpPost);
			String str = httpClient.execute(httpPost, new StringResponseHandler(), httpContext);
			buildResponse(retMap, str);
			Thread.sleep(getSleepIntervalInMilliseconds());	
		}catch(Exception e){
			getLogger().error("Unable to submit form["+formName+"] for "+requestObject.getEmail());
			getLogger().error("Error while submitting form["+formName+"].", e);
    		retMap.put("success", new Boolean(false));
    		retMap.put("response", "Unable to register "+requestObject.getEmail()+" for the the fader. Please try re-uploading this file.");
    		requestObject.setProcessingError("Unable to register "+requestObject.getEmail()+" for the Green Room Party. Please try re-uploading this file.");
		}
		return objMapper.writeValueAsString(retMap);		
	}
	public abstract String getKeyURL();
	
	protected String firePostRequestForKey(CloseableHttpClient httpClient, String keyURL, HttpClientContext httpContext) throws ClientProtocolException, IOException {
		HttpPost httpPost = new HttpPost(keyURL);
		return httpClient.execute(httpPost, new StringResponseHandler(), httpContext);
	}

	public String handleKey(String keyResponse) throws JsonProcessingException, IOException {
		JsonNode response = objMapper.readTree(keyResponse);
		return response.get("key").asText();
	}
}
