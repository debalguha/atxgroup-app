package org.atxgroup.names.cache;

import java.util.List;
import java.util.Set;

import org.atxgroup.persistence.model.NameRequest;
import org.atxgroup.persistence.service.NameService;

public interface NamesCache {

	public abstract void setNameService(NameService nameService);

	public abstract List<NameRequest> searchInNamesCache(String name);

	public abstract List<NameRequest> searchInEmailCache(String email);
	
	public abstract NameRequest searchInEmailCacheForExactMatch(String email);

	public abstract Set<NameRequest> getNamesCache();

	public abstract void updateCache(NameRequest name);
	
	public void refresh();

}