package org.atxgroup.names.cache;

@SuppressWarnings("serial")
public class TooMuchReordsFoundException extends RuntimeException {
	private final long searchRecordsFound;
	
	public TooMuchReordsFoundException(long searchRecordsFound, String message) {
		super(message);
		this.searchRecordsFound = searchRecordsFound;
	}

	public long getSearchRecordsFound() {
		return searchRecordsFound;
	}
	
}
