package org.atxgroup.persistence.model;

import java.util.Date;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.Transient;

@Entity
@AssociationOverrides({ @AssociationOverride(name = "pk.nameRequest", joinColumns = @JoinColumn(name = "name_id")), @AssociationOverride(name = "pk.formApplication", joinColumns = @JoinColumn(name = "form_id")) })
public class NameRequestApplicationForm extends BaseModel{
	@EmbeddedId
	private NameRequestApplicationFormPK pk = new NameRequestApplicationFormPK();
	
	private Date creationDate;
	private Date updateDate;
	@Column(nullable=false, columnDefinition="TEXT")
	private String resultJSON;
	
	@Column(name="group_name", nullable=true, length=100)
	private String group;
	
	@Column(name="affiliation", nullable=true, length=100)
	private String affiliation;
	
	@Enumerated
	@Column(nullable = true)
	private SubmitStatus status;
	
	public SubmitStatus getStatus() {
		return status;
	}

	public void setStatus(SubmitStatus status) {
		this.status = status;
	}

	@Transient
	public NameRequest getNameRequest(){
		return pk.getNameRequest();
	}
	
	public void setNameRequest(NameRequest nameRequest) {
		pk.setNameRequest(nameRequest);
	}	
	
	@Transient
	public ApplicationForm getApplicationForm(){
		return pk.getApplicationForm();
	}
	
	public void setApplicationForm(ApplicationForm applicationForm) {
		pk.setApplicationForm(applicationForm);
	}

	public NameRequestApplicationFormPK getPk() {
		return pk;
	}

	public void setPk(NameRequestApplicationFormPK pk) {
		this.pk = pk;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getResultJSON() {
		return resultJSON;
	}

	public void setResultJSON(String resultJSON) {
		this.resultJSON = resultJSON;
	}

	@Override
	public long getID() {
		throw new RuntimeException("Composite ID. Can not return a long.");
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public String getAffiliation() {
		return affiliation;
	}

	public void setAffiliation(String affiliation) {
		this.affiliation = affiliation;
	}
}
