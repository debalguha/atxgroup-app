package org.atxgroup.persistence.model;

import java.util.Date;

public abstract class BaseModel {
	public abstract long getID();
	public abstract Date getCreationDate();
	public abstract Date getUpdateDate();
	public abstract void setCreationDate(Date date);
	public abstract void setUpdateDate(Date date);
}
