package org.atxgroup.persistence.model;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@SuppressWarnings("serial")
@Embeddable
public class NameRequestApplicationFormPK implements Serializable{
	@ManyToOne
	private NameRequest nameRequest;
	
	@ManyToOne(fetch = FetchType.EAGER)
	private ApplicationForm applicationForm;

	public NameRequest getNameRequest() {
		return nameRequest;
	}

	public void setNameRequest(NameRequest nameRequest) {
		this.nameRequest = nameRequest;
	}

	public ApplicationForm getApplicationForm() {
		return applicationForm;
	}

	public void setApplicationForm(ApplicationForm applicationForm) {
		this.applicationForm = applicationForm;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null || getClass() != obj.getClass())
			return false;
		NameRequestApplicationFormPK that = (NameRequestApplicationFormPK)obj;
		if(nameRequest!=null ? !nameRequest.equals(that.nameRequest) : that.nameRequest!=null)
			return false;
		
		if(applicationForm!=null ? !applicationForm.equals(that.applicationForm) : that.applicationForm!=null)
			return false;
		
		return super.equals(obj);
	}
}
