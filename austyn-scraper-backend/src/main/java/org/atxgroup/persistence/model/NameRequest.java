package org.atxgroup.persistence.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@NamedQueries({
	@NamedQuery(
	name = "findAll",
	query = "from NameRequest r where r.ID between :startId and :endId order by r.name"
	),
	@NamedQuery(
	name = "findByEmail",
	query = "from NameRequest r where r.email = :email"
	),
	@NamedQuery(
	name = "findMaxId",
	query = "select max(id) from NameRequest r"
	),
	@NamedQuery(
	name = "findMinId",
	query = "select min(id) from NameRequest r"
	)	
})
public class NameRequest extends BaseModel{
	@Id
	@Column
	@GeneratedValue(strategy=GenerationType.AUTO)
	protected long ID;
	
	@Column(length=10, nullable=true)
	private String action;
	@Column(length=100, unique=true)
	private String email;
	@Column(length=50, nullable=true)
	private String impression_id;
	@Column(length=50, nullable=true)
	private String security_hash;
	@Column(length=100, nullable=false)
	private String name;
	@Column(length=50, nullable=true)
	private String firstName;
	@Column(length=50, nullable=true)
	private String lastName;	
	@Transient
	private boolean subscribe;
	
	@Transient
	private String groupName;
	@Transient
	private String affiliation;
	
	@Transient
	private String processingError;
	
	private Date creationDate;
	private Date updateDate;
	
	@Column(length = 50, nullable = true)
	private String city;
	@Column(length = 50, nullable = true)
	private String state;
	@Column(length = 15, nullable = true)
	private String zipCode;
	
	@JsonIgnore
	@OneToMany(fetch = FetchType.EAGER, mappedBy="pk.nameRequest")
	private Set<NameRequestApplicationForm> applications;
	
	public NameRequest(String action, String email, String impression_id, String security_hash, String name, boolean subscribe) {
		super();
		this.action = action;
		this.email = email;
		this.impression_id = impression_id;
		this.security_hash = security_hash;
		this.name = name;
		this.subscribe = subscribe;
	}
	
	public NameRequest(String action, String email, String impression_id, String security_hash, String name, boolean subscribe, String city, String state, String zipCode) {
		super();
		this.action = action;
		this.email = email;
		this.impression_id = impression_id;
		this.security_hash = security_hash;
		this.name = name;
		this.subscribe = subscribe;
		this.city = city;
		this.state = state;
		this.zipCode = zipCode;
	}
	
	public NameRequest() {
	}

	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getImpression_id() {
		return impression_id;
	}
	public void setImpression_id(String impression_id) {
		this.impression_id = impression_id;
	}
	public String getSecurity_hash() {
		return security_hash;
	}
	public void setSecurity_hash(String security_hash) {
		this.security_hash = security_hash;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isSubscribe() {
		return subscribe;
	}
	public void setSubscribe(boolean subscribe) {
		this.subscribe = subscribe;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public long getID() {
		return ID;
	}
	public void setID(long iD) {
		ID = iD;
	}

	@Override
	public String toString() {
		return "NameRequest [action=" + action + ", email=" + email + ", impression_id=" + impression_id + ", security_hash=" + security_hash + ", name=" + name + ", subscribe=" + subscribe + ", creationDate=" + creationDate + ", updateDate=" + updateDate + "]";
	}

	public String getProcessingError() {
		return processingError;
	}

	public void setProcessingError(String processingError) {
		this.processingError = processingError;
	}

	@JsonIgnore
	public Set<NameRequestApplicationForm> getApplications() {
		return applications;
	}

	public void setApplications(Set<NameRequestApplicationForm> applications) {
		this.applications = applications;
	}
	
	@Override
	public boolean equals(Object obj) {
		return obj==null?false:email.equals(((NameRequest)obj).email);
	}
	
	@Override
	public int hashCode() {
		return email.hashCode();
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getAffiliation() {
		return affiliation;
	}

	public void setAffiliation(String affiliation) {
		this.affiliation = affiliation;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
}
