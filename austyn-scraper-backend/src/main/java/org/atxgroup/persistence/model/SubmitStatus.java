package org.atxgroup.persistence.model;

public enum SubmitStatus {
	FAIL, SUCCESS
}
