package org.atxgroup.persistence.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@NamedQueries({
	@NamedQuery(
	name = "findFormByName",
	query = "from ApplicationForm r where r.formName = :name"
	)
})
public class ApplicationForm extends BaseModel{
	@Id
	@Column
	@GeneratedValue(strategy=GenerationType.AUTO)
	protected long ID;
	
	@Column(nullable=false, length=100, unique=true)
	private String formName;
	@Column(nullable=false, length=256)
	private String formURL;
	
	private boolean attachmentRequired;
	private boolean active;
	
	private Date creationDate;
	private Date updateDate;

	public String getFormName() {
		return formName;
	}
	public void setFormName(String formName) {
		this.formName = formName;
	}
	public String getFormURL() {
		return formURL;
	}
	public void setFormURL(String formURL) {
		this.formURL = formURL;
	}
	public boolean isAttachmentRequired() {
		return attachmentRequired;
	}
	public void setAttachmentRequired(boolean attachmentRequired) {
		this.attachmentRequired = attachmentRequired;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	@Override
	public long getID() {
		return ID;
	}
	
}
