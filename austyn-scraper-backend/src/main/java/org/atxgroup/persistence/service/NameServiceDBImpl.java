package org.atxgroup.persistence.service;

import java.util.Collection;
import java.util.List;

import org.atxgroup.persistence.NamesDAO;
import org.atxgroup.persistence.model.ApplicationForm;
import org.atxgroup.persistence.model.NameRequest;
import org.atxgroup.persistence.model.NameRequestApplicationForm;
import org.atxgroup.persistence.model.SubmitStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class NameServiceDBImpl implements NameService{

	@Autowired
	private NamesDAO nameDao;

	public void setNameDao(NamesDAO nameDao) {
		this.nameDao = nameDao;
	}
	
	@Transactional
	public boolean createName(NameRequest req){
		nameDao.saveEntity(req);
		return true;
	}
	
	@Transactional
	public boolean updateName(NameRequest req){
		nameDao.updateEntity(req);
		return true;
	}
	
	@Transactional
	public Collection<NameRequest> findNamesWithinRange(long startId, long endId){
		return nameDao.getNames(startId, endId);
	}
	
	@Transactional
	public long findMaxId(){
		return nameDao.findMaxId();
	}
	
	@Transactional
	public long findMinId(){
		return nameDao.findMinId();
	}	
	
	@Transactional
	public void fillApplicationForm(NameRequest nameRequest, ApplicationForm applicationForm, String resultJSON, SubmitStatus status){
		nameDao.fillApplicationForm(nameRequest, applicationForm, resultJSON, status);
	}
	
	@Transactional
	public ApplicationForm findApplicationFormByName(String name){
		return nameDao.findApplicationFormByName(name);
	}
	
	@Transactional
	public void createNameAndSubmitApplicationForm(NameRequest nameRequest, String formnName, String resultJSON, SubmitStatus status){
		if(nameRequest.getID()<=0)
			createName(nameRequest);
		ApplicationForm form = findApplicationFormByName(formnName);
		fillApplicationForm(nameRequest, form, resultJSON, status);
	}

	@Transactional
	public NameRequest findNameRequestByEmail(String email) {
		NameRequest nameRequest = null;
		try{
			nameRequest = nameDao.findNameByEmail(email);
		}catch(Exception e){e.printStackTrace();}
		return nameRequest;
	}
	
	@Transactional
	public void batchUpdateGroupName(List<NameRequestApplicationForm> entitiesToUpdate){
		nameDao.batchUpdateGroupName(entitiesToUpdate);
	}
	
	@Transactional
	public Collection<NameRequest> findAllNamesSubmittedForAForm(String formNam){
		return nameDao.findAllNamesSubmittedForAForm(formNam);
	}

	public void refreshName(NameRequest name) {
		nameDao.refresh(name);
	}

}
