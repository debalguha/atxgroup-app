package org.atxgroup.persistence.service;

import java.util.Collection;
import java.util.List;

import org.atxgroup.persistence.model.ApplicationForm;
import org.atxgroup.persistence.model.NameRequest;
import org.atxgroup.persistence.model.NameRequestApplicationForm;
import org.atxgroup.persistence.model.SubmitStatus;

public interface NameService {

	public boolean createName(NameRequest req);
	
	public boolean updateName(NameRequest req);
	
	public Collection<NameRequest> findNamesWithinRange(long startId, long endId);
	
	public long findMaxId();
	
	public long findMinId();
	
	public void fillApplicationForm(NameRequest nameRequest, ApplicationForm applicationForm, String resultJSON, SubmitStatus status);
	
	public ApplicationForm findApplicationFormByName(String name);
	
	public void createNameAndSubmitApplicationForm(NameRequest nameRequest, String formnName, String resultJSON, SubmitStatus status);

	public NameRequest findNameRequestByEmail(String email);
	
	public Collection<NameRequest> findAllNamesSubmittedForAForm(String formName);
	
	public void batchUpdateGroupName(List<NameRequestApplicationForm> entitiesToUpdate);
	
	public void refreshName(NameRequest name);
}
