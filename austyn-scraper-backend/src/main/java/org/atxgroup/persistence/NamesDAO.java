package org.atxgroup.persistence;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.atxgroup.persistence.model.ApplicationForm;
import org.atxgroup.persistence.model.BaseModel;
import org.atxgroup.persistence.model.NameRequest;
import org.atxgroup.persistence.model.NameRequestApplicationForm;
import org.atxgroup.persistence.model.SubmitStatus;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import ch.lambdaj.Lambda;

@Repository
public class NamesDAO {
	@PersistenceContext
	private EntityManager entityManager;
	
	@Value("${hibernate.jpa.batchSize}")
	private int batchSize;
	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	
	public void saveEntity(BaseModel model){
		Date nowDate = new Date();
		model.setUpdateDate(nowDate);
		if(model.getCreationDate()==null)
			model.setCreationDate(nowDate);
		entityManager.persist(model);
	}
	
	public void updateEntity(BaseModel model){
		Date nowDate = new Date();
		model.setUpdateDate(nowDate);
		entityManager.merge(model);
	}
	
	@SuppressWarnings("unchecked")
	public Collection<NameRequest> getNames(long startId, long endId){
		Query query = entityManager.createNamedQuery("findAll").setParameter("startId", startId).setParameter("endId", endId);
		Collection<NameRequest> names = new ArrayList<NameRequest>();
		Iterator<NameRequest> itr = query.getResultList().iterator();
		if(itr!=null){
			while(itr.hasNext())
				names.add(itr.next());
		}
		return names;
	}
	
	public long findMaxId(){
		Query query = entityManager.createNamedQuery("findMaxId");
		return new Long(query.getSingleResult().toString());
	}

	public long findMinId() {
		Query query = entityManager.createNamedQuery("findMinId");
		try{
			return new Long(query.getSingleResult().toString());
		}catch(Exception e){
			return -1;
		}
	}
	
	public void fillApplicationForm(NameRequest nameRequest, ApplicationForm applicationForm, String resultJSON, SubmitStatus status){
		NameRequestApplicationForm application = new NameRequestApplicationForm();
		application.setApplicationForm(applicationForm);
		application.setNameRequest(nameRequest);
		application.setResultJSON(resultJSON);
		application.setGroup(nameRequest.getGroupName());
		application.setAffiliation(nameRequest.getAffiliation());
		application.setStatus(status);
		saveEntity(application);
	}
	
	public ApplicationForm findApplicationFormByName(String name){
		Query query = entityManager.createNamedQuery("findFormByName").setParameter("name", name);
		try{
			return (ApplicationForm)query.getSingleResult();
		}catch(Exception e){
			throw new RuntimeException(e);
		}
	}
	
	@SuppressWarnings("unchecked")
	public Collection<NameRequest> findAllNamesSubmittedForAForm(String formName){
		ApplicationForm form = findApplicationFormByName(formName);
		Collection<NameRequest> emptyCollection = Collections.emptyList();
		List<NameRequestApplicationForm> nameRequestApplications = entityManager.createQuery("from NameRequestApplicationForm where pk.applicationForm = ?").setParameter(1, form).getResultList();
		return CollectionUtils.isEmpty(nameRequestApplications)? emptyCollection:Lambda.extract(nameRequestApplications, Lambda.on(NameRequestApplicationForm.class).getPk().getNameRequest());
	}

	public NameRequest findNameByEmail(String email) {
		Query query = entityManager.createNamedQuery("findByEmail").setParameter("email", email);
		try{
			return (NameRequest)query.getSingleResult();
		}catch(Exception e){
			throw new RuntimeException(e);
		}
	}

	public void batchUpdateGroupName(List<NameRequestApplicationForm> entitiesToUpdate) {
		int counter=0;
		for(BaseModel model : entitiesToUpdate){
			if(counter==batchSize){
				counter=0;
				entityManager.flush();
				entityManager.clear();
			}
			entityManager.merge(model);
			counter++;
		}
	}

	public void setBatchSize(int batchSize) {
		this.batchSize = batchSize;
	}
	
	public void refresh(BaseModel baseModel){
		entityManager.refresh(baseModel);
	}
}
